# Opendaylight Workshop

## Objective

After completing this workshop, network administrators will be able to run and manage Opendaylight Controller. They will understand how to develop python apps using Opendaylight RESTConf interface. Finally, they will also develop an Opendaylight application using MD-SAL architecture.

## Agenda

### Day One

In this first day of Opendaylight workshop students will get introduced to Opendaylight SDN controller, YANG modelling language, NETCONF and Openflow protocols. Students will also interact with Opendaylight REST interface and will write python clients to manage an Openflow and NETCONF devices.Students ideally should have some basic understanding of REST interface and python language.
* Introduction to SDN and Opendaylight
* Setup (optional)
	* Install SDN controller
	* Install Vyatta Virtual Router
	* Install Mininet Network
* YANG Modeling Language
	* Lab: YANG
* NETCONF Network Configuration Protocol
	* Lab: NETCONF and vRouter
* Openflow
	* Lab: Openflow with a Mininet network
* Python labs
	* Topology client
	* Flows manager for Openflow
	* Adding/removing a vRouter
	* Managing firewall rules on a vRouter

***

### Day Two

In this second day of Opendaylight workshop students will get introduced to an Opendaylight application development process. Students will develop an application gradually after finishing all labs.Students ideally should have some understanding of Java, Eclipse, maven, REST and OSGI.
* MD-SAL Application Architecture
* Development environment (Eclipse, Maven, OSGI)
* Labs
	* Create a project from archetype
	* Deploying an application on the controller
	* Update YANG Model
	* Register Listeners
	* Create Transactions
	* Adding flows
	* Adding RPC methods to YANG Model
	* Adding Notifications to YANG Model
