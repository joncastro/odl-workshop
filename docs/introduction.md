# Developing for the Brocade SDN Controller

## What is SDN

Software Defined Networking (SDN) is an operational network architecture that focuses on augmenting the role of the control plane on network devices. It is important to note that it does not replace the control plane but merely extends the functionality and capability by a having a view of the entire network all at once vs from one position in the topology.

This is not the only functionality SDN adds, it also enables the network to be accessed programmatically via APIs or changes in state. This allows for automated management through orchestration techniques. Configuration can now be applied across multiple routers, switches and servers. This is done by the decoupling of the application that performs these operations from the network devices OS.

SDN is not constrained to a single network domain such as the data centre, nor a single customer type (e.g. research/education), a single application (e.g. data centre orchestration), single architectural model (centralised controller and a group of droid switches) or even a single protocol (e.g. OpenFlow). This makes SDN an enabler to programmatically optimise, monetise, and scale networks with the flexibility to meet the demands of diverse and unique network architectures.


## Brief History of SDN

The underlying ideas of SDN have evolved over the [last 20 years][RSDN].

### Mid 1990s - Early 2000s (Active Networking)

Beginning in the mid 90s to early 2000s with 'Active Networking' which envisioned a programmable interface (API) that exposed network resources such as packet queues. This would be achieved through the installation of new data-plane functionality by carrying code in data packets. Active Networking didn't see widespread deployment because of a lack of compelling solutions to pressing needs at the time.

### Early 2000s - Mid 2000s(Control Plane and Data Plane Decoupling)

The need for traffic engineering, large scale network management, new services such as VPN where being demanded by network operators. However conventional routers and switches had tightly integrated control and data planes making debugging configuration and controlling routing behaviour challenging. 

Rapid advances in commodity computing platforms meant that servers often had substantially more memory and processing resources than the control-plane processor of a router. These forces drove the development of innovations that generated SDN concepts that have been carried forward. 

Innovations such as ForCES which aimed at creating a open interface between the control and data planes and logically centralised control architectures such the Routing Control Platform (RCP) focused on network management, programmability in the control plane and network-wide visibility and control.

Widespread adoption didn't take place even though industry prototypes and standardisation efforts made progress because hardware vendors had little incentive to adopt standardised APIs that could enable new entrants into the market place. In addition the need to support existing routing protocols imposed limitations on the range of applications that programmable controllers could support.

### Mid 2000s - Present (OpenFlow)

Switch chipset manufacturers were opening their APIs to allow programmers to control certain forwarding behaviours, this coupled with the availability of these chipsets meant a wider range of companies could build switches without having to fabricate their own data-plane hardware. This created a environment where innovations such as OpenFlow could take hold because of the ubiquity of devices and the ability to program them. 

OpenFlow which began development at Stanford University in 2008 enabled more functions than earlier route controllers and was built to operate on existing switch hardware which meant (in theory) it was instantly deployable through firmware upgrades. Real SDN use cases came from initial OpenFlow deployments on campus networks and data-centre networks which were demanding finer traffic management control at a large scale.

OpenFlow built upon previous SDN principles by introducing generalisation of network devices and functions by defining rules on 13 different packet headers unifying different types of network devices that differ only in terms of which header fields they match, and which actions they perform. It also developed a vision of a network operating system which conceptualises network operation into three layers: 

1. A data plane with an open interface
2. A state management layer responsible for maintaining a consistent view of network state
3. Control logic that performs various operations depending on its view of network state. 

The success and adoption of SDN depends on whether it can be used to solve pressing problems in networking that were difficult or impossible to solve with earlier protocols. SDN has already proved useful for solving problems related to network virtualisation.


## OpenDaylight - SDN Controller

[OpenDaylight] is an open source Java based SDN controller. It's goal is to enable SDN and NFV for networks of any size and scale, OpenDaylight software is a combination of components including a fully pluggable controller, interfaces, protocol plug-ins and applications.

There are three tiers of membership for OpenDaylight: Platinum, Gold and Silver, with varying levels of commitment. Each Platinum member must contribute 10 developers to the project while Gold members must contribute 3 developers.

[Brocade] is a founding, Platinum member of OpenDaylight and offers a commercial controller the Brocade SDN Controller that is built directly from OpenDaylight code, without any proprietary extensions or platform dependencies. Users can freely optimise their network infrastructure to match the needs of their workloads, and develop network applications that can be run on any OpenDaylight-based controller.

### OpenDaylight Architecture

![OpenDaylight Architecture](https://github.com/brocade-apj/workshops/blob/master/odl/images/daylight-architecture.png?raw=true)

#### Southbound

The Southbound interfaces allow the controller to communicate with network elements via multiple protocols (as plugins) such as OpenFlow, BGP-LS etc.

##### Service Abstraction Layer (SAL)

![Service Abstraction Layer](https://github.com/brocade-apj/workshops/blob/master/odl/images/service_abstraction_layer.jpg?raw=true)

The Service Abstraction Layer (SAL) determines how to fulfil an internal or external request by mapping it to the appropriate southbound plugin(s) and provides basic service abstractions that higher level services are built upon, depending on the capabilities of the plugin(s). Each plugin is independent of each other and are loosely coupled with the SAL. This means as a developer the specifics of how to program a flow in OpenFlow are abstracted, so that a flow can be created irrespective of the southbound OpenFlow plugin version.

Following is a list of services the SAL provides:

* Topology Service is a set of services that allow to convey topology information like a new node a new link has been discovered and so on.
* Data Packet services, in summary the possibility to deliver to applications the packets coming from the agents, if any.
* Flow Programming service is supposed to provide the necessary logic to program in the different agents a Match/Actions rule.
* Statistics service will export API to be able to collect statistics at least per:
    * Flow
    * Node Connector (port)
    * Queue
* Inventory service will provide APIs for returning inventory information about the node and node connectors for example
* Resource service is a placeholder to query resource status

#### TODO: Controller Platform



#### Northbound

The controller exposes a bidirectional REST API that can be consumed by user applications such as a web portal, the OpenDaylight controller features a GUI that has implemented the same Northbound API available for any other user application. 


### OpenDaylight Projects

OpenDaylight projects are focused in the following areas:

* The OpenDaylight controller
* Software for forwarding elements
* Southbound plugins to enable the controller to speak to the OpenDaylight supplied and other network devices
* Northbound plugins to expose interfaces to those writing applications to the controller
* Network services and applications intended to run on top of the controller, integration between the controller and other devices
* Support projects such as tools, infrastructure, or testing.
* Plugins for inter-controller communication

![OpenDaylight Helium Diagram](https://github.com/brocade-apj/workshops/blob/master/odl/images/odl_diagram_helium.jpg?raw=true)




[RSDN]: https://www.cs.princeton.edu/courses/archive/fall13/cos597E/papers/sdnhistory.pdf ("The Road to SDN: An Intellectual History of Programmable Networks")

[OpenDaylight]: http://www.opendaylight.org/software

[Brocade]: http://www.brocade.com/en/possibilities/technology/sdn-and-opendaylight.html