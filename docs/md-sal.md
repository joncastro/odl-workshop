# MD-SAL Application Architecture
Model Driven - Service Abstraction Layer (MD-SAL) is a set of infrastructure services aimed at providing common and generic support for application and plugin developers.

MD-SAL currently provides infrastructure services for:

* Data Store
    * Data Broker access to the data tree 
* RPC / Service routing
* Notification subscription and publish services

It's aim is to unify both northbound and southbound APIs and the data structures used in various services and components of an SDN Controller.

## Key Components of MD-SAL

* Flexibility to run very different set of applications on the controller, with very different APIs and functionality. But still have a common framework and common programming model.
* Consistent APIs for different applications
    * Have the APIs generated at either compile time or runtime from the models
* Functionality equivalent APIs for different language bindings
    * A plugin that provides a model and is a provider of an API doesn't know if a consumer of that API is coming through REST (Northbound) or coming through Java (Southbound), so does not discriminate in this way.
    * The API contract is specified in the model and thus can be translated into different languages or different interpretations.
* Run-time extensibility
    * New models need to be added during runtime such as uploading of models for the use in NETCONF
    * A plugin is loaded at runtime, the controller can adapt itself to be able to work with the APIs that are exposed by the model, or interpret the data that the plugin uses.
* The MD-SAL is built as pure plumbing doesn't interpret the data in anyway
    * This allows for plugins to be developed independently and utilise each others APIs to interact
    * The SAL can store a plugin data and it can do the routing between northbound and southbound plugins
    
## Brief History of MD-SAL

The first SDN controllers developed were focused on OpenFlow, and such the applications built for the SDN controllers were based on the southbound protocol OpenFlow.

One of the primary goals of the OpenDaylight project was to have a protocol agnostic architecture, one that wasn't directly tied to OpenFlow. The first revisions of the Service Abstraction Layer (SAL) became a collection of independent and discrete APIs, with one set of APIs for each and every southbound protocol.

The result of this architecture was that it was difficult to develop SDN applications that required the use of multiple southbound protocols. As the API Driven SAL (AD-SAL) as it became known was more for communicating directly with network elements using protocol(s) associated with a specific API.

MD-SAL utilises a model layer in which SDN applications interact with software models of network elements rather than the elements themselves. Providing an architecture that is protocol agnostic and allowing greater flexibility to SDN applications in interacting with network elements.

## MD-SAL and YANG

YANG is the modeling language for MD-SAL based applications.

* It is used as a [Interactive Data Language (IDL)](https://en.wikipedia.org/wiki/IDL_(programming_language)) to support the function of the controller
* Allows all components to be modelled as a single system
* Defines semantic elements and their relationships
* The XML nature of YANG data model presents an opportunity for:
    * Self-describing data
    * Links between controller components and applications northbound API endpoints.
    
YANG is ideal for creating the interfaces (contracts) to express what an application model can do. In order to utilise YANG in OpenDaylight MD-SAL driven applications the models must be converted to Java code.

### YANG Tools

The [YANG Tools project](https://wiki.opendaylight.org/view/YANG_Tools:Main) aims to provide the necessary tooling and libraries to provide Java runtime and support for data structures modeled by YANG.

YANG Tools assists in the MD-SAL application development process by parsing YANG models and generating infrastructure code, including [RESTFul API's](http://tools.ietf.org/html/draft-bierman-netconf-yang-api-01).

This greatly reduces the amount of code that needs to be written by OpenDaylight MD-SAL applications allowing developers to focus on the implementation of the interfaces that have been defined in the YANG model(s).

## MD-SAL High-Level Overview

A MD-SAL based application relies on models which produce an API to which other application models can communicate. This produces a community of models, some of which can be both producers, and consumers, of the implemented services.

MD-SAL applications models are essentially contracts of **what** the application can do. These contracts (interfaces) are implement by the application which **provides** the functionality specified by the contracts. The MD-SAL application can also **consume** other MD-SAL applications services that have been exposed based on the models (contracts) they have provided.

This allows applications to build on top of each others functionality such as the Brocade Path Explorer application that utilises the Device and Topology applications models (contracts) that have been provided. In turn another application can make use of the Path Explorer models that haven provided.

## MD-SAL Deep Dive

MD-SAL relies on a data store, also known as the MD-SAL database. This is used by providers to store data and by consumer to retrieve data. This is how providers and consumers exchange data. Data in the MD-SAL is accessed through getter and setter APIs generated from models. Note that this is in contrast to the AD-SAL, which is stateless.

### MD-SAL Request Routing

MD-SAL provide request routing which is when an RPC call or notification event is generated by either a southbound plugin or northbound plugin. 

The SAL has no concept of north or south and is only concerned with data exchange mechanisms between plugins. The SAL roles (producer and consumer) are defined by what they are doing, if a plugin's code is implementing an API defined by a model and providing API data then it is a **provider** otherwise if the code is consuming API data then it is a **consumer**.

The main function of the MD-SAL is to facilitate the plumbing between providers and consumers. A provider or a consumer can register itself with the MD-SAL. A consumer can find a provider that it’s interested in.

##### Provider
* A provider can generate notifications
* A provider can insert data into SAL’s storage

##### Consumer
* A consumer can receive notifications and issue RPCs to get data from providers
* A consumer can read data from SAL’s storage


### MD-SAL an Infrastructure for Service Adaptation

Service adaptation is the translation of one plugins (model generated) API to another plugins (model generated) API. This is necessary in order to consume services written by separate plugins.

To better understand service adaptation an example is necessary:

##### AD-SAL Service Adaptation
A southbound plugin such as OpenFlow which communicates directly with OpenFlow switches it exposes an API to program a flow in the switches flow table. A northbound plugin such as the Forwarding Rules Manager exposes a REST API north of itself to a client application. In a AD-SAL application the Forwarding Rules Manager plugin would implement interfaces defined by the OpenFlow plugin directly. 

##### MD-SAL Service Adaptation
Unlike AD-SAL, MD-SAL does not provide service adaption itself but instead provides  the infrastructure necessary to allow inter application communication. This is achieved by applications registering their intents and APIs in the MD-SAL data store.

### MD-SAL Remote Procedure Calls
An RPC is a one-to-one call triggered by a Consumer, which may be processed by a Provider either local or remote. The MD-SAL routes RPC calls between Consumers and Providers through the **RPC Call Router**.

### MD-SAL Notification
A Notification is an event, which a Consumer may be interested in to receive, and which is triggered / originated in a Provider. The MD-SAL provides a subscription-based mechanism for delivery of Notifications from Publishers to Subscribers through the **Notification Broker**.

### MD-SAL Data Store
The Data Store is a conceptual data tree, which is described by YANG schemas. 

A Path is a unique locator of a leaf or sub-tree in the conceptual data tree.

The MD-SAL routes data reads from Consumers to a particular data store and coordinates data changes between Providers through the **Data Broker**.

#### Data Handling Functionality
The implementation of the above MD-SAL functions requires the use of two data representations and two sets of MD-SAL Plugin APIs.

The DOM Broker uses YANG data APIs to describe data.

The DOM Broker uses Instance Identifiers specific to YANG to describe paths to data in the system.

##### Binding Independent DOM Broker 
A Data Object Model (DOM) representation of YANG trees that is loaded from the YANG models at runtime and is the core component of the MD-SAL runtime.

This format is suitable for generic components, such as the data store, the NETCONF Connector, RESTCONF, which can derive behavior from a YANG model itself.

##### Binding Aware DOM Broker
A specific YANG to Java language binding, which specifies how Java Data Transfer Objects (DTOs) and APIs are generated from YANG models

The API definition for these DTOs, interfaces for invoking / implementing RPCs, interfaces containing Notification callbacks are generated at compile time. Codecs to translate between the Java DTOs and DOM representation are generated on demand at run time.

##### Binding Aware to Binding Independent Translation

![MD-SAL BA-BI Translation](https://github.com/brocade-apj/workshops/blob/master/odl/images/md-sal-ba-bi.jpg?raw=true)

The Binding-Aware Broker connects to the DOM Broker through the BA-BI Connector. This is so that the Binding Aware (generate code) can communicate with their respective binding independent counterparts.

The BA-BI Connector, together with the Mapping Service, the Schema Service, the Codec Registry and the Codec Generator implement dynamic late binding.


### MD-SAL Application Workflow

Steps 1-3 in the figure show pkt_in processing. Steps 4-7 show pkt_out processing (flooding if not match is found). finally, Steps 8-12 show flow programming to the switch.

![MD-SAL Workflow](https://github.com/brocade-apj/workshops/blob/master/odl/images/md-sal-workflow.jpg?raw=true)

The sequence details are as follows:

1. A packet_in is sent from the switch to controller
2. The OpenFlow java library parses the packet and translates it into an MD-SAL format governed by the OpenFlow protocol model; the OpenFlow 1.0/1.3 Plugin creates an MD-SAL Notification and publishes it into the MD-SAL.
3. MD-SAL routes the notification to all registered consumers, in this case the Learning Switch application
4. If the Learning Switch application does not yet have the mapping between the mac address and the port, it floods the packet. Flooding is done as pkt_out to the switch. The application issues an MD-SAL RPC (with input parameter that constitutes the pkt_out PDU payload).
5. MD-SAL routes the RPC to the OpenFlow 1.0/1.3 Plugin
6. The OpenFlow 1.0/1.3 Plugin processes the packet, which is eventually translated into an OpenFlow pkt_out PDU in the OpenFlow Java library.
7. The OpenFlow pkt_out PDU is sent to the switch.
8. When the application wants to program a flow, it creates a new flow in the MD-SAL Database. A new flow is created in the appropriate table (Table 0) , which is in the appropriate flow-capable node (i.e switch, openflow:1 in the example), which is in the config space of the MD-SAL inventory database. The inventory database is a subtree (namespace) of the MD-SAL database.
9. MD-SAL generates a change notification, which is received by the Forwarding Rules Manager (FRM). The FRM listens on flow updates - i.e. it registered for MD-SAL change notifications on the config name space subtree that corresponds to flows.
10. The FRM programs the flow. It issues an MD-SAL RPC flow programming operation.
11.  MD-SAL routes the RPC to the OpenFlow 1.0/1.3 Plugin
12. The OpenFlow 1.0/1.3 Plugin processes the packet, which is eventually translated into an OpenFlow FlowMod PDU in the OpenFlow Java library.
13. The OpenFlow FlowMod PDU is sent to the switch.

 