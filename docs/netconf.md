# Netconf

## What is Netconf

The **Network Configuration Protocol** (**NETCONF**) is a network management protocol that defines a simple mechanism through which a network device can be managed, configuration data information can be retrieved, and new configuration data can be uploaded and manipulated. NETCONF operations are realized on top of a **Remote Procedure Call (RPC)** layer using an **XML** encoding and provides a basic set of operations to edit and query configuration on a network device.

## Overview 

The NETCONF protocol can be conceptually partitioned into four layers:

* The Content layer consists of configuration data and notification data.
* The Operations layer defines a set of base protocol operations to retrieve and edit the configuration data.
* The Messages layer provides a mechanism for encoding remote procedure calls (RPCs) and notifications.
* The Secure Transport layer provides a secure and reliable transport of messages between a client and a server.

![NETCONF Layers](https://github.com/brocade-apj/workshops/blob/master/odl/images/netconf_layers.png?raw=true)

NETCONF uses an RPC-based communication paradigm.  A client sends a series of one or more RPC request messages, which cause the server to respond with a corresponding series of RPC reply messages. NETCONF exposes a list of [operations](###operations) to manage configuration datastore and retrive device state information.

NETCONF is connection-oriented, requiring a persistent connection between peers.  This connection MUST provide reliable, sequenced data delivery.  NETCONF connections are long-lived, persisting between protocol operations.

NETCONF supports at least SSH communications but devices could implement others based on HTTP/TLS, BEEP/TLS, ...


### Key Components of Netconf

* Operations sent by Remote Procedure Call (**RPC**)
* Encodes are based on **XML**
* Each device exposes its configuration modeled by **YANG**
* Secure transport SSH, TLS, BEEP/TLS, SOAP/HTTP/TLS, ...


## RPC Model

The NETCONF protocol uses an RPC-based communication model. There is a rpc request element and rpc reply:


* **rpc request**: The rpc request (\<rpc\>) element is used to enclose a NETCONF request sent from the client to the server
* **rpc reply**: The rpc reply (\<rpc-reply\>) message is sent in response to an \<rpc\> message. This element could contain and error, a sucessful answer with no data and a sucessful answer with data.
	* The <rpc-error> element is sent in \<rpc-reply\> messages if an error occurs during the processing of an \<rpc\> request.
	* The \<ok\> element is sent in \<rpc-reply\> messages if no errors or warnings occurred during the processing of an \<rpc\> request, and no data was returned from the operation.

   
## Operations

The **NETCONF** protocol provides a set of operations to manage device configurations and retrieve device  information. A device could extend the protocol and expose other operations.

NETCONF protocol provides protocol operations:

* **get**: Retrieve running configuration and device state information.
* **get-config**: Retrieve all or part of a specified configuration datastore.
* **edit-config**: Edit a configuration datastore by creating, deleting, merging or replacing content.
* **copy-config**: Create or replace an entire configuration datastore with the contents of another complete configuration datastore.
* **delete-config**: Delete a configuration datastore.
* **lock**: Lock the entire configuration datastore system of a device.
* **unlock**: Release a configuration lock, previously obtained with the <lock> operation.
* **close-session**: Request graceful termination of a NETCONF session.
* **kill-session**: Force the termination of a NETCONF session.


## Capabilities

A NETCONF device and client could extend NETCONF protocol to provide enhanced capabilities. Client and server exchange hello messages which include its capabilities.  Each peer needs to understand only those capabilities that it might use and ignore the rest.
Following list contains capabilitity types:

* **Writable-Running Capability**: indicates that the device supports direct writes to the <running> configuration datastore.
* **Candidate Configuration Capability**: indicates that the device supports a candidate configuration datastore, which is used to hold configuration data that can be manipulated without impacting the device's current configuration.
* **Confirmed Commit Capability**: indicates that the server will support the commits operations, like confirmed, cancel commit, etc.
* **Rollback-on-Error Capability**: indicates that the server will support the rollback-on-error in edit config operation.
* **Validate Capability**: indicates that the server will support validation of the new configuration before applying it.
* **URL Capability**: indicates that peer has the ability to accept the url element in source and target parameters.
* **XPath Capability**: indicates that the NETCONF peer supports the use of XPath expressions in the filter element.


### Examples


**Get config**

```
<rpc message-id="101" xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
 <get-config>
   <source>
     <running/>
   </source>
   <!-- get the user named fred -->
   <filter xmlns:t="http://example.com/schema/1.2/config"
           type="xpath"
           select="/t:top/t:users/t:user[t:name='fred']"/>
  </get-config>
</rpc>

<rpc-reply message-id="101"
          xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
 <data>
   <top xmlns="http://example.com/schema/1.2/config">
     <users>
       <user>
         <name>fred</name>
         <company-info>
           <id>2</id>
         </company-info>
       </user>
     </users>
   </top>
 </data>
</rpc-reply>
```

**Copy config**

```
<rpc message-id="101" xmlns="urn:ietf:params:xml:ns:netconf:base:1.0">
 <copy-config>
   <target>
     <startup/>
   </target>
   <source>
     <running/>
   </source>
 </copy-config>
</rpc>
```     
