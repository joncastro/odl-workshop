# OpenFlow

## What is OpenFlow
OpenFlow is a set of protocols that enable a OpenFlow controller to determine the path of a network frame or packet by pushing changes to the data plane of an OpenFlow enabled network device. This enables the behaviour of the network to be controlled dynamically and programatically.

### Key Components of OpenFlow

* Separation of the control and data planes
* Control plane to be managed on a logically centralised controller system
* Standardised protocol between controller and an agent on the network device for instantiating forwarding state
* Providing network programmability from a centralised view via modern, extensible APIs

### Open Networking Foundation (ONF)

The Open Networking Foundation (ONF) was formed by a group of service providers to commercialise, standardise, and promote the use of OpenFlow in production environments. They oversee and maintain the development of OpenFlow specifications which includes all standards that define OpenFlow protocols.


## OpenFlow Management and Configuration Protocol (OF-CONFIG 1.2.x)

The OpenFlow Management and Configuration protocol (OF-CONFIG) is a companion protocol to the OpenFlow Switch Protocol. The OF-CONFIG protocol enables the remote configuration of OpenFlow switches whether physical or virtual in an OpenFlow environment.

While the OpenFlow Switch Protocol generally operates on a timescale of a flow (i.e. as flows are added and deleted), OF-CONFIG operates on a slower timescale. An example is building forwarding tables and deciding forwarding actions which are done via the OpenFlow Switch Protocol while enabling/disabling a port generally does not need to be done at the timescale of a flow, hence is done via the OF-CONFIG protocol.

The OF-CONFIG protocol is bound to	the	NETCONF operations and transport standard for network device configuration and management and thus relies on the use of YANG data models.

## OpenFlow Switch Protocol (1.5.x)

The OpenFlow Switch Protocol commonly referred to as simply the OpenFlow Protocol is used between a OpenFlow controller and an OpenFlow Logical Switch to allow a OpenFlow controller to add, update and delete flow entries in the switches flow tables, both reactively (i.e. in response to packets) or proactively. These flow entries determine the forwarding behaviour of incoming frames or packets.

#### Terminology
The following is a list of terms used throughout this topic.

##### OpenFlow/OpenFlow Protocol/OpenFlow Switch Protocol
The terms OpenFlow, OpenFlow Protocol and OpenFlow Switch Protocol will be used interchangeably for here on in unless specified otherwise, as the OpenFlow Switch Protocol is the primary protocol referred to in conversations about OpenFlow.

##### OpenFlow Capable Switch
A network device virtual or physical whose control plane software has implemented the OpenFlow Protocol is referred to as a OpenFlow Capable Switch. An OpenFlow Capable Switch can host one or more OpenFlow Logical Switches by partitioning a set of OpenFlow related resource such as ports and queues.

##### OpenFlow Logical Switch
A OpenFlow Logical Switch is a set of OpenFlow related resources (ports) from an OpenFlow Capable Switch which can be associated with a OpenFlow Controller. The OpenFlow Switch Protocol works directly with an OpenFlow Logical Switch over the OpenFlow channel.

##### OpenFlow Channel
The OpenFlow Channel is the interface that connects each OpenFlow Logical Switch to an OpenFlow controller. Through this interface, the controller configures and manages the switch, receives events from the switch, and sends packets out the switch. The OpenFlow channel is usually encrypted using TLS, but may be run directly over TCP

##### OpenFlow Controller
An OpenFlow Controller is software which controls OpenFlow Logical Switches via the OpenFlow protocol.

### How OpenFlow works

Each OpenFlow Logical Switch maintains a number of flow tables and a group table, which performs packet lookups and forwarding. It also contains one or more OpenFlow Channels to an external OpenFlow controller. The controller manages the switch through the OpenFlow Channels using the OpenFlow protocol.

#### OpenFlow Pipeline Processing

Pipeline processing happens in two stages, ingress processing and egress processing.

Egress processing is optional. The two stages of processing utilise different flow tables.

**Note:** Table numbering on egress tables (if it exists) must be greater than the last ingress table number. That is:

    iftn = ingress flow table number
    eftn = egress flow table number
    
    eftn > iftn
    first eftn = last iftn + 1

![OpenFlow Pipeline Processing](https://github.com/brocade-apj/workshops/blob/master/odl/images/of-pipeline-processing.png?raw=true)

##### Ingress Pipeline Processing

When a frame or packet arrives on an OpenFlow Port of a OpenFlow Logical Switch it's packet headers are extracted, and packet pipeline fields retrieved.

In addition to packet headers, matches can also be performed against the ingress port the packet was received, the metadata field and any other pipeline fields. See [match fields](#match-fields) for more information.

The packet header fields and pipeline fields represent the packet in its current state and can be modified by the *Apply-Actions* instruction.

The packet header and pipeline fields are then compared to [flow entries](#flow-entries) in [flow tables](#flow-tables). Matching begins at the first flow table and may continue to additional flow tables in the pipeline.

Packets are matched based on flow entry priority, with the highest priority matching entry in each table being selected.

###### Match Not Found

If a match is **not** found the outcome depends on the configuration of the table-miss flow entry. The table-miss flow entry determines if the packet is to be forwarded to the controller over the OpenFlow Channel, dropped, or to continue to the next flow table.

The table-miss flow entry behaves in most ways like any other flow entry: it does not exist by default in a flow table, the controller may add it or remove it at any time.

Like a regular flow table entry it matches packets in the table as expected from its set of match fields and priority, with the highest priority being selected. 

When a table-miss flow entry is found the instructions in the instructions field are carried out. 

If the table-miss flow entry does not exist, by default packets unmatched by flow entries are dropped. This behaviour can be overridden and a different behaviour can be specified such as sending packets to a controller.

###### Match Found

When a match **is** found, the flow [counter](#counters) is incremented and the specified set of instructions in the [instructions](#instructions) field are carried out.

The [actions](#actions) contained in the instructions field describe packet forwarding, packet modification and group table processing.

Pipeline processing instructions allow packets to be sent to subsequent tables for further processing and allow information, in the form of metadata, to be communicated between tables.

Table pipeline processing stops when the instruction set associated with a matching flow entry does not specify a next table; at this point the packet is usually modified and forwarded.

Flow entries may forward to a port. This is usually a physical port, but it may also be a logical port defined by the switch or a reserved port defined by the OpenFlow Switch specification.

[Reserved ports](#reserved-ports) may specify generic forwarding actions such as sending to the controller, flooding, or forwarding using non-OpenFlow methods, such as “normal” switch processing.

Actions associated with flow entries may also direct packets to a group, which specifies additional processing.

Groups represent sets of actions for flooding, as well as more complex forwarding semantics (e.g. multipath, fast reroute, and link aggregation).

The [group table](#group-table) contains group entries; each [group entry](#group-entry) contains a list of action buckets with specific semantics dependent on group type. The actions in one or more action buckets are applied to packets sent to the group.

Eventually an output action is specified in either a regular flow entry or in a group entry action bucket.

The output action forwards a packet to a specified OpenFlow port where it starts egress processing.

##### Egress Pipeline Processing

Egress processing is the processing that happens after the determination of the output port, it happens in the context of the output port and may involve zero or more egress flow tables. 

Egress processing follows much the same process as ingress processing. However the *GoTo-Table* action only forwards to egress flow tables, and there is no egress group table.

If there is no egress table, the ingress entry output action is applied directly. Otherwise egress processing occurs until the output action is found and executed.

Valid values for that output action are any physical or logical port, or the CONTROLLER or LOCAL reserved ports.

![OpenFlow Pipeline Processing Detailed](https://github.com/brocade-apj/workshops/blob/master/odl/images/of-pipeline-processing-detailed.png?raw=true)

### Glossary

#### <a name="flow-tables"></a>Flow Tables
All flow table are numbered and the first table is numbered zero.

Each flow table in the switch contains a set of flow entries.

Incoming packets are first compared to flow table entries in table zero.

#### <a name="flow-entry"></a>Flow Entry

Each flow entry contains the following fields:

![Flow Entry](https://github.com/brocade-apj/workshops/blob/master/odl/images/of-flow-entry.png?raw=true)

* **Match Fields**: to match against packets. These consist of the ingress port and packet headers, and optionally metadata specified by a previous table.
* **Priority**: matching precedence of the flow entry 
* **Counters**: to update for matching packets
* **Instructions**: to modify the action set or pipeline processing.
* **Timeouts**: maximum amount of time or idle time before flow is expired by the switch
* **Cookie**: opaque data value chosen by the controller. May be used by the controller to filter flow entries affected by flow statistics, flow modification and flow deletion requests. Not used when processing packets.
* **Flags**:  flags alter the way flow entries are managed, for example the flag OFPFF_SEND_FLOW_REM triggers flow removed messages for that flow entry.

A flow table entry is uniquely identified by the **match fields** and **priority**.

#### <a name="match-fields"></a>Match Fields
Match fields come in two types, header match fields and pipeline match fields.

##### <a name="header-match-fields"></a>Header Match Fields
Header match fields are match fields matching values extracted from the packet headers. Most header match fields map directly to a specific field in the packet header defined by a datapath protocol.

##### <a name="header-match-fields"></a>Pipeline Match Fields
Pipeline match fields are match fields matching values attached to the packet for pipeline processing and not associated with packet headers.

#### <a name="instructions"></a>Instructions
Each flow entry contains a set of instructions that are executed when a packet matches the entry. These instructions result in changes to the packet, action set and/or pipeline processing.

#### <a name="instruction-types"></a>Instruction Types

There are several instruction types, and not all have to be implemented by a switch. These optional instructions that may or may not be implemented by a OpenFlow switch are Apply-Actions, Write-Metadata, and Stat-Trigger.

The instruction set associated with a flow entry contains a maximum of one instruction of each type.

* Apply-Actions *action(s)* - Applies the specific action(s) immediately, without any change to the Action Set. This instruction may be used to modify the packet between two tables or to execute multiple actions of the same type. The actions are specified as a list of actions.
* Clear-Actions - Clears all the actions in the action set immediately. Support
of this instruction is required only for table-miss flow entries, and is optional for other flow entries.
* Write-Actions *action(s)* - Merges the specified set of action(s) into the current action set. If an action of the given type exists in the current set, overwrite it, otherwise add it. This instruction must be supported in all flow tables.
* Write-Metadata *metadata/mask* - Writes the masked metadata value into the metadata field. The mask specifies which bits of the metadata register should be modified
* Stat-Trigger *stat thresholds* - Generate an event to the controller if some of the flow statistics cross one of the stat threshold values.
* Goto-Table *next-table-id* - Indicates the next table in the processing
pipeline. The table-id must be greater than the current table-id. This instruction must be supported in all flow tables except the last one. The flow entries of the last table of the pipeline can not include this instruction.

#### <a name="actions"></a>Actions
Much like Instructions Actions have an action type, some are optional for an implementation of OpenFlow on a switch. Required actions must be supported in all flow tables.

* Output *port_no* - The Output action forwards a packet to a specified OpenFlow port where it starts egress processing. OpenFlow switches must support forwarding to physical ports, switch-defined logical ports and the required reserved ports
* Group *group_id* - Process the packet through the specified group. The
exact interpretation depends on group type.
* Drop - There is no explicit action to represent drops. Instead, packets whose action sets have no output action and no group action must be dropped.
* Set-Queue *queue_id* - The set-queue action sets the queue id for a packet. Used to provide basic Quality-of-Service (QoS) support.
* Meter *meter_id* - Direct packet to the specified meter.
* Push-Tag/Pop-Tag *ethertype* - Switches may support the ability to push/pop tags.
* Set-Field *field_type* *value* - The various Set-Field actions are identified by their field type and modify the values of respective header fields in the packet.
* Copy-Field *src_field_type* *dst_field_type* - The Copy-Field action may copy data between any header or pipeline fields.
* Change-TTL *ttl* -  The various Change-TTL actions modify the values of the IPv4
TTL, IPv6 Hop Limit or MPLS TTL in the packet.

#### <a name="action-set"></a>Action Set

An action set is associated with each packet. This set is empty by default. A flow entry can modify the action set using a *Write-Action* instruction or a *Clear-Action* instruction associated with a particular match.

The action set is carried between flow tables. When the instruction set of a flow entry does not contain a Goto-Table instruction, pipeline processing stops and the actions in the action set of the packet are executed.

An action set contains a maximum of one action of each type. If multiple actions of the same type are required, e.g. pushing multiple MPLS labels or popping multiple MPLS labels, the Apply-Actions instruction should be used.

The actions in an action set are applied in the order specified below, regardless of the order that they were added to the set. The switch may support arbitrary
action execution order through the list of actions of the Apply-Actions instruction

1. copy TTL inwards: apply copy TTL inward actions to the packet
2. pop: apply all tag pop actions to the packet
3. push-MPLS: apply MPLS tag push action to the packet
4. push-PBB: apply PBB tag push action to the packet
5. push-VLAN: apply VLAN tag push action to the packet
6. copy TTL outwards: apply copy TTL outwards action to the packet
7. decrement TTL: apply decrement TTL action to the packet
8. set: apply all set-field actions to the packet
9. qos: apply all QoS actions, such as meter and set queue to the packet
10. group: if a group action is specified, apply the actions of the relevant group bucket(s) in the
order specified by this list
11. output: if no group action is specified, forward the packet on the port specified by the output
action


#### <a name="group-table"></a>Group Table

A group table consists of group entries. The ability for a flow entry to point to a group enables OpenFlow to represent additional methods of forwarding.

#### <a name="group-entry"></a>Group Entry

Each group entry is identified by its group identifier and contains:

* Group Identifier - a 32 bit unsigned integer uniquely identifying the group on the OpenFlow switch.
* Group Type - to determine group semantics
    * Indirect -  Execute the one defined bucket in this group. This group supports only a single bucket 
    * All - Execute all buckets in the group. This group is used for multicast or broadcast forwarding
    * Select -  Execute one bucket in the group. Packets are processed by a single bucket in the group, based on a switch-computed selection algorithm
    * Fast Failover - Execute the first live bucket. Each action bucket is associated with a specific port and/or group that controls its liveness
* Counters - updated when packets are processed by a group.
* Action Buckets - an ordered list of action buckets, where each action bucket contains a set of actions to execute and associated parameters. The actions in a bucket are always applied as an action set

A group entry can have 0 or more Action Buckets except for the Group Type *indirect* that always has 1 bucket. A group with no buckets drops the packet.

A bucket typically contains actions that modify the packet and an output action that forwards it to a port. A bucket may also include a group action which invokes another group if the switch supports group chaining.

#### <a name="counters"></a>Counters
Counters are maintained for each flow table, flow entry, port, queue, group, group bucket, meter and meter band.

* Duration refers to the amount of time the flow entry, a port, a group, a queue or a meter has been installed in the switch, and must be tracked with second precision.
* The Receive Errors field is the total of all receive and collision errors

#### <a name="reserved-ports"></a>Reserved Ports
They specify generic forwarding actions such as sending to the controller, flooding, or forwarding using non OpenFlow methods, such as “normal” switch processing.

* **ALL:** Packet starts egress processing on all standard ports, excluding the packet ingress port and ports that are configured OFPPC_NO_FWD
* **CONTROLLER:** Represents the control channel with the OpenFlow controllers. When used as an output port, encapsulates the packet in a packet-in message and sends it using the OpenFlow switch protocol otherwise when used as an ingress port, this identifies a packet originating from the controller.
* **TABLE:** Represents the start of the OpenFlow pipeline, only valid in an output action.  submits the packet to the first flow table so that the packet can be processed through the regular OpenFlow pipeline.
* **INPUT:** Represents the packet ingress port. Can be used only as an output port, sends the packet out through its ingress port
* **ANY:** Special value used in some OpenFlow requests when no port is specified (i.e. port is wildcarded). Can neither be used as an ingress port nor as an output port.
* **UNSET:** Special value to specify that the output port has not been set in the *Action-Set*. Can neither be used as an ingress port nor as an output port.
* **LOCAL:** Represents the switch’s local networking stack and its management stack. Can be used as an ingress port or as an output port.
* **NORMAL:** Represents forwarding using the traditional non-OpenFlow pipeline of the switch.  Can be used only as an output port and processes the packet using the normal pipeline.
* **FLOOD:** Represents flooding using the traditional non-OpenFlow pipeline of the switch. Can be used only as an output port, in general will send the packet out all standard ports, but not to the ingress port