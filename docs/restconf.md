# RESTCONF

## What is RESTCONF

**RESTCONF** is a HTTP based protocol for accessing data defined in YANG via REST providing CRUD(Create, Read, Update and Delete) operations. RESTCONF provides a simplify subset of funcionalitiy provided by NETCONF. RESTCONF is not intended to replace NETCONF, but rather provide an additional simplified interface that follows REST principles and is compatible with a resource-oriented device abstraction. 

## Overview 

The RESTCONF protocol is a REST interface which API contract is driven by YANG models. The server lists each YANG module it supports using the “ietf-yang-library” YANG module.

Data is classified as configuration or non-configuration based on YANG “config” statement. If data is classified as configuration it can be modified, if data is classified as non-configuration only read-only access will be provided.

RESTCONF provides an interface to manage a hierarchy of [resources](##resources) starting with the top-level API resource itself defined by YANG model. Each resource can be identified by an [URI](##URI Structure) with a set of allowed [operations](##Operations) on that data.


### Resources

A resource can be considered a collection of conceptual data. It can contain child nodes that are nested resources. A resource can contain zero or more nested resources.  A resource can be created and deleted independently of its parent resource, as long as the parent resource exists.


### URI Structure

Resources are represented with URIs following the structure for generic URIs. A RESTCONF operation is derived from the HTTP method and the request URI, using the following conceptual fields:

```
	<OP> /restconf/<path>?<query>
```
* **OP**: the HTTP method GET, PUT, POST, ...) identifying the RESTCONF operation requested by the client HTTP method.
* **restconf**: RESTCONF entry point.
* **path**: the path expression identifying the resource that is being accessed by the operation.
* **query**: the set of parameters associated with the RESTCONF message.
   
### Operations

The RESTCONF protocol uses HTTP methods to identify the CRUD operation requested for a particular resource.  The following list shows how the RESTCONF operations relate to NETCONF protocol operations:


* **GET**: get a resource
* **POST**: create a resource or invoke a remote procedure
* **PUT**: create or replace a resource
* **DELETE**: remove a resource


## Example

Following examples contains how to create a flow in an openflow device using Opendaylight openflow RESTCONF

HTTP PUT **http://{odl-ip}:8181/restconf/config/opendaylight-inventory:nodes/node/openflow:4/table/0/flow/1**

```
{
    "flow-node-inventory:flow": [
        {
            "id": "1",
            "priority": 200,
            "instructions": {
                "instruction": [
                    {
                        "order": 0,
                        "apply-actions": {
                            "action": [
                                {
                                    "order": 0,
                                    "drop-action": {}
                                }
                            ]
                        }
                    }
                ]
            },
            "match": {
                "ipv4-destination": "10.0.0.1/32",
                "ethernet-match": {
                    "ethernet-type": {
                        "type": 2048
                    }
                }
            },
            "flow-name": "flow1",
            "installHw": true,
            "cookie_mask": 255,
            "table_id": 0,
            "idle-timeout": 34,
            "barrier": false,
            "strict": false,
            "cookie": 3
        }
    ]
}
```
