# YANG

## What is YANG
YANG is a data modeling language for the NETCONF protocol and is used to model configuration data, operational state data and notification events of network elements. In addition it allows the definition of remote procedure calls (RPCs) that can be invoked on network elements. It is defined in [RFC 6020](https://tools.ietf.org/html/rfc6020).

Although the primary purpose of YANG is to model network elements to work with the NETCONF protocol its key components have been identified as advantageous in other contexts, one such context is the development of OpenDaylight SDN controller applications. OpenDaylight MD-SAL applications now use YANG to:

* Define the RPCs that will become RESTCONF APIs
* Specify the information (data) contained in the object being modeled
* Specify any notifications to be sent to interested listeners.

### Key Components of YANG

* Human readable, easy to learn representation
* Hierarchical configuration data models
* Reusable types and groupings (structured types)
* Extensibility through augmentation mechanisms
* Supports the definition of operations (RPCs)
* Formal constraints for configuration validation
* Data modularity through modules and submodules
* Versioning rules and development support


## Brief History of YANG
In the late 1980s network administers had a need for a common system for exchanging configuration and operational information with network devices in order to manage and control the network. The earliest common protocol was the [Simple Network Management Protocol](http://en.wikipedia.org/wiki/Simple_Network_Management_Protocol) (SNMP), which used a data modeling language called [Structure of Management Information](http://en.wikipedia.org/wiki/Structure_of_Management_Information) (SMI).

Updates to the SNMP and SMI protocol were made in order to fulfil the evolving needs of networks. However SNMP was primarily being used for network monitoring and not device management as equipment vendors did not provide the option to completely configure their devices via SNMP and it was difficult to use. The NETMOD working group was chartered to work on a network configuration protocol, which will better align with the needs of network operators and equipment vendors the result of this being the NETCONF protocol.

NETCONF required a "human-friendly" modeling language for defining the semantics of operational data, configuration data, notifications, and operations and as such YANG was developed and chosen as the data modeling language for NETCONF.

## YANG Primer

YANG models the hierarchical organization of data as a tree in which each node has a name, and either a value or a set of child nodes. YANG provides clear and concise descriptions of the nodes, as well as the interaction between those nodes.

### Module

YANG structures data models into modules and submodules.

A module contains the following statements:

#### Module Header Statements
The module header statements specifies the yang-version, namespace and prefix.

```
module acme-system {
    namespace "http://acme.example.com/system";
    prefix "acme";
```
#### Module Linkage Statements
The module linkage statements import modules and include submodules.

![YANG Module Linkage](https://github.com/brocade-apj/workshops/blob/master/odl/images/yang-module-linkage.png?raw=true)

##### Import

A module can **import** data from another **module**.

The "import" statement makes definitions from one module available inside another module or submodule. The mandatory "prefix" substatement assigns a prefix for the imported module that is scoped to the importing module or submodule.

```
module acme-system {
    import "yang-types" {
        prefix yang;
    }
```

##### Include

A module can **include** data from another **submodule**.

Modules are only allowed to include submodules that belong to that module, as defined by the "belongs-to" statement. When a module includes a submodule, it incorporates the contents of the submodule into the node hierarchy of the module.

```
module acme-system {
    include acme-types;
```

#### Module Meta information Statements
Module meta information describe the module and give information about the module itself.

```
module acme-system {
    organization "ACME Inc.";
    contact "joe@acme.example.com";
    description
        "The module for entities implementing the ACME system.";
```

#### Revision Statements
The revision statements give information about the history of the module

```
module acme-system {
    revision 2007-11-05 {
        description "Initial revision.";
    }
```
#### Definition Statements
The body of the module where the data model is defined should follow a format similar to that outlined by [RFC 6087](https://tools.ietf.org/html/rfc6087#appendix-B).

```
... Previous Module header statements ...

    // extension statements
    // feature statements
    // identity statements
    // typedef statements
    // grouping statements
    // data definition statements
    // augment statements
    // rpc statements
    // notification statements
}

```

---

### Types

YANG defines a set of built-in types, and has a type mechanism through which additional types may be defined.

#### Built-In Types


| Name                | Description                         | 
|---------------------|-------------------------------------| 
| binary              | Any binary data                     | 
| bits                | A set of bits or flags              | 
| boolean             | true or "false"                     | 
| decimal64           | 64-bit signed decimal number        | 
| empty               | A leaf that does not have any value | 
| enumeration         | Enumerated strings                  | 
| identityref         | A reference to an abstract identity | 
| instance-identifier | References a data tree node         | 
| leafref             | A reference to a leaf instance      | 
| string              | Human-readable string               |
| int{8,16,32,64}     | {8,16,32,64}-bit signed integer     |
| uint{8,16,32,64}    | {8,16,32,64}-bit unsigned integer   | 
| union               | Choice of member types              | 


#### Derived Types

Derived types can restrict their base type's set of valid values using mechanisms like range or pattern restrictions.

The "typedef" statement defines a new type that may be used locally in the module, in modules or submodules which include it, and by other modules that import from it. All derived types can be traced back to a YANG built-in type and MUST NOT have the name of one of the YANG built-in types.


```
typedef percent {
    type uint16 {
        range "0 .. 100";
    }
    description "Percentage";
}

```

---
### Data Definition Statements
---

### Container

A container node does not have a value, it holds related child nodes in the data tree.  The child nodes are defined in the container's substatements. The following statements can be used to define child nodes to the container:

* container
* leaf
* list
* leaf-list
* uses
* choice
* anyxml

The "container" statement is used to define an interior data node in the schema tree.  It takes one argument, which is an identifier, followed by a block of substatements that holds detailed container information.

There is only one instance of a container node in a module. It's presence can have special meaning and if a container has the "presence" statement, the container's existence in the data tree carries some meaning otherwise, the container is used to give some structure to the data, and it carries no meaning by itself.


```
container system {
    container services {
        container ssh {
            presence "Enables SSH";
            description "SSH service specific configuration";
             // more leafs, containers and stuff here...
        }
    }
}
```
### List

A list node is similar to a container in that it holds related child nodes, however can exist in multiple instances, with each instance being known as a list entry.

The "list" statement takes one argument, which is an identifier, followed by a block of substatements that holds detailed list information. A list entry is uniquely identified by the "key" sub statement.

The "key" statement, which MUST be present if the list represents configuration, takes as string argument that specifies a space-separated list of leaf identifiers of this list. All key leafs MUST be given values when a list entry is created, can be of any type (derived or built-in) EXCEPT the built-in type "empty".

```
list user {
    key name;
    leaf name {
        type string;
    }
    leaf class {
        type string;
        default viewer;
    }
}
```

### Leaf Node

A leaf node is an endpoint in the schema tree. Leaf nodes have a value, but cannot contain any child nodes.

#### Leaf

The "leaf" statement is used to define a single leaf node in the schema tree. It takes one argument, which is an identifier, followed by a block of substatements that holds detailed leaf information. A leaf node exists in zero or one instances in the data tree.

```
leaf host-name {
    type string;
    description "Hostname for this system"
}
```

#### Leaf List

The "leaf-list" statement is used to define an array of a particular type. It takes one argument, which is an identifier, followed by a block of substatements that holds detailed leaf-list information. Leaf-lists can be ordered. 

The values in a leaf-list MUST be unique.

```
leaf-list allow-user  {
    type string;
    description "A list of user name patterns to allow";
}
```

---

### Grouping

A Grouping is a reusable block of nodes, which may be used locally in the module, in modules that include it, and by other modules that import from it.

The "grouping" statement takes one argument, which is an identifier, followed by a block of substatements that holds detailed grouping information. If the grouping is defined at the top level of a YANG module or submodule, the grouping's identifier MUST be unique within the module. The "grouping" statement is not a data definition statement and, as such, does not define any nodes in the schema tree.

```
import ietf-inet-types {
    prefix "inet";
}

grouping endpoint {
    description "A reusable endpoint group.";
    leaf ip {
        type inet:ip-address;
    }
    leaf port {
        type inet:port-number;
    }
}
```

#### Using a Grouping

Once a grouping is defined, it can be referenced in a "uses" statement. A grouping MUST NOT reference itself, neither directly nor indirectly through a chain of other groupings.

The effect of a "uses" reference to a grouping is that the nodes defined by the grouping are copied into the current schema tree.

```
container http-server {
    leaf name {
        type string;
    }
    uses endpoint;
}
```

---

### Augment

YANG allows the augmentation (extension) of a data node in the schema tree from current or imported modules. Nodes can be inserted into the existing hierarchy, with the original or (augmented) module being unchanged. Inserted nodes appear in the context of the current (augmenter) modules namespace.

The "augment" statement takes a target node string as an argument to identify the node that is being augmented. The target node MUST be either a container, list, choice, case, input, output, or notification node. It is augmented with the nodes defined in the substatements that follow the "augment" statement.

```
augment http-server {
    leaf version {
        type string;
    }
}
```

---

### RPC

A RPC is a remote procedure call (invocation of a method on a remote system). The "rpc" statement defines an rpc node in the schema tree. It takes one argument, which is an identifier, followed by a block of substatements that holds detailed rpc information.

The RPC identifier is the name of the RPC method on the remote system. The RPC can be defined with input parameters that will be sent as arguments and output parameters that it expects from invoking the RPC.

#### Input

The "input" statement, which is optional, is used to define input parameters to the RPC operation. It does not take an argument. The substatements to "input" define nodes under the RPC's input node.

#### Output

 The "output" statement, which is optional, is used to define output parameters to the RPC operation. It does not take an argument. The substatements to "output" define nodes under the RPC's output node.
 
```
rpc rock-the-house {
    input {
        leaf zip-code {
            type string;
        }
    }
    output {
        leaf is-rocking {
            type boolean;
        }
    }
}
```

---

### Notification

YANG allows the modeling of notifications, by modelling a notification an event is being defined which can pertain to common events, such as a change in capabilities, or link failure that may impact management applications. In the context of an OpenDaylight MD-SAL application notification events are defined and subscribed to by listeners in order to take some action.

The "notification" statement defines a notification node in the schema tree. It takes one argument, which is an identifier, followed by a block of substatements that holds detailed notification information.

```
notification link-failure {
    description "A link failure has been detected";    
    container link {
        uses connection;
    }
}
```

---

## Additional Information

More information about YANG can be found at:

| Link | Description |
| --- | --- |
| [RFC 6020](https://tools.ietf.org/html/rfc6020)| The primary specification of the YANG data modeling language |
| [Yang Central](http://www.yang-central.org/twiki/bin/view/Main/WebHome) | Location for all things related to YANG, including tutorials, documents, examples |
| [NETCONF Central](http://www.netconfcentral.org/) | NETCONF-oriented but with much information about YANG, including RFCs and tutorials |
| [YANG Cheat Sheet](http://www.netconfcentral.org/papers/yang-cheat-sheet-v1.pdf)| Very useful list of YANG directives when developing YANG models|
| [NETCONF-YANG paper](http://www.tail-f.com/wordpress/wp-content/uploads/2013/02/Tail-f-Presentation-Netconf-Yang.pdf) | Tail-F document describing both NETCONF and YANG |
| [Tail-f YANG Videos](http://www.tail-f.com/confd-training-videos/)| Videos on YANG modeling and YANG + NETCONF concepts |
| [Tail-f YANG Primer](http://www.tail-f.com/wordpress/wp-content/uploads/2014/02/Tail-f-Instant-YANG.pdf)| Short primer on YANG |
