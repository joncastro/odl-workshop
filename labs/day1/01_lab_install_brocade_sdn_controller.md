# Lab: Install Brocade SDN Controller

## Prerequisites

Students requires running an Ubuntu Trusty machine. It can be a virtual machine over any virtual environment like VMware, VirtualBox, etc.

## Requirements

* Ubuntu version Trusty 64 bits
* Brocade SDN Controller binaries. They can downloaded from [my.broade.com](http://my.brocade.com)
	* SDN-Controller-2.0.1-Software.gz

## Overview

In this lab we will install Brocade SDN Controller which is based on [OpenDaylight](www.opendaylight.org). **OpenDaylight** is an open source Java based SDN controller. It's goal is to enable SDN and NFV for networks of any size and scale, OpenDaylight software is a combination of components including a fully pluggable controller, interfaces, protocol plug-ins and applications.

## Installation Steps

### JDK 1.7

Following commands shows how to install Open JDK 1.7 on Ubuntu server.

Update package list and install Java 1.7

```
sudo apt-get update
sudo apt-get install openjdk-7-jdk -y
```

Some Java applications reads JAVA_HOME value from the environment. It is recommended that we set JAVA_HOME variable in the user bashrc file. Edit .bashrc file

```
vi ~/.bashrc
```

and append the following line to the end of the bashrc file. Verify what is your java home folder, in many cases it will be `/usr/lib/jvm/java-7-openjdk-amd64/jre` as suggested in following line.

```
export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64/jre
```

### Zip and Curl

Install **Zip**

```
 sudo apt-get install zip unzip -y
```

Install **Curl**

```
 sudo apt-get install curl -y
```

### Nodejs

Install **NodeJs** apt key and repository

```
 curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | sudo apt-key add -
 curl -sL https://deb.nodesource.com/setup | sudo bash -
```

Install Nodejs

```
 sudo apt-get update
 sudo apt-get install nodejs -y
```
Create node symbolic link if it does not exists

```
 [ -f /usr/bin/node ] || sudo ln -s /usr/bin/nodejs /usr/bin/node
```

### SSH Server

Ensure ssh server is installed, if not install it

```
type sshd || sudo apt-get install openssh-server -y
```

### Unzip binaries

Unzip Brocade SDN Controller binary files in user home folder folder. root user cannot be used.

```
 tar -xzf SDN-Controller-2.0.1-Software.gz
```

### Install Brocade SDN Controller

Ensure JAVA_HOME environment is set

```
export JAVA_HOME=/usr/lib/jvm/java-7-oracle/jre
```

Execute **installbsc** command from uncompressed folder

```
cd SDN-Controller-2.0.1-Software
./installbsc -y
```

### Configure bsc service

Configure bsc service to start and stop Brocade SDN Controller as a linux service with **sudo service bsc stop** and **sudo service bsc start** commands.

Create a **/etc/init.d/bsc** file with following content, replacing **user="vagrant"** with the username used for BSC installation.

```
#!/bin/sh
### BEGIN INIT INFO
# Provides:          bsc
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start daemon at boot time
# Description:       Brocade SDN Controller service.
### END INIT INFO
#set -x
dir=""
user="vagrant"

name=`basename $0`
pid_file="/opt/bvc/$name.pid"
stdout_log="/opt/bvc/log/$name.start.log"
stderr_log="/opt/bvc/log/$name.start.err"

get_pid() {
    CURRENT_PID=`ps -ef | grep karaf | grep bvc | grep java | grep -v grep  | awk '{print $2}'`
    if [ ! -f $pid_file ]
     then
      echo $CURRENT_PID > $pid_file
    fi
    if [ "$CURRENT_PID" != "" ] && [ -f $pid_file ] && [ "$CURRENT_PID" != "`cat $pid_file`" ]
      then
      echo $CURRENT_PID > $pid_file
    fi
    cat "$pid_file"
}

is_running() {
    [ `ps -ef | grep karaf | grep bvc | grep java | grep -v grep  | wc -l` -ne 0 ]
}

case "$1" in
    start)
    if is_running; then
        echo "Already started"
    else
        echo "Starting $name"
        [ "" != "$dir" ] || cd "$dir"
        if [ "" != "$user" ]
          then
          sudo -u "$user" /opt/bvc/bin/start >> "$stdout_log" 2>> "$stderr_log" &
        else
          /opt/bvc/bin/start >> "$stdout_log" 2>> "$stderr_log" &
        fi

        sleep 4

        if is_running; then
           return 0
        fi
        #ps -ef | grep karaf | grep bvc | grep java | grep -v grep  | awk '{print $2}' > "$pid_file"

        echo "starting by controller script "
        if [ "" != "$user" ]
          then
          sudo -u "$user" /opt/bvc/controller/bin/start >> "$stdout_log" 2>> "$stderr_log" &
        else
          /opt/bvc/controller/bin/start >> "$stdout_log" 2>> "$stderr_log" &
        fi

        sleep 4

        if ! is_running; then
            echo "Unable to start, see $stdout_log and $stderr_log"
            exit 1
        fi
    fi
    ;;
    stop)
    if is_running; then
        echo -n "Stopping $name.."
        /opt/bvc/bin/stop
        for i in {1..10}
        do
            if ! is_running; then
                break
            fi

            echo -n "."
            sleep 1
        done

        if is_running; then
          kill -9 `get_pid`
        fi

        if is_running; then
            echo "Not stopped; may still be shutting down or shutdown may have failed"
            exit 1
        else
            echo "Stopped"
            if [ -f "$pid_file" ]; then
                rm "$pid_file"
            fi
        fi
    else
        echo "Not running"
    fi
    ;;
    kill)
    if is_running; then
        echo -n "Stopping $name.."
        kill -9 `get_pid`
        /opt/bvc/bin/stop
    else
        echo "Not running"
    fi
    ;;
    restart)
    $0 stop
    if is_running; then
        echo "Unable to stop, will not attempt to start"
        exit 1
    fi
    $0 start
    ;;
    status)
    if is_running; then
        echo "Running"
    else
        echo "Stopped"
        exit 1
    fi
    ;;
    *)
    echo "Usage: $0 {start|stop|restart|status}"
    exit 1
    ;;
esac

exit 0
```


### Restart Brocade SDN Controller

Stop and restart BSC

```
sudo service bsc stop
sudo service bsc start
```
