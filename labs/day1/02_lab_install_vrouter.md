# Lab: Install Brocade vRouter

## Prerequisites

Students requires a local or remote kvm or VMWare virtualization server. It is also required student being familiar how to create a virtual machine in the choosen virtualization server.

## Requirements

* [VMware Fusion](https://www.vmware.com/au/products/fusion) or [Vmware Workstation](http://www.vmware.com/au/products/workstation) or [KVM librvirt](https://libvirt.org/drvqemu.html)

## Overview

In this lab we will install a virtual Router. Brocade vRouter provides software-based virtual router, virtual firewall and VPN products for Internet Protocol networks (IPv4 and IPv6). vRouter delivers advanced routing for physical, virtual, and cloud networking environments. It includes dynamic routing, Policy-Based Routing (PBR), stateful firewall, VPN support, and traffic management in a solution optimized for virtualized environments. All features can be configured through a familiar, networkcentric Command Line Interface (CLI), a Web-based GUI, or external management systems such as Brocade SDN Controller.


---

## Installation steps

### Download Brocade vRouter

Download vRouter 5600 iso file available on from [Brocade](my.brocade.com) . This guide has been tested with following versions **vRouter 5600 3.2.1R6**:

* **vRouter 5600 3.2.1R6** _vyatta-livecd_3.2.1R6T180_amd64.iso_

--

### Create a new virtual machine

Create a new virtual machine with folowing options. This lab does not cover how to create a virtual machine. Further information can be found for [KVM Librvirt](http://wiki.libvirt.org/page/CreatingNewVM_in_VirtualMachineManager) or [VMWare Fusion](http://pubs.vmware.com/fusion-5/index.jsp#com.vmware.fusion.help.doc/GUID-3AC723D3-CB7A-4001-BD38-4E98ADB8EDED.html)

* Guest Operating System: Debian-7 64 bits
* Hard disk : At least 8GB
* Memory: At least 2GB
* Processors: At least 2.
* Interfaces: AT least 3
* Attach iso to the CD/DVD Drive

**Hypervisor specific paramateres**


* **VMWare**: it is required to modify the vmx file and change interface e1000 value for vmxnet3 in all interfaces
* **KVM Libvirt**: interfaces type must be set to virtio and CPU Model to Nehalem


### Install vRouter image

Start the virtual machine, connect to the console, execute **install image** and follow the installation steps.

Execute **install image**

```
vyatta@vyatta:~$ install image
```

Select the default image name presing enter

```
What would you like to name this image? [3.5R3]:
OK.  This image will be named: 3.5R3
Probing drives: OK
```

Select **Auto** partition pressing enter

```
The Vyatta image will require a minimum 1000MB root.
Would you like me to try to partition a drive automatically
or would you rather partition it manually with parted?  If
you have already setup your partitions, you may skip this step

Partition (Auto/Parted/Skip) [Auto]:
```

Install the image **sda** pressing enter

```
I found the following drives on your system:
 sda	8589MB


Install the image on? [sda]:
```

Confirm installation on sda entering **Yes**

```
This will destroy all data on /dev/sda.
Continue? (Yes/No) [No]: Yes
```

Press enter to select the default disk size (maximum by default)

```
How big of a root partition should I create? (1000MB - 8589MB) [8589]MB:

Creating filesystem on /dev/sda1: OK
Done!
Mounting /dev/sda1...
I need to install the GRUB boot loader.
I found the following drives on your system:
 sda	8589MB

```

Install GRUB on **sda** pressing enter

```
Which drive should GRUB modify the boot partition on? [sda]:

Setting up grub on /dev/sda:OK
Copying squashfs image...
Copying kernel and initrd images...
```

Press enter to choose default **/config/config.boot** config file.

```
I found the following configuration files:
    /config/config.boot
    /opt/vyatta/etc/config.boot.default
Which one should I copy? [/config/config.boot]:

Copying /config/config.boot...
```

Enter desired username. By default is **vyatta**.

```
Enter username for administrator account [vyatta]:
```


Enter password the password twice


```
Enter password for administrator account
Enter password for user 'vyatta':
Retype password for user 'vyatta':
Done.
vyatta@vyatta:~$
```

### Reboot vRouter

Disconnect iso file from the virtual machine and reboot vRouter executing **reboot** command. If iso is connected then virtual machine could start again the livecd instead of from the disk, which we have installed the vRouter. If virtual machine boot order starts with hard disk before cd then it will not matter if iso is connected or not.

```
vyatta@vyatta:~$ reboot
Proceed with reboot? (Yes/No) [No] Yes

The system is going down for reboot NOW!
vyatta@vyatta:~$
```

### Configure interfaces

Configure virtual machine interfaces. This guide supposes that all interfaces will obtain an ip address from a dhcp server in the network. It is possible to set ip address configuration manually too.

First, execute **show interfaces** command to see the available interfaces detected by vRouter. It should detect all the interfaces configured in the virtual machine.

```
vyatta@vyatta:~$ show interface
Codes: S - State, L - Link, u - Up, D - Down, A - Admin Down
Interface        IP Address                        S/L  Description
---------        ----------                        ---  -----------
dp0p33p1         -                                 u/u
dp0p34p1         -                                 u/u
dp0p35p1         -                                 A/D
vyatta@vyatta:~$
```


Enter configuration mode

```
vyatta@vyatta:~$ configure
[edit]
vyatta@vyatta#
```

For each inteface, configure dhcp service in order to get an ip address automatically from the dhcp server.

```
vyatta@vyatta# set interfaces dataplane dp0p33p1 address dhcp
[edit]
```

Commit configuration to make it active. After commmiting, ssh service is running.

```
vyatta@vyatta# commit
[edit]
vyatta@vyatta#
```

Save new configuration to make it permanent.

```
vyatta@vyatta# save
[edit]
vyatta@vyatta#
```

Exit from configuration mode

```
vyatta@vyatta# exit
vyatta@vyatta:~$
```




### Enable ssh

Enable ssh service to be able to connect to the vRouter remotely.

Enter configuration mode

```
vyatta@vyatta:~$ configure
[edit]
```

Enable ssh service

```
vyatta@vyatta# set service ssh
[edit]
vyatta@vyatta#
```

Enable netconf service

```
vyatta@vyatta# set service netconf
[edit]
vyatta@vyatta#
```

Commit configuration to make it active. After commmiting, ssh service is running.

```
vyatta@vyatta# commit
[edit]
vyatta@vyatta#
```

Save new configuration to make it permanent.

```
vyatta@vyatta# save
[edit]
vyatta@vyatta#
```

Exit from configuration mode

```
vyatta@vyatta# exit
vyatta@vyatta:~$
```

### Validate installation

Check Vyatta vRouter is accessible through ssh. First, execute **show interfaces** command to know the ip addres of the data plane

```
vyatta@vyatta:~$ show interface
Codes: S - State, L - Link, u - Up, D - Down, A - Admin Down
Interface        IP Address                        S/L  Description
---------        ----------                        ---  -----------
dp0p33p1         192.168.27.138/24                 u/u
dp0p34p1         -                                 u/u
dp0p35p1         -                                 A/D
vyatta@vyatta:~$
```


Connect via ssh to one of the interfaces via ssh. Use the username and password set in the [install image](#Install vRouter image) step.
