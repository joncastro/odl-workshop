# Lab NETCONF and vRouter in Brocade SDN Controller


## Prerequisites

Students should have already completed the following labs:

* "Install Brocade SDN Controller"
* "Install Brocade vRouter"



## Requirements

* Brocade vRouter 5600 3.2.1R6T (Do not use 3.5R3 or higher) Follow **Install Brocade vRouter** lab
* Brocade SDN Controller Follow **Install Brocade SDN Controller** lab
	* SDN-Controller-2.0.1-Software.gz

## Add vRouter to SDN Controller

SSH to SDN Controller

Students using ravello use the following credentials:

* Username: ubuntu
* Password: ubuntu

```
ssh ubuntu@sdn-controller-public-ip
```

Start SDN Controller if it is not running

```
sudo service bsc start
```

SSH to vRotuer

Use the following credentials:

* Username: vyatta
* Password: vyatta

```
ssh vyatta@vyatta-public-ip
```

Start vRouter and check available interfaces

```
vyatta@vyatta:~$ show interface
Codes: S - State, L - Link, u - Up, D - Down, A - Admin Down
Interface        IP Address                        S/L  Description
---------        ----------                        ---  -----------
dp0p33p1         192.168.27.138/24                 u/u
dp0p34p1         -                                 u/u
dp0p35p1         -                                 A/D
vyatta@vyatta:~$
```

In above example, only one interface has an ip address. We must use an interface accessible from SDN Controller. For further information about interfaces, please refer to [vRouter official documentation](http://www.brocade.com/en/products-services/software-networking/network-functions-virtualization/5600-vrouter.html)


### Enable NETCONF

SDN Controller connects to the vRouter using NETCONF. NETCONF Enable NETCONF on vRouter 5600. The **Network Configuration Protocol** (**NETCONF**) is a network management protocol that defines a simple mechanism through which a network device can be managed, configuration data information can be retrieved, and new configuration data can be uploaded and manipulated.

Login to vRouter and enter in **configure** mode

Enter configuration mode

```
vyatta@vyatta:~$ configure
[edit]
vyatta@vyatta#
```

Ensure the ssh and NETCONF services are running. They will be under a node named `service`.
```
vyatta@vyatta# show configuration
```

Set ssh and NETCONF services if not enabled

```
vyatta@vyatta# set service ssh
[edit]
```

Enable NETCONF service

```
vyatta@vyatta# set service netconf
[edit]
```

Commit configuration to make it active. After commiting, NETCONF service is running.

```
vyatta@vyatta# commit
[edit]
vyatta@vyatta#
```

Save new configuration to make it permanent.

```
vyatta@vyatta# save
[edit]
vyatta@vyatta#
```

Exit from configuration mode

```
vyatta@vyatta# exit
vyatta@vyatta:~$
```

### Mount vRouter to SDN Controller

Adding a netconf device SDN Controller can be done using SDN Controller RESTCONF interface.

Bellow example will use following values:

* **vrouter device name**: vrouter1
* **vrouter device ip**: 10.0.0.4
* **vrouter device ssh-port**: 22
* **vrouter device username**: vyatta
* **vrouter device password**: vyatta

If the device has not been added yet, an HTTP POST request should be send to following `http://{odl-ip}:8181/restconf/config/opendaylight-inventory:nodes/node/controller-config/yang-ext:mount/config:modules`

Ravello users the odl-ip should be `localhost` if you have created a tunnel

**Headers** must contain following three attribute. Opendaylight default REST API both user and password is `admin`.

```
Authorization: Basic YWRtaW46YWRtaW4=
Content-Type: application/json
Accept: application/json
```

**Payload** must contain follow json data. The payload contains device attribute information to be able to establish the netconf communication:

* device name: `"name": "vrouter1"`
* device ip address: `"odl-sal-netconf-connector-cfg:address": "10.0.0.4"`
* device ssh port: `"odl-sal-netconf-connector-cfg:port": 22`
* device user: `"odl-sal-netconf-connector-cfg:password": "vyatta"`
* device password:  `"odl-sal-netconf-connector-cfg:username": "vyatta"`

Ensure, your payload contains the appropiate values for your vRouter netconf device.

```
{
    "module": [
        {
            "type": "odl-sal-netconf-connector-cfg:sal-netconf-connector",
            "name": "vrouter1",
            "odl-sal-netconf-connector-cfg:keepalive-executor": {
                "name": "global-netconf-ssh-scheduled-executor",
                "type": "threadpool:scheduled-threadpool"
            },
            "odl-sal-netconf-connector-cfg:keepalive-delay": 120,
            "odl-sal-netconf-connector-cfg:address": "10.0.0.4",
            "odl-sal-netconf-connector-cfg:processing-executor": {
                "name": "global-netconf-processing-executor",
                "type": "threadpool:threadpool"
            },
            "odl-sal-netconf-connector-cfg:dom-registry": {
                "name": "dom-broker",
                "type": "opendaylight-md-sal-dom:dom-broker-osgi-registry"
            },
            "odl-sal-netconf-connector-cfg:binding-registry": {
                "name": "binding-osgi-broker",
                "type": "opendaylight-md-sal-binding:binding-broker-osgi-registry"
            },
            "odl-sal-netconf-connector-cfg:client-dispatcher": {
                "name": "global-netconf-dispatcher",
                "type": "odl-netconf-cfg:netconf-client-dispatcher"
            },
            "odl-sal-netconf-connector-cfg:between-attempts-timeout-millis": 2000,
            "odl-sal-netconf-connector-cfg:sleep-factor": 1.5,
            "odl-sal-netconf-connector-cfg:tcp-only": false,
            "odl-sal-netconf-connector-cfg:reconnect-on-changed-schema": false,
            "odl-sal-netconf-connector-cfg:connection-timeout-millis": 20000,
            "odl-sal-netconf-connector-cfg:port": 22,
            "odl-sal-netconf-connector-cfg:event-executor": {
                "name": "global-event-executor",
                "type": "netty:netty-event-executor"
            },
            "odl-sal-netconf-connector-cfg:max-connection-attempts": 0,
            "odl-sal-netconf-connector-cfg:password": "vyatta",
            "odl-sal-netconf-connector-cfg:default-request-timeout-millis": 60000,
            "odl-sal-netconf-connector-cfg:username": "vyatta"
        }
    ]
}
```





### Validate vRouter

To validate vRouter has been mounted properly it is required to check `http://{odl-ip}:8181/apidoc/explorer/index.html` web page, click on **Mounted Resources** tab, click on **opendaylight-inventory:nodes/node/{vrouter-device-name}** and ensure vRouter models are attached.



## Manage vRouter through RESTCONF

**RESTCONF** is a HTTP based protocol for accessingv data defined in YANG via REST providing CRUD(Create, Read, Update and Delete) operations. RESTCONF provides a simplify subset of funcionalitiy provided by NETCONF. RESTCONF is not intended to replace NETCONF, but rather provide an additional simplified interface that follows REST principles and is compatible with a resource-oriented device abstraction.

Spend some time checking vRouter yang models to understand available configuration and information through RESTCONF interfaces. The following steps will show some examples of get, add and delete configuration from the vRouter.

### Get services configured on vRouter

Get services configured in  vRouter requires to execute a GET request on `vyatta-services:service` model. This models contains all the configuration related to services.

Execute an HTTP GET on `http://{odl-ip}:8181/restconf/config/opendaylight-inventory:nodes/node/{vrouter-device-name}/yang-ext:mount/vyatta-services:service`.  `{vrouter-device-name}` must be replaced in the url for the device name given when thet netconf device was added to the controller.

Headers

```
Authorization: Basic YWRtaW46YWRtaW4=
Accept: application/json
Content-Type: application/json
```

At least, ssh and NETCONF service will be returned.

```
{
    "service": {
        "vyatta-service-ssh:netconf": "",
        "vyatta-service-ssh:ssh": {
            "port": [
                22
            ]
        }
    }
}
```

### Get current policies

vRouter configuration policies are located under `vyatta-policy:policy` model.

Executing an HTTP GET on `http://{odl-ip}:8181/restconf/config/opendaylight-inventory:nodes/node/{vrouter-device-name}/yang-ext:mount/vyatta-policy:policy/` url will return all policies configuration. If there are no policies configured in the vRouter device then the HTTP call will return 404 Code which means, no policies found.

The vRouter given in this lab has not been preconfigured with any policies. So, this is the expected the output.

Headers

```
Authorization: Basic YWRtaW46YWRtaW4=
Accept: application/json
Content-Type: application/json
```

### Add a new policy

Sending following HTTP POST request to the vRouter will add a new policy if it does not exist yet in the device.

HTTP POST `http://{odl-ip}:8181/restconf/config/opendaylight-inventory:nodes/node/{vrouter-device-name}/yang-ext:mount/vyatta-policy:policy`

Headers

```
Authorization: Basic YWRtaW46YWRtaW4=
Accept: application/json
Content-Type: application/json
```

```
{
  "vyatta-policy-route:route": {
    "access-list": [
      {
        "tagnode": 14,
        "rule": [
          {
            "tagnode": 14,
            "source": {
              "any": ""
            },
            "description": "permit any",
            "destination": {
              "any": ""
            },
            "action": "permit"
          }
        ]
      }
    ]
  }
}
```

This action should return 204 code, which means the configuration has been added succesfully.

If you execute a HTTP GET request on the same url used for POST action it should return the same configuration sent before.

### Get an acces-list

It is possible to filter the results and request an `access-list` in particular instead of getting all access-list configuration. In above request we have added the access-list with `tag-node 14`. Tagnode is the unique identifier for each access-list so appending `vyatta-policy-route:route/access-list/14` to the previous url we will only obtain that access-list configuration in particular.

HTTP GET `http://{odl-ip}:8181/restconf/config/opendaylight-inventory:nodes/node/{vrouter-device-name}/yang-ext:mount/vyatta-policy:policy/vyatta-policy-route:route/access-list/14`

Headers

```
Authorization: Basic YWRtaW46YWRtaW4=
Accept: application/json
Content-Type: application/json
```


The expected output is

```
{
    "vyatta-policy-route:access-list": [
        {
        "tagnode": 14,
        "rule": [
          {
            "tagnode": 14,
            "source": {
              "any": ""
            },
            "description": "permit any",
            "destination": {
              "any": ""
            },
            "action": "permit"
          }
        ]
      }
    ]
}
```


### Get a rule

An access-list could contain a list of rules. Rules contains a `tagnode` attribute which is the unique identifier. If we just want to obtain a rule under an access-list we need to append to the previous url `rule/<tag-node-number>`, based in above example we should append `rule/14`.

HTTP GET `http://{odl-ip}:8181/restconf/config/opendaylight-inventory:nodes/node/{vrouter-device-name}/yang-ext:mount/vyatta-policy:policy/vyatta-policy-route:route/access-list/14/rule/14`

Headers

```
Authorization: Basic YWRtaW46YWRtaW4=
Accept: application/json
Content-Type: application/json
```


The expected output is

```
{
    "vyatta-policy-route:rule": [
        {
            "tagnode": 14,
            "source": {
              "any": ""
            },
            "description": "permit any",
            "destination": {
              "any": ""
            },
            "action": "permit"
          }
    ]
}
```


### Delete a policy rule

To delete a rule we just need to execute an HTTP DELETE and provide the url which uniquely identifies the rule. The url is exactly the same as we have used before in the previous get rule example.

HTTP DELETE `http://{odl-ip}:8181/restconf/config/opendaylight-inventory:nodes/node/{vrouter-device-name}/yang-ext:mount/vyatta-policy:policy/vyatta-policy-route:route/access-list/14/rule/14`

After deleting the rule, if you try execute the HTTP GET of the same url it should return a 404 code error, which means rule not found.

### Delete an access-list

Same as delete a rule, we just need to execute an HTTP DELETE provide the url which uniquely identifies the access-list. The url is exactly the same as we have used before in the previous get access-list example.

HTTP DELETE `http://{odl-ip}:8181/restconf/config/opendaylight-inventory:nodes/node/{vrouter-device-name}/yang-ext:mount/vyatta-policy:policy/vyatta-policy-route:route/access-list/14`


### Delete all policy configuration

Executing a HTTP DELETE request action on the `vyatta-policy:policy` model will remove all policy configuration.

HTTP DELETE `http://{odl-ip}:8181/restconf/config/opendaylight-inventory:nodes/node/{vrouter-device-name}/yang-ext:mount/vyatta-policy:policy`

### Dismount vRouter on SDN Controller

To dismount the vRouter issue a HTTP DELETE on the following URI.

```
http://{odl-ip}:8181/restconf/config/opendaylight-inventory:nodes/node/controller-config/yang-ext:mount/config:modules/module/odl-sal-netconf-connector-cfg:sal-netconf-connector/{vrouter-device-name}
```

`{vrouter-device-name}` must be replaced in the url for the device name given when thet netconf device was added to the controller.
