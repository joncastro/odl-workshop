# Lab Openflow with Mininet

## Prerequisites

Students should have already completed the following lab:

* "Install Brocade SDN Controller"

## Requirements

* Ubuntu version Trusty 64 bits
* Brocade SDN Controller Follow **Install Brocade SDN Controller** lab
	* SDN-Controller-2.0.1-Software.gz

## Proactive mode

Ensure **/opt/bvc/controller/etc/opendaylight/karaf/54-arphandler.xml** proactive flood mode is enabled in the SDN Controller. If not, set value to true and restart the SDN Controller.

```
<is-proactive-flood-mode>true</is-proactive-flood-mode>
```

## Install Mininet

[Mininet](http://mininet.org) creates a realistic virtual network, running real kernel, switch and application code, on a single machine (VM, cloud or native), in seconds, with a single command. Mininet is also a great way to develop, share, and experiment with OpenFlow and Software-Defined Networking systems.

There are many options to use Mininet.

* Download directly a (Mininet VM)[https://github.com/mininet/mininet/wiki/Mininet-VM-Images]
* Native installation from source
* Installation from Packages

In this lab, we will choose the second option and we will install Mininet directly using source repository.

First, ensure if mininet is not installed. If `sudo mn --version` command returns `command not found` it means mininet is not installed. If it returns a version number Mininet is installed so we do not need to follow the rest of the steps.

```
$ sudo mn --version
sudo: mn: command not found
$
```

Mininet can be installed in the same virtual machine that SDN Controller is running or in a separate vm. Choose a vm with Ubuntu Trusty 64 bits and install Mininet following next steps.

Install git client if it is not present

```
type git || sudo apt-get install git -y
```

Clone Mininet repository

```
git clone https://github.com/mininet/mininet.git
```

Set **2.2.1** tag. As of now (07/2015), **2.2.1** is the latest stable version.

```
cd mininet
git checkout tags/2.2.1
cd ..
```
Update git clone links to avoid connectivity issues using github.

```
sed -i 's/git:\/\/openflowswitch.org\/openflow.git/https:\/\/github.com\/mininet\/openflow.git/g' mininet/util/install.sh
sed -i 's/git:\/\/gitosis.stanford.edu\/oflops.git/https:\/\/github.com\/mininet\/oflops.git/g' mininet/util/install.sh
```

Execute installation script to install Openflow OVS and User switches.

```
mininet/util/install.sh -n3fv
```



## Create and manage Openflow networks

OpenFlow is a set of protocols that enable an OpenFlow controller to determine the path of a network frame or packet by pushing changes to the data plane of an OpenFlow enabled network device. This enables the behavior of the network to be controlled dynamically and programmatically.

Ensure Brocade SDN Controller is started

```
sudo service bsc start
```

### Create a simple Mininet network

Login into **http://{odl-ip}:9001** and check **Topology Manager**. It should be empty if this controller has not been used for other Openflow purposes.


Now, we are going to create a simple Mininet Openflow network with just one switch pointing to the controller. Following command must be executed in Mininet vm and it will create a switch with hosts connected. Ensure controller is accessible from Mininet and set the Controller ip in the following command.

```
$ sudo mn --controller=remote,ip=10.0.0.6,port=6633
*** Creating network
*** Adding controller
*** Adding hosts:
h1 h2
*** Adding switches:
s1
*** Adding links:
(h1, s1) (h2, s1)
*** Configuring hosts
h1 h2
*** Starting controller
c0
*** Starting 1 switches
s1 ...
*** Starting CLI:
```

Go back to Controller UI **http://{odl-ip}:9001** and check Topology Manager contains a new Openflow switch.

Now, we are going to stop the Mininet network executing **exit**, and after we will check Topology Manager does not contain anymore the Openflow switch.


```
mininet> exit
*** Stopping 1 controllers
c0
*** Stopping 2 links
..
*** Stopping 1 switches
s1
*** Stopping 2 hosts
h1 h2
*** Done
completed in 84.718 seconds
```

### Discover a Mininet network including hosts

Create a Mininet network with 4 linear Openflow Switches and validate on Controller UI that 4 new switches has been created connected linearly.

```
$ sudo mn --controller=remote,ip=10.0.0.6,port=6633 --topo=linear,4
*** Creating network
*** Adding controller
*** Adding hosts:
h1 h2 h3 h4
*** Adding switches:
s1 s2 s3 s4
*** Adding links:
(h1, s1) (h2, s2) (h3, s3) (h4, s4) (s2, s1) (s3, s2) (s4, s3)
*** Configuring hosts
h1 h2 h3 h4
*** Starting controller
c0
*** Starting 4 switches
s1 s2 s3 s4 ...
*** Starting CLI:
mininet>
```

Validate in Controller UI that there are four switches connected linearly in Topology Manager.

It is also possible to obtain the Topology through RESTCONF interface. This API can be used by external applications to analyze network devices and hosts, and how they are connected each other.

HTTP GET **http://{odl-ip}:8181/restconf/operational/network-topology:network-topology/topology/flow:1**

Returned json contains just switches information because host are not still discovered by the controller because traffic has not generated yet.


Execute pingall in Mininet. All hosts should be reachable and SDN Controller will discover the host connected to each switch. Validate host has been discovered properly in Topology Manager on SDN Controller UI.

```
> pingall
*** Ping: testing ping reachability
h1 -> h2 h3 h4
h2 -> h1 h3 h4
h3 -> h1 h2 h4
h4 -> h1 h2 h3
*** Results: 0% dropped (12/12 received)
mininet>
```

Execute again HTTP GET **http://{odl-ip}:8181/restconf/operational/network-topology:network-topology/** . Now topology also contains host information.

Topology returns two main objects:

* links: provides how nodes are connected one each other
* nodes: contains the list of nodes including their information (type and ports)



### Create new flows entries

The OpenFlow Protocol is used between a OpenFlow controller and an OpenFlow Logical Switch to allow a OpenFlow controller to add, update and delete flow entries in the switches flow tables, both reactively (i.e. in response to packets) or proactively. These flow entries determine the forwarding behavior of incoming frames or packets.

In this example, we will add a flow proactively to enable and disable connectivity to a host creating a flow entry to avoid any traffic from s4 switch to h1 host. This is an example of how firewalls rules can be set in the Edge to avoid having unwanted traffic in the network.

This example is a continuation of [previous step](#Discover a Mininet network including hosts). If previous step has not been executed then first create following linear network

```
$ sudo mn --controller=remote,ip=10.0.0.6,port=6633 --topo=linear,4
```

Keep a ping open from h4 to h1 that we will use to validate connectivity to host1.

```
mininet> h4 ping h1
PING 10.0.0.1 (10.0.0.1) 56(84) bytes of data.
64 bytes from 10.0.0.1: icmp_seq=1 ttl=64 time=0.605 ms
64 bytes from 10.0.0.1: icmp_seq=2 ttl=64 time=1.28 ms
```

Create a firewall rule to avoid any input traffic in host 1 (10.0.0.1) using RESTCONF interface in SDN Controller. Following rule will configured in the switch number 4 because the url contains `openflow:4` which it is the name of that switch.

HTTP PUT **http://{odl-ip}:8181/restconf/config/opendaylight-inventory:nodes/node/openflow:4/table/0/flow/1**

Header

```
Authorization: Basic YWRtaW46YWRtaW4=
Content-Type: application/json
Accept: application/json
```

Notice in the following payload we are providing two key components:

* **action**: `"drop-action": {}` which defines what to do with the packet matching the flow
* **match**: `"ipv4-destination": "10.0.0.1/32"` which identifies the traffic to apply the actions. In this case, any traffic with `10.0.0.1` ip destination.

```
{
    "flow-node-inventory:flow": [
        {
            "id": "1",
            "priority": 200,
            "instructions": {
                "instruction": [
                    {
                        "order": 0,
                        "apply-actions": {
                            "action": [
                                {
                                    "order": 0,
                                    "drop-action": {}
                                }
                            ]
                        }
                    }
                ]
            },
            "match": {
                "ipv4-destination": "10.0.0.1/32",
                "ethernet-match": {
                    "ethernet-type": {
                        "type": 2048
                    }
                }
            },
            "flow-name": "flow1",
            "installHw": true,
            "cookie_mask": 255,
            "table_id": 0,
            "idle-timeout": 34,
            "barrier": false,
            "strict": false,
            "cookie": 3
        }
    ]
}
```

After sending above HTTP PUT request, check h1 is not reachable form h4 anymore. Ping from h3 and h2 to h1 should work too because they do not go throw s4 switch.


Delete flow and validate h1 is reachable from h4 again.

HTTP DELETE **http://{odl-ip}:8181/restconf/config/opendaylight-inventory:nodes/node/openflow:4/table/0/flow/1**
