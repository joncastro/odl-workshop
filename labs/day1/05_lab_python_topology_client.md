# Lab: Topology python client

## Prerequisites

Students should have already completed the lab "Openflow with a Mininet network".

## Requirements

* [Python 2.7](https://www.python.org/downloads/)

On ubuntu

```
sudo apt-get install python
```


## Overview

In this lab we will be creating a python client to obtain Openflow nodes and topology using Opendaylight RESTConf API.

---

Following steps will create gradually a python Topology client to obtain an Openflow Topology from the SDN controller.

### Step1: Run Mininet network

Ensure Mininet is running. Check previous lab "Openflow with a Mininet network" for further details.

```
$ sudo mn --controller=remote,ip=10.0.0.6,port=6633
*** Creating network
*** Adding controller
*** Adding hosts:
h1 h2
*** Adding switches:
s1
*** Adding links:
(h1, s1) (h2, s1)
*** Configuring hosts
h1 h2
*** Starting controller
c0
*** Starting 1 switches
s1 ...
*** Starting CLI:
```

### Step2: Get full topology

First, create a simple client which will call the Opendaylight REST API to obtain the nodes topology.

Create a file called **topology.py** with the following content


```
#!/usr/bin/env python

import requests
import json

url = 'http://localhost:8181/restconf/operational/network-topology:network-topology/topology/flow:1'
headers = { 'Authorization': 'Basic YWRtaW46YWRtaW4=','Content-type': 'application/json', 'Accept': 'application/json' }
r = requests.get(url, headers=headers)

if (r.status_code != 200):
  print "ERROR: http code: %d" % r.status_code
  print "ERROR: http message: " + r.content
  exit(1)

print r.content
```

Execute the topology client. Ensure topology.py has execution permissions by executing `chmod u+x topology.py`

```
./topology.py
```

and it should now print unformatted json data containing the full openflow topology.



# Step 3: Parse and print json data

In order to manipulate properly the HTTP GET request coming from the Opendaylight controller we will convert the output into a json object.

In this step, we will create a json object and then format it nicely before printing it to the output (the 'json' library is used to do these functions - note that we import the json library at the beginning of the code).

Replace the line

```
print r.content
```

with

```
data = json.loads(r.content)
print json.dumps(data, indent=4)
```

Execute topology client

```
./topology.py
```

and it should produce a json formatted output containing the full topology information.

# Step 4: parse json object

We are going to print only the node-id from node objects instead of the full topology.  

Replace following line

```
print json.dumps(data, indent=4)
```
with

```
for node in data['topology'][0]['node']:
    print node['node-id']
```

# Step 5: get network topology

Can you try to print just the links?
