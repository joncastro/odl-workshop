# Lab: Flowmanager python client

This lab is divided in two parts. The first one will show how to quickly create and remove flows using minimal code. The second part will create a more complete python class to get and configure flows via command line.


# Part1: Add and remove a flow.

## Prerequisites

Students should have already completed the lab "Openflow with a Mininet network".

## Requirements

* [Python 2.7](https://www.python.org/downloads/)

On ubuntu

```
sudo apt-get install python
```

## Overview

In this lab we will create two python classes to add and remove a flow in a Openflow network using Opendaylight REST API.

### Step1: Run Mininet network

Ensure Mininet is running. Check previous lab "Openflow with a Mininet network" for further details.

```
$ sudo mn --controller=remote,ip=10.0.0.6,port=6633
*** Creating network
*** Adding controller
*** Adding hosts:
h1 h2
*** Adding switches:
s1
*** Adding links:
(h1, s1) (h2, s1)
*** Configuring hosts
h1 h2
*** Starting controller
c0
*** Starting 1 switches
s1 ...
*** Starting CLI:
```


### Step2: Add a flow

First, create a simple client which will call Opendaylight REST API to obtain add a flow to a switch.

Create a file called **flowadd.py** with following content.


```
#!/usr/bin/env python

import requests

data = {
    "flow": [
        {
            "id": "1",
            "priority": 200,
            "instructions": {
                "instruction": [
                    {
                        "order": 0,
                        "apply-actions": {
                            "action": [
                                {
                                    "order": 0,
                                    "drop-action": {}
                                }
                            ]
                        }
                    }
                ]
            },
            "match": {
               "ipv4-destination": "10.0.0.1/32",
                "ethernet-match": {
                    "ethernet-type": {
                        "type": 2048
                    }
                }
            },
            "flow-name": "flow1",
            "installHw": 'true',
            "cookie_mask": 255,
            "table_id": 0,
            "idle-timeout": 34,
            "barrier": 'false',
            "strict": 'false',
            "cookie": 3
        }
    ]
 }

url = 'http://127.0.0.1:8181/restconf/config/opendaylight-inventory:nodes/node/openflow:3/table/0/flow/1'
headers = { 'Authorization': 'Basic YWRtaW46YWRtaW4=','Content-type': 'application/json', 'Accept': 'application/json' }
r = requests.put(url, json=data, headers=headers)

if (r.status_code != 200):
  print "ERROR: http code: %d" % r.status_code
  print "ERROR: http message: " + r.content
  exit(1)

print "Succeded!"
```


Keep a ping from h3 to h1. Ping should work.

```
mininet> h3 ping h1
PING 10.0.0.1 (10.0.0.1) 56(84) bytes of data.
64 bytes from 10.0.0.1: icmp_seq=1 ttl=64 time=0.261 ms
64 bytes from 10.0.0.1: icmp_seq=2 ttl=64 time=0.326 ms
```


Execute flowadd.py client. Ensure flowadd.py has execution permissions executing `chmod u+x flowadd.py`

```
./flowadd.py
```

Ping from h3 to h1 should not work because we have added a drop action rule matching that ping traffic.


### Step3: Delete a flow

Create a simple client which will call Opendaylight REST API to remove a flow from a switch.

Create a file called **flowdelete.py** with following content.

```
#!/usr/bin/env python

import requests

url = 'http://127.0.0.1:8181/restconf/config/opendaylight-inventory:nodes/node/openflow:3/table/0/flow/1'
headers = { 'Authorization': 'Basic YWRtaW46YWRtaW4=','Content-type': 'application/json', 'Accept': 'application/json' }
r = requests.delete(url, headers=headers)

if (r.status_code != 200):
  print "ERROR: http code: %d" % r.status_code
  print "ERROR: http message: " + r.content
  exit(1)

print "Succeded!"
```

Execute flowdelete.py client. Ensure flowdelete.py has execution permissions executing `chmod u+x flowdelete.py`

```
./flowdelete.py
```

Ping from h3 to h1 should work again because we have removed the drop action rule matching that ping traffic.


# Part2: Flow manager CLI client

## Prerequisites

Students should have already completed the lab "Openflow with a Mininet network".

## Requirements

* Ubuntu Trust 14.04 64 Bit
* Python

```
sudo apt-get install python
```

* pip

```
sudo apt-get install python-pip
```

* pybvc

```
sudo pip install pybvc
```


## Overview

In this lab we will be creating and removing flows from an Openflow network using **Pybvc** library. This lab will use mainly Pyvbvc **Controller**, **OFSwitch**, **FlowEntry**, **Instruction**, **DropAction**, **Match**. Check following links to get a better understanding of the methods provided by these objects.

*  Controller object [controller.py](https://github.com/BRCDcomm/pybvc/blob/master/pybvc/controller/controller.py)
* Openflow objects [ofswitch.py](https://github.com/BRCDcomm/pybvc/blob/master/pybvc/openflowdev/ofswitch.py)

---

Following steps will create gradually a python flow manager client to add and remove flows entries to an Openflow network using the SDN controller.


### Step 1: YAML configuration file

Create **ctrl.yml** YAML configuration file to let python client know where is located Brocade SDN controller, user, password, etc.

```
#--------------------------------------
# Controller specific attributes
#--------------------------------------

# IP address
ctrlIp: 127.0.0.1

# Controller TCP port number
ctrlPort: 8181

# Admin user name
ctrlUser: admin

# Admin user password
ctrlPassword: admin

```

### Step2: Create basic flowmanager structure

First, create **flowmanager.py** with following content. It provides a command based client with arguments parsing. It will take the first argument and call the function if it exists.

```
#!/usr/bin/env python

import json
import argparse

from pybvc.controller.controller import Controller
from pybvc.common.utils import load_dict_from_file

from pybvc.openflowdev.ofswitch import OFSwitch
from pybvc.openflowdev.ofswitch import FlowEntry
from pybvc.openflowdev.ofswitch import Instruction
from pybvc.openflowdev.ofswitch import DropAction
from pybvc.openflowdev.ofswitch import Match
from pybvc.common.status import STATUS
from pybvc.common.constants import *


#-------------------------------------------------------------------------------
# Class 'Flowmanager'
#-------------------------------------------------------------------------------
class Flowmanager(object):
    """ Flowmanager commands executer """

    def __init__(self):
        self.prog = 'flowmanager'
        parser = argparse.ArgumentParser(
            prog=self.prog,
            description='Flow manager command line tool for interaction with OpenFlow Controller',
            usage="%(prog)s [-h] [-C <path>] <command> [<args>]\n"
                  "(type '%(prog)s -h' for details)\n"
                     "\nAvailable commands are:\n"
                     "\n   show-switch       Show network switch information"
                     "\n   show-flow       Show OpenFlow flows information"
                     "\n   delete-flow     Delete OpenFlow flows"
                     "\n   add-flow        Add OpenFlow flows"
                     "\n"
                     "\n  '%(prog)s help <command>' provides details for a specific command")

        parser.add_argument('-C', metavar="<path>", dest='ctrl_cfg_file',
                            help="path to the controller's configuration file (default is './ctrl.yml')",
                            default="./ctrl.yml")

        parser.add_argument('command', help='command to be executed')

        # following method will save on args the first argument
        # and re
        args, remaining_args = parser.parse_known_args()

        # Get Controller's attributes from configuration file
        self.ctrl  = self.get_controller(args.ctrl_cfg_file)
        if(self.ctrl == None):
            print "\n".strip()
            print ("Cannot find controller configuration file")
            print "\n".strip()
            exit(1)

        # Invoke method that is matching the name of sub-command argument
        # cmd variable contains the first argument
        cmd = args.command.replace('-', '_')

        if hasattr(self, cmd):
            getattr(self, cmd)(remaining_args)
        else:
            print "\n".strip()
            print ("Error, unrecognized command '%s'" % cmd)
            print "\n".strip()
            parser.print_help()
            exit(1)

    #---------------------------------------------------------------------------
    # get controller object given yaml configuration file
    #---------------------------------------------------------------------------
    def get_controller(self, file):
        ctrlProperties = {}
        if(load_dict_from_file(file, ctrlProperties) == False):
            print("Config file '%s' read error: " % f)
            return None
        try:
            ctrlIp = ctrlProperties['ctrlIp']
            ctrlPort = ctrlProperties['ctrlPort']
            ctrlUser = ctrlProperties['ctrlUser']
            ctrlPassword = ctrlProperties['ctrlPassword']
        except:
            print ("Some attribute is missing in ctrl.yml file")
            return None

        # create Controller object which represent an API provided
        # by pybvc to execute the most commons actions on the
        # SDN Controller
        return Controller(ctrlIp, ctrlPort, ctrlUser, ctrlPassword, None)


    #---------------------------------------------------------------------------
    # show network switch information
    #---------------------------------------------------------------------------
    def show_switch(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s show-switch [-s=SWITCHID|--switch=SWITCHID] [-v|--verbose]\n\n"
                  "Show OpenFlow Switch information\n\n"
                  "Options:\n"
                  "  -s, --switch    switch identifier\n")
        parser.add_argument('-s', '--switch', metavar = "<SWITCHID>")
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


        print ("TODO print switch '%s' switch" % args.switch)

    #---------------------------------------------------------------------------
    # show flow table information
    #---------------------------------------------------------------------------
    def show_table (self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s show-table [-s=SWITCHID|--switch=SWITCHID] [-t=TABLEID|--table=TABLEID] [-v|--verbose]\n\n"
                  "Show OpenFlow switch table information\n\n"
                  "Options:\n"
                  "  -s, --switch    switch identifier\n"
                  "  -t, --tableid   table identifier\n")
        parser.add_argument('-s', '--switch', metavar = "<SWITCHID>")
        parser.add_argument('-t', '--tableid', metavar = "<TABLEID>")
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


        print "TODO"

    #---------------------------------------------------------------------------
    # add flow to a table and switch
    #---------------------------------------------------------------------------
    def add_flow(self, arguments):
        print "TODO"

    #---------------------------------------------------------------------------
    # delete flow to a table and switch
    #---------------------------------------------------------------------------
    def delete_flow(self, arguments):
        print "TODO"


#-------------------------------------------------------------------------------
# main
#-------------------------------------------------------------------------------
if __name__ == "__main__":
    Flowmanager()
```

Execute flowmanager client and get familiar with the options

```
$ ./flowmanager.py -h
usage: flowmanager [-h] [-C <path>] <command> [<args>]
(type 'flowmanager -h' for details)

Available commands are:

   show-switch       Show network switch information
   show-flow       Show OpenFlow flows information
   delete-flow     Delete OpenFlow flows
   add-flow        Add OpenFlow flows

  'flowmanager help <command>' provides details for a specific command

Flow manager command line tool for interaction with OpenFlow Controller

positional arguments:
  command     command to be executed

optional arguments:
  -h, --help  show this help message and exit
  -C <path>   path to the controller's configuration file (default is
              './ctrl.yml')
```


# Step 3: print Openflow switch detail


Add a new method to print a switch detail give the node name. Following method use **OFSwitch** object to get switch information from the controller.

```
  #---------------------------------------------------------------------------
  # print node information
  #---------------------------------------------------------------------------
  def print_node(self, nodeName):

    # create openflow switch object
    ofswitch = OFSwitch(self.ctrl, nodeName)

    print ("\nNode information for: '%s'" % nodeName)

    # get ofswitch information
    result = ofswitch.get_switch_info()

    if(result.get_status().eq(STATUS.OK) == True):
        print ("\nSwitch information for: '%s'" % nodeName)
        print json.dumps(result.get_data(), indent=4)
    else:
       print ("\nSwitch information not found : '%s'" % nodeName)
       return

    # get ports brief information
    result = ofswitch.get_ports_brief_info()

    if(result.get_status().eq(STATUS.OK) == True):
        print ("\nBrief port info for: '%s'" % nodeName)
        print json.dumps(result.get_data(), indent=4)

    # get features information
    result = ofswitch.get_features_info()

    if(result.get_status().eq(STATUS.OK) == True):
        print ("\nFeatures for: '%s'" % nodeName)
        print json.dumps(result.get_data(), indent=4)
```

Then, append following lines at the end of then of **show_switch** method.

```
tableid = args.tableid
        if (tableid == None):
            tableid = 0

if (args.switch == None):
    result = self.ctrl.get_nodes_operational_list()
    if(result.get_status().eq(STATUS.OK) == True):
        for node in result.get_data():
            self.print_node(node)

else:
    self.print_node(args.switch)
```

Create a Mininet Openflow network using a linear topology. The server that contains the Mininet network must be able to connect to the SDN controller.

Replace following `10.0.0.6` ip address with your Controller ip address.

```
sudo mn --controller=remote,ip=10.0.0.6,port=6633 --topo=linear,4
```

Execute flowmanager client and analyze the output. It should print switch information

```
./flowmanager.py show-switch
```

or

```
./flowmanager.py show-switch -s openflow:1
```

# Step 4: print table flow entries


Create following **print_table** method to print the flows in the configuration data store. Configuration data store contains the configuration that controller should keep in the network. Controller keeps configuration data store synchronized with device configuration.

```
#---------------------------------------------------------------------------
# print node information
#---------------------------------------------------------------------------
def print_table(self, nodeName, tableid):

  # create openflow switch object
  ofswitch = OFSwitch(self.ctrl, nodeName)

  print ("\nTable information for node '%s' and table '%s'" % (nodeName, tableid))

  # get ofswitch information
  result = ofswitch.get_configured_flows(tableid)
  if(result.get_status().eq(STATUS.OK) == True):
      print "\nConfigured flows:"
      print json.dumps(result.get_data(), indent=4)
```

Additionally, it could also print operational flows. Operational information contains what is actually configured in the device.

```
  # get ofswitch information
  result = ofswitch.get_operational_flows(tableid)
  if(result.get_status().eq(STATUS.OK) == True):
      print "\nOperational flows:"
      print json.dumps(result.get_data(), indent=4)
```

Configuration data store contains the desire state in the network and operational data store contains the actual configuration in the network. In a normal state, which the device is up and running and the device can talk normally with the controller, configuration and operational will have the same information. If the device goes down, then operational data will not be available and as soon as the device restarts the controller will push the settings coming from the configuration data store and updating operational state.


Append following lines to the end of **show_table** method.

```
if (args.switch == None):
    result = self.ctrl.get_nodes_operational_list()
    if(result.get_status().eq(STATUS.OK) == True):
        for node in result.get_data():
            self.print_table(node,tableid)
else:
    self.print_table(args.switch,tableid)
```

Start Mininet network if it is not running, execute flowmanager client and analyze the output. Currently, configuration table is still empty because we have not configured yet any flow in the SDN Controller.

```
./flowmanager.py show-table
```

or

```
./flowmanager.py show-table -s openflow:1
```

or

```
./flowmanager.py show-table -s openflow:1 -t 0
```


# Step 5: add a flow entry

This step will provide the ability to add a flow entry which drops traffic to a particular host ip.

Replace **add_flow** method with the following content.

```
#---------------------------------------------------------------------------
# add flow to a table and switch
#---------------------------------------------------------------------------
def add_flow(self, arguments):
    # parse agurments
    parser = argparse.ArgumentParser(
        prog=self.prog,
        usage="%(prog)s show-table -s=SWITCHID|--switch=SWITCHID -t=TABLEID|--table=TABLEID"
                "-i=IP|--ip=IP -f=FLOWID|--flowid=FLOWID\n\n"
              "Show OpenFlow switch table information\n\n"
              "Options:\n"
              "  -s, --switch    switch identifier\n"
              "  -t, --tableid   table identifier\n"
              "  -i, --ip   ip address\n"
              "  -f, --flowid   flow id\n")
    parser.add_argument('-s', '--switch', metavar = "<SWITCHID>")
    parser.add_argument('-t', '--tableid', metavar = "<TABLEID>")
    parser.add_argument('-i', '--ip', metavar = "<IP>")
    parser.add_argument('-f', '--flowid', metavar = "<FLOWID>")
    parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

    args = parser.parse_args(arguments)
    if(args.usage):
        parser.print_usage()
        print "\n".strip()
        return

    ofswitch = OFSwitch(self.ctrl, args.switch)

    flow_entry = FlowEntry()
    flow_entry.set_flow_table_id(args.tableid)
    flow_entry.set_flow_id(args.flowid)
    flow_entry.set_flow_priority(flow_priority = 1000)

    # --- Instruction: 'Apply-actions'
    #     Action:      'Drop'
    instruction = Instruction(instruction_order = 0)
    action = DropAction(order = 0)
    instruction.add_apply_action(action)
    flow_entry.add_instruction(instruction)

    # --- Match Fields: Ethernet Type
    #                   IPv4 Destination Address
    match = Match()
    match.set_eth_type(ETH_TYPE_IPv4)
    match.set_ipv4_dst(args.ip)
    flow_entry.add_match(match)

    print ("\nFlow to send:")
    print flow_entry.get_payload()

    result = ofswitch.add_modify_flow(flow_entry)
    status = result.get_status()
    if(status.eq(STATUS.OK) == True):
        print ("Flow successfully added to the Controller")
    else:
        print ("\n")
        print ("Flow cannot be configured, reason: %s" % status.brief().lower())
```

Start Mininet network if it is not running and keep a ping from h4 to h1. Ping should work

```
mininet> h4 ping h1
PING 10.0.0.1 (10.0.0.1) 56(84) bytes of data.
64 bytes from 10.0.0.1: icmp_seq=1 ttl=64 time=0.261 ms
64 bytes from 10.0.0.1: icmp_seq=2 ttl=64 time=0.326 ms
```

Following command will add a drop flow entry to avoid any host in switch 4 to connect to h1. After, adding following entry h4 should not able to ping h1.

```
$ ./flowmanager.py add-flow -s openflow:4 -t 0 -i "10.0.0.1/32" -f 1

Flow to send:
{
    "flow-node-inventory:flow": {
        "id": "1",
        "instructions": {
            "instruction": [
                {
                    "apply-actions": {
                        "action": [
                            {
                                "drop-action": {},
                                "order": 0
                            }
                        ]
                    },
                    "order": 0
                }
            ]
        },
        "match": {
            "ethernet-match": {
                "ethernet-type": {
                    "type": 2048
                }
            },
            "ipv4-destination": "10.0.0.1/32"
        },
        "priority": 1000,
        "table_id": "0"
    }
}
Flow successfully added to the Controller
```

Host 1 (h1) should be accessible from other host like h2 and h3 because they do not go through switch 4. For example, host 3 should be able to ping.

```
mininet> h3 ping h1
PING 10.0.0.1 (10.0.0.1) 56(84) bytes of data.
64 bytes from 10.0.0.1: icmp_seq=1 ttl=64 time=0.318 ms
64 bytes from 10.0.0.1: icmp_seq=2 ttl=64 time=5.32 ms
```

# Step 6: remove a flow entry

Fill **delete_flow** method to remove flows from a given switch and table. Use **delete_flow** method on **OFSwitch** object to remove a flow from the configuration.

This lab expects students will be able to write the code to provide above features.
