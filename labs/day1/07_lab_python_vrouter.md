# Lab: vRouter python client

## Prerequisites

Students should have already completed the lab "NETCONF and vRouter in Brocade SDN Controller".

## Requirements

* Ubuntu Trust 14.04 64 Bit
* Brocade vRouter 5600
* Python

```
sudo apt-get install python
```

* pip

```
sudo apt-get install python-pip
```

* pybvc

```
sudo pip install pybvc
```


## Overview

In this lab we will be adding and removing NETCONF devices to the SDN Controller using **Pybvc** library. Also, the client will provide basic operations like get configurations and schemas. This lab will use mainly Pyvbvc **Controller**, **NetconfNode**, **VRouter5600**. Check following links to get a better understanding of the methods provided by these objects.

*  [controller.py](https://github.com/BRCDcomm/pybvc/blob/master/pybvc/controller/controller.py)
* [netconfnode.py](https://github.com/BRCDcomm/pybvc/blob/master/pybvc/controller/netconfnode.py)
* [vrouter5600.py](https://github.com/BRCDcomm/pybvc/blob/master/pybvc/netconfdev/vrouter/vrouter5600.py)

---

Following steps will create gradually a python netconf manager using a vRouter Netconf capable device and the SDN controller.


### Step 1: YAML configuration file

Create **ctrl.yml** YAML configuration file to let python client know where is located Brocade SDN controller, user, password, etc.

```
#--------------------------------------
# Controller specific attributes
#--------------------------------------

# IP address
ctrlIp: 127.0.0.1

# Controller TCP port number
ctrlPort: 8181

# Admin user name
ctrlUser: admin

# Admin user password
ctrlPassword: admin

```

### Step2: Create basic netconf client structure

First, create **netconfmanager.py** with following content. It provides a command based client with arguments parsing. It will take the first argument and call the function if it exists.

```
#!/usr/bin/env python

import json
import argparse

from pybvc.controller.controller import Controller
from pybvc.common.utils import load_dict_from_file
from pybvc.netconfdev.vrouter.vrouter5600 import VRouter5600
from pybvc.controller.netconfnode import NetconfNode
from pybvc.common.status import STATUS

#-------------------------------------------------------------------------------
# Class 'Netconfmanager'
#-------------------------------------------------------------------------------
class Netconfmanager(object):
    """ Netconf manager commands executer """

    def __init__(self):
        self.prog = 'netconfmanager'
        parser = argparse.ArgumentParser(
            prog=self.prog,
            description='NETCONF manager command line tool for interaction with OpenFlow Controller',
            usage="%(prog)s [-h] [-C <path>] <command> [<args>]\n"
                  "(type '%(prog)s -h' for details)\n"
                     "\nAvailable commands are:\n"
                     "\n   add-device           Add netconf capable device"
                     "\n   add-vrouter          Add a vRouter5600 netconf device"
                     "\n   delete-device        Delete netconf device"
                     "\n   delete-vrouter       Delete vRouter5600 netconf device"
                     "\n   get-status           Get netconf device status"
                     "\n   get-schemas          Get netconf device schemas"
                     "\n   get-vrouter-config   Get vRouter5600 configuration"
                     "\n"
                     "\n  '%(prog)s help <command>' provides details for a specific command")

        parser.add_argument('-C', metavar="<path>", dest='ctrl_cfg_file',
                            help="path to the controller's configuration file (default is './ctrl.yml')",
                            default="./ctrl.yml")

        parser.add_argument('command', help='command to be executed')

        # following method will save on args the first argument
        # and re
        args, remaining_args = parser.parse_known_args()

        # Get Controller's attributes from configuration file
        self.ctrl  = self.get_controller(args.ctrl_cfg_file)
        if(self.ctrl == None):
            print "\n".strip()
            print ("Cannot find controller configuration file")
            print "\n".strip()
            exit(1)

        # Invoke method that is matching the name of sub-command argument
        # cmd variable contains the first argument
        cmd = args.command.replace('-', '_')

        if hasattr(self, cmd):
            getattr(self, cmd)(remaining_args)
        else:
            print "\n".strip()
            print ("Error, unrecognized command '%s'" % cmd)
            print "\n".strip()
            parser.print_help()
            exit(1)

    #---------------------------------------------------------------------------
    # get controller object given yaml configuration file
    #---------------------------------------------------------------------------
    def get_controller(self, file):
        ctrlProperties = {}
        if(load_dict_from_file(file, ctrlProperties) == False):
            print("Config file '%s' read error: " % f)
            return None
        try:
            ctrlIp = ctrlProperties['ctrlIp']
            ctrlPort = ctrlProperties['ctrlPort']
            ctrlUser = ctrlProperties['ctrlUser']
            ctrlPassword = ctrlProperties['ctrlPassword']
        except:
            print ("Some attribute is missing in ctrl.yml file")
            return None

        # create Controller object which represent an API provided
        # by pybvc to execute the most commons actions on the
        # SDN Controller
        return Controller(ctrlIp, ctrlPort, ctrlUser, ctrlPassword, None)


    #---------------------------------------------------------------------------
    # add netconf device
    #---------------------------------------------------------------------------
    def add_device(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s add-device -n=NAME|--name=NAME -i=IP|--ip=IP -p=PORT|--port=PORT"
                  " -u=USER|--user=USER -c=CREDENTIALS|--credentials=CREDENTIALS\n\n"
                  "Add netconf device\n\n"
                  "Options:\n"
                  "  -n, --name        device name\n"
                  "  -i, --ip          device ip address\n"
                  "  -p, --port        netconf port\n"
                  "  -u, --user        user name\n"
                  "  -c, --credentials password\n")
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-i', '--ip', metavar = "<IP>", required=True)
        parser.add_argument('-p', '--port', metavar = "<PORT>", required=True)
        parser.add_argument('-u', '--user', metavar = "<USER>", required=True)
        parser.add_argument('-c', '--credentials', metavar = "<CREDENTIALS>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


        print ("TODO add netconf device '%s' " % args.name)

    #---------------------------------------------------------------------------
    # add vrouter netconf device
    #---------------------------------------------------------------------------
    def add_vrouter(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s add-vrouter -n=NAME|--name=NAME -i=IP|--ip=IP -p=PORT|--port=PORT"
                  " -u=USER|--user=USER -c=CREDENTIALS|--credentials=CREDENTIALS\n\n"
                  "Add vrouter netconf device\n\n"
                  "Options:\n"
                  "  -n, --name          device name\n"
                  "  -i, --ip          device ip address\n"
                  "  -p, --port        netconf port\n"
                  "  -u, --user        user name\n"
                  "  -c, --credentials password\n")
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-i', '--ip', metavar = "<IP>", required=True)
        parser.add_argument('-p', '--port', metavar = "<PORT>", required=True)
        parser.add_argument('-u', '--user', metavar = "<USER>", required=True)
        parser.add_argument('-c', '--credentials', metavar = "<CREDENTIALS>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


        print ("TODO add vrouter netconf device '%s' " % args.name)

    #---------------------------------------------------------------------------
    # delete netconf device
    #---------------------------------------------------------------------------
    def delete_device(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s delete-device -n=NAME|--name=NAME\n\n"
                  "Delete netconf device\n\n"
                  "Options:\n"
                  "  -n, --name          device name\n")
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


        print ("TODO delete netconf device '%s' " % args.name)

    #---------------------------------------------------------------------------
    # delete vrouter netconf device
    #---------------------------------------------------------------------------
    def delete_vrouter(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s delete-vrouter -n=NAME|--name=NAME\n\n"
                  "Delete vrouter netconf device\n\n"
                  "Options:\n"
                  "  -n, --name          device name\n")
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


        print ("TODO delete vrouter netconf device '%s' " % args.name)

    #---------------------------------------------------------------------------
    # get netconf device status
    #---------------------------------------------------------------------------
    def get_status(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s get-status -n=NAME|--name=NAME\n\n"
                  "Get netconf device status\n\n"
                  "Options:\n"
                  "  -n, --name          device name\n")
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return

        print ("TODO get status from a netconf device '%s' " % args.name)

    #---------------------------------------------------------------------------
    # get netconf device schemas
    #---------------------------------------------------------------------------
    def get_schemas(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s get-schemas -n=NAME|--name=NAME\n\n"
                  "Get netconf device schemas\n\n"
                  "Options:\n"
                  "  -n, --name          device name\n")
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


        print ("TODO get schemas from a netconf device '%s' " % args.name)

    #---------------------------------------------------------------------------
    # get vrouter configuration
    #---------------------------------------------------------------------------
    def get_vrouter_config(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s get-vrouter-config -n=NAME|--name=NAME\n\n"
                  "Get netconf device schemas\n\n"
                  "Options:\n"
                  "  -n, --name          device name\n")
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


        print ("TODO get vrouter configuration from device '%s' " % args.name)


#-------------------------------------------------------------------------------
# main
#-------------------------------------------------------------------------------
if __name__ == "__main__":
    Netconfmanager()
```

Execute netconfmanager client and get familiar with the options

```
./netconfmanager.py -h
usage: netconfmanager [-h] [-C <path>] <command> [<args>]
(type 'netconfmanager -h' for details)

Available commands are:

  add-device           Add netconf capable device
  add-vrouter          Add a vRouter5600 netconf device
  delete-device        Delete netconf device
  delete-vrouter       Delete vRouter5600 netconf device
  get-status           Get netconf device status
  get-schemas          Get netconf device schemas
  get-vrouter-config   Get vRouter5600 configuration

 'netconfmanager help <command>' provides details for a specific command

NETCONF manager command line tool for interaction with OpenFlow Controller

positional arguments:
 command     command to be executed

optional arguments:
 -h, --help  show this help message and exit
 -C <path>   path to the controller's configuration file (default is
             './ctrl.yml')
```


# Step 3: add and remove Netconf device


Fill **add_device** and **remove_device** methods on `netconfmanager.py` file using **Controller** and **NetconfNode** classes. **Controller** class provides **add_netconf_node** and **delete_netconf_node** which requires a NetconfNode object as an input argument.

Adding Netconf device just requires to append following lines to **add_device**. It requires to create NetconfNode object and call add_netconf_node

```
# create NETCONF Node
node = NetconfNode(self.ctrl, args.name, args.ip, args.port, args.user, args.credentials)

# add node to the controller
result = self.ctrl.add_netconf_node(node)

# check results
status = result.get_status()
if(status.eq(STATUS.OK)):
    print ("'%s' NETCONF node was successfully added to the Controller" % args.name)
else:
    print ("\n")
    print ("Error adding NETCONF device, reason: %s" % status.brief())
    print status.detailed()
```

Execute netconfmanager client adding a new device and check in http://[controller-ip]:8181/apidoc/explorer/index.html **Mounted Resources** tab if device appears in the list.

```
./netconfmanager.py add-device -n vyatta-11 -i 10.0.0.4 -p 22 -u vyatta -c vyatta
```

Removing is similar to add, it requires to create the NetconfNode object (in this case only name is necessary) and call delete_netconf_node method on the controller.


```
# create NETCONF Node
node = NetconfNode(self.ctrl, args.name, None, None, None, None, None)

# remove node from the controller
result = self.ctrl.delete_netconf_node(node)

# check results
status = result.get_status()
if(status.eq(STATUS.OK)):
    print ("'%s' NETCONF node was successfully removed from the Controller" % args.name)
else:
    print ("\n")
    print ("Error removing NETCONF device, reason: %s" % status.brief())
    print status.detailed()
```

Execute netconfmanager client deleting the device and check in http://[controller-ip]:8181/apidoc/explorer/index.html **Mounted Resources** tab device does not appear anymore.

```
./netconfmanager.py delete-device -n vyatta-11
```

# Step 4: adding and removing vrouter


It is also possible to use **VRouter5600** class to add and remove a vRouter netconf device in the controller. VRouter5600 extends NetconfNode, so the controller also accepts it in add_netconf_node and delete_netconf_node methods.

Fill **add_vrouter** and **delete_vrouter** method using the same code of lines defined above and just changing NetconfNode for VRouter5600.

Execute netconfmanager client adding a new vrouter and check in http://[controller-ip]:8181/apidoc/explorer/index.html **Mounted Resources** tab if device appears in the list.

```
./netconfmanager.py add-vrouter -n vyatta-11 -i 172.24.86.216 -p 22 -u vyatta -c vyatta
```

Execute netconfmanager client deleting the vrouter and check in http://[controller-ip]:8181/apidoc/explorer/index.html **Mounted Resources** tab device does not appear anymore.

```
./netconfmanager.py delete-vrouter -n vyatta-11
```

# Step 5: obtain status and schemas

A netconf device contains a list of schemas defining which configuration and operational data is available through netconf protocol.

Fill **get_schemas** method to return all schemas supported by the NETCONF device. Append following lines to the **get_schemas** method. 

```
# add node to the controller
result = self.ctrl.get_schemas(args.name)

# check results
status = result.get_status()
if(status.eq(STATUS.OK)):
    print ("'%s' NETCONF node schemas: \n" % args.name)
    print json.dumps(result.get_data(), indent=4)
else:
    print ("\n")
    print ("Error removing NETCONF device, reason: %s" % status.brief())
    print status.detailed()
```

Check schemas are returned for a vRouter added to the SDN Controller.

```
./netconfmanager.py get-schemas -n vyatta-11
```

Fill **get_status** method which will return if the NETCONF device is configured and connected append following lines.

```
result = self.ctrl.check_node_config_status(args.name)
print result.get_status().detailed()

result = self.ctrl.check_node_conn_status(args.name)
print result.get_status().detailed()
```

Check device is connected and configured.

```
 ./netconfmanager.py get-status -n vyatta-11
```


# Step 6: obtain full configuration from vrouter

Fill **get_vrouter_config** on `netconfmanager.py` file, using **get_cfg** method provided by VRouter5600 class and print the configuration to the standard output.

This lab expects students will be able to write the code to provide above feature.
