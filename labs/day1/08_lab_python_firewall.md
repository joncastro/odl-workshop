# Lab: Firewall python client

## Prerequisites

Students should have already completed the lab "vRouter python client".

## Requirements

* Ubuntu Trust 14.04 64 Bit
* Brocade vRouter 5600
* Python

```
sudo apt-get install python
```

* pip

```
sudo apt-get install python-pip
```

* pybvc

```
sudo pip install pybvc
```


## Overview

In this lab we will be managing firewall rules to a vRouter device through the SDN Controller using **Pybvc** library.  This lab will use mainly Pyvbvc **Controller** and **VRouter5600**. Check following links to get a better understanding of the methods provided by these objects.

* [controller.py](https://github.com/BRCDcomm/pybvc/blob/master/pybvc/controller/controller.py)
* [vrouter5600.py](https://github.com/BRCDcomm/pybvc/blob/master/pybvc/netconfdev/vrouter/vrouter5600.py)

---

Following steps will create gradually a python firewall manager using a vRouter Netconf capable device and the SDN controller.


### Step 1: YAML configuration file

Create **ctrl.yml** YAML configuration file to let python client know where is located Brocade SDN controller, user, password, etc.

```
#--------------------------------------
# Controller specific attributes
#--------------------------------------

# IP address
ctrlIp: 127.0.0.1

# Controller TCP port number
ctrlPort: 8181

# Admin user name
ctrlUser: admin

# Admin user password
ctrlPassword: admin

```

### Step2: Create basic netconf client structure

First, create **firewallmanager.py** with following content. It provides a command based client with arguments parsing. It will take the first argument and call the function if it exists.

```
#!/usr/bin/env python

import json
import argparse
import requests

from pybvc.controller.controller import Controller
from pybvc.common.utils import load_dict_from_file
from pybvc.netconfdev.vrouter.vrouter5600 import VRouter5600
from pybvc.common.status import OperStatus, STATUS
from requests.exceptions import ConnectionError, Timeout
from pybvc.common.result import Result


#-------------------------------------------------------------------------------
# Class 'Firewallmanager'
#-------------------------------------------------------------------------------
class Firewallmanager(object):
    """ Firewall manager commands executer """

    def __init__(self):
        self.prog = 'firewallmanager'
        parser = argparse.ArgumentParser(
            prog=self.prog,
            description='Firewall manager command line tool for interaction with OpenFlow Controller',
            usage="%(prog)s [-h] [-C <path>] <command> [<args>]\n"
                  "(type '%(prog)s -h' for details)\n"
                     "\nAvailable commands are:\n"
                     "\n   get-fw-cfg            Get firewall rules"
                     "\n   add-group             Add firewall group"
                     "\n   overwrite-group        Overwrite firewall group"
                     "\n   delete-group          Delete firewall group"
                     "\n   add-rule              Add firewall rule"
                     "\n   overwrite-rule        Overwrite firewall rule"
                     "\n   delete-rule           Delete firewall rule"
                     "\n"
                     "\n  '%(prog)s help <command>' provides details for a specific command")

        parser.add_argument('-C', metavar="<path>", dest='ctrl_cfg_file',
                            help="path to the controller's configuration file (default is './ctrl.yml')",
                            default="./ctrl.yml")

        parser.add_argument('command', help='command to be executed')

        # following method will save on args the first argument
        # and re
        args, remaining_args = parser.parse_known_args()

        # Get Controller's attributes from configuration file
        self.ctrl  = self.get_controller(args.ctrl_cfg_file)
        if(self.ctrl == None):
            print "\n".strip()
            print ("Cannot find controller configuration file")
            print "\n".strip()
            exit(1)

        # Invoke method that is matching the name of sub-command argument
        # cmd variable contains the first argument
        cmd = args.command.replace('-', '_')

        if hasattr(self, cmd):
            getattr(self, cmd)(remaining_args)
        else:
            print "\n".strip()
            print ("Error, unrecognized command '%s'" % cmd)
            print "\n".strip()
            parser.print_help()
            exit(1)

    #---------------------------------------------------------------------------
    # get controller object given yaml configuration file
    #---------------------------------------------------------------------------
    def get_controller(self, file):
        ctrlProperties = {}
        if(load_dict_from_file(file, ctrlProperties) == False):
            print("Config file '%s' read error: " % f)
            return None
        try:
            ctrlIp = ctrlProperties['ctrlIp']
            ctrlPort = ctrlProperties['ctrlPort']
            ctrlUser = ctrlProperties['ctrlUser']
            ctrlPassword = ctrlProperties['ctrlPassword']
        except:
            print ("Some attribute is missing in ctrl.yml file")
            return None

        # create Controller object which represent an API provided
        # by pybvc to execute the most commons actions on the
        # SDN Controller
        return Controller(ctrlIp, ctrlPort, ctrlUser, ctrlPassword, None)

    #---------------------------------------------------------------------------
    #
    #---------------------------------------------------------------------------
    def http_put_request(self, url, nodeName , data):
        """Sends HTTP PUT request to a remote server and returns the response.

        :param string url: The partial url to the schema and element

        :param string data: The data to include in the body of the request.
                            Typically set to None.
        :return: Request status of the node.
        :rtype: None or :class:`pybvc.common.status.OperStatus`

        """

        content = None
        status = OperStatus()

        try:
            fullurl = "http://{}:{}/restconf/config/" + \
                          "opendaylight-inventory:nodes/node/{}/" + \
                          "yang-ext:mount/{}/"
            fullurl =  fullurl.format(self.ctrl.ipAddr,self.ctrl.portNum,nodeName,url)

            headers = {'content-type': 'application/json', 'accept': 'application/json'}

            resp = self.ctrl.http_put_request(fullurl,data,headers)
            if(resp == None):
                status.set_status(STATUS.CONN_ERROR)
            elif(resp.content == None):
                status.set_status(STATUS.CTRL_INTERNAL_ERROR)
            elif(resp.status_code == 200 or resp.status_code == 204):
                status.set_status(STATUS.OK)
                content = resp.content
            else:
                status.set_status(STATUS.HTTP_ERROR, resp)

            return Result(status, content)

        except (ConnectionError, Timeout) as e:
            print "Error: " + repr(e)
            status.set_status(STATUS.CONN_ERROR)

        return Result(status, content)

    #---------------------------------------------------------------------------
    #
    #---------------------------------------------------------------------------
    def http_post_request(self, url, nodeName , data):
        """Sends HTTP POST request to a remote server and returns the response.

        :param string url: The partial url to the schema and element

        :param string data: The data to include in the body of the request.
                            Typically set to None.
        :return: Request status of the node.
        :rtype: None or :class:`pybvc.common.status.OperStatus`

        """

        content = None
        status = OperStatus()

        try:
            fullurl = "http://{}:{}/restconf/config/" + \
                          "opendaylight-inventory:nodes/node/{}/" + \
                          "yang-ext:mount/{}/"

            fullurl =  fullurl.format(self.ctrl.ipAddr,self.ctrl.portNum,nodeName,url)

            headers = {'content-type': 'application/json', 'accept': 'application/json'}

            resp = self.ctrl.http_post_request(fullurl,data,headers)
            if(resp == None):
                status.set_status(STATUS.CONN_ERROR)
            elif(resp.content == None):
                status.set_status(STATUS.CTRL_INTERNAL_ERROR)
            elif(resp.status_code == 200 or resp.status_code == 204):
                status.set_status(STATUS.OK)
                content = resp.content
            else:
                status.set_status(STATUS.HTTP_ERROR, resp)

            return Result(status, content)

        except (ConnectionError, Timeout) as e:
            print "Error: " + repr(e)
            status.set_status(STATUS.CONN_ERROR)

        return Result(status, content)

    #---------------------------------------------------------------------------
    #
    #---------------------------------------------------------------------------
    def http_delete_request(self, url, nodeName , data=None):
        """Sends HTTP POST request to a remote server and returns the response.

        :param string url: The partial url to the schema and element

        :param string data: The data to include in the body of the request.
                            Typically set to None.
        :return: Request status of the node.
        :rtype: None or :class:`pybvc.common.status.OperStatus`

        """

        content = None
        status = OperStatus()

        try:
            fullurl = "http://{}:{}/restconf/config/" + \
                          "opendaylight-inventory:nodes/node/{}/" + \
                          "yang-ext:mount/{}/"

            fullurl =  fullurl.format(self.ctrl.ipAddr,self.ctrl.portNum,nodeName,url)

            headers = {'content-type': 'application/json', 'accept': 'application/json'}

            resp = self.ctrl.http_delete_request(fullurl,data,headers)
            if(resp == None):
                status.set_status(STATUS.CONN_ERROR)
            elif(resp.content == None):
                status.set_status(STATUS.CTRL_INTERNAL_ERROR)
            elif(resp.status_code == 200 or resp.status_code == 204):
                status.set_status(STATUS.OK)
                content = resp.content
            else:
                status.set_status(STATUS.HTTP_ERROR, resp)

            return Result(status, content)

        except (ConnectionError, Timeout) as e:
            print "Error: " + repr(e)
            status.set_status(STATUS.CONN_ERROR)

        return Result(status, content)

    #---------------------------------------------------------------------------
    # get firewall rules
    #---------------------------------------------------------------------------
    def get_fw_cfg(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s get-fw-cfg -n=NAME|--name=NAME\n\n"
                  "Get firewall configuration from vRouter device\n\n"
                  "Options:\n"
                  "  -n, --name          device name\n")
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


    #---------------------------------------------------------------------------
    # add group
    #---------------------------------------------------------------------------
    def add_group(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s add-group -n=NAME|--name=NAME -j=JSON|--json=JSON\n\n"
                  "Add firewall group \n\n"
                  "Options:\n"
                  "  -n, --name          device name\n"
                  "  -j, --json          json file\n"
                  )
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-j', '--json', metavar = "<JSON>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


    #---------------------------------------------------------------------------
    # add group
    #---------------------------------------------------------------------------
    def overwrite_group(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s overwrite-group -n=NAME|--name=NAME -g=GROUP|--group=GROUP -j=JSON|--json=JSON\n\n"
                  "Overwrite firewall group \n\n"
                  "Options:\n"
                  "  -n, --name          device name\n"
                  "  -g, --group         group name\n"
                  "  -j, --json          json file\n"
                  )
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-g', '--group', metavar = "<GROUP>", required=True)
        parser.add_argument('-j', '--json', metavar = "<JSON>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


    #---------------------------------------------------------------------------
    # delete group
    #---------------------------------------------------------------------------
    def delete_group(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s delete-group -n=NAME|--name=NAME -g=GROUP|--group=GROUP\n\n"
                  "Delete firewall group \n\n"
                  "Options:\n"
                  "  -n, --name          device name\n"
                  "  -g, --group         group name\n"
                  )
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-g', '--group', metavar = "<GROUP>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


    #---------------------------------------------------------------------------
    # add rule
    #---------------------------------------------------------------------------
    def add_rule(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s add-rule -n=NAME|--name=NAME -j=JSON|--json=JSON\n\n"
                  "Add firewall rule \n\n"
                  "Options:\n"
                  "  -n, --name          device name\n"
                  "  -j, --json          json file\n"
                  )
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-j', '--json', metavar = "<JSON>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


    #---------------------------------------------------------------------------
    # add group
    #---------------------------------------------------------------------------
    def overwrite_rule(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s overwrite-rule -n=NAME|--name=NAME -g=GROUP|--group=GROUP -r=RULE|--rule=RULE -j=JSON|--json=JSON\n\n"
                  "Overwrite firewall rule \n\n"
                  "Options:\n"
                  "  -n, --name          device name\n"
                  "  -g, --group         group name\n"
                  "  -r, --rule          rule name\n"
                  "  -j, --json          json file\n"
                  )
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-g', '--group', metavar = "<GROUP>", required=True)
        parser.add_argument('-r', '--rule', metavar = "<RULE>", required=True)
        parser.add_argument('-j', '--json', metavar = "<JSON>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


    #---------------------------------------------------------------------------
    # delete group
    #---------------------------------------------------------------------------
    def delete_rule(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s delete-rule -n=NAME|--name=NAME -g=GROUP|--group=GROUP -r=RULE|--rule=RULE\n\n"
                  "Delete firewall rule\n\n"
                  "Options:\n"
                  "  -n, --name          device name\n"
                  "  -g, --group         group name\n"
                  "  -r, --rule          rule name\n"
                  )
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-g', '--group', metavar = "<GROUP>", required=True)
        parser.add_argument('-r', '--rule', metavar = "<RULE>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)


#-------------------------------------------------------------------------------
# main
#-------------------------------------------------------------------------------
if __name__ == "__main__":
    Firewallmanager()
```

Execute firewallmanager client and get familiar with the options

```
$ ./firewallmanager.py -h
usage: firewallmanager [-h] [-C <path>] <command> [<args>]
(type 'firewallmanager -h' for details)

Available commands are:

   get-fw-cfg            Get firewall rules
   add-group             Add firewall group
   overwrite-group        Overwrite firewall group
   delete-group          Delete firewall group
   add-rule              Add firewall rule
   overwrite-rule        Overwrite firewall rule
   delete-rule           Delete firewall rule

  'firewallmanager help <command>' provides details for a specific command

Firewall manager command line tool for interaction with OpenFlow Controller

positional arguments:
  command     command to be executed

optional arguments:
  -h, --help  show this help message and exit
  -C <path>   path to the controller's configuration file (default is
              './ctrl.yml')
```


# Step 3: get firewall rule configuration

Fill **get_fw_cfg** to obtain firewall configuration from vRouter.

Append following lines which use VRouter5600 **get_firewalls_cfg** method which returns full firewall configuration.

```
# create NETCONF Node
node = VRouter5600(self.ctrl, args.name, None, None, None, None, None)

# remove node from the controller
result = node.get_firewalls_cfg()

# check results
status = result.get_status()
if(status.eq(STATUS.OK)):
    print ("'%s' firewall configuration" % args.name)
    print json.dumps(json.loads(result.get_data()), indent=4)
else:
    print ("\n")
    print status.detailed()
```

Verify firewall configuration is printed in the standard output.

```
./firewallmanager.py  get-fw-cfg -n vyatta-local
```

If the command answer _Requested data not found_ it means there is not firewall configuration in the vRouter. Configure manually a firewall configuration like following and try again.

```
vyatta@vyatta:~$ configure
[edit]
vyatta@vyatta# set security firewall name test
[edit]
vyatta@vyatta# commit
[edit]
vyatta@vyatta# save
[edit]
vyatta@vyatta# exit
logout
vyatta@vyatta:~$
```

# Step 4: add, overwrite and delete firewall groups

Fill **add_group**, **overwrite_group** and **delete_group** methods to add, modify and delete groups.

Append following lines to **add_group**

```
with open(args.json, 'r') as content_file:
    content = content_file.read()

url = "vyatta-security:security/vyatta-security-firewall:firewall"

result = self.http_post_request(url,args.name,content)
status = result.get_status()
if(status.eq(STATUS.OK)):
    print ("'%s' group added" % args.name)
else:
    print ("\n")
    print status.detailed()
```

Create a file called **group.json** with following content

```
{
    "vyatta-security-firewall:firewall": {
        "name": [
            {
                "tagnode": "test2",
                "rule": [
                    {
                        "tagnode": 1,
                        "action": "accept"
                    }
                ]
            }
        ]
    }
}
```

Execute add-group command.

```
./firewallmanager.py add-group -n vyatta-local -j group.json
```

Verify group has been added properly executing get firewall configuration command.

```
./firewallmanager.py  get-fw-cfg -n vyatta-local
```


Append following lines to **delete_group**

```
url = "vyatta-security:security/vyatta-security-firewall:firewall/name/{}"
url = url.format(args.group)

result = self.http_delete_request(url,args.name)
status = result.get_status()
if(status.eq(STATUS.OK)):
    print ("'%s' group removed" % args.name)
else:
    print ("\n")
    print status.detailed()
```


Execute delete-group command.

```
./firewallmanager.py delete-group -n vyatta-local -g test2
```

Verify group has been removed properly executing get firewall configuration command.

```
./firewallmanager.py  get-fw-cfg -n vyatta-local
```


Append following lines to **overwrite_group**

```
with open(args.json, 'r') as content_file:
    content = content_file.read()

url = "vyatta-security:security/vyatta-security-firewall:firewall/name/{}"
url = url.format(args.group)

result = self.http_put_request(url,args.name,content)
status = result.get_status()
if(status.eq(STATUS.OK)):
    print ("'%s' group overwritten" % args.name)
else:
    print ("\n")
    print status.detailed()
```

Create a file called **group_overwrite.json** with following content

```
{
    "vyatta-security-firewall:name": [
        {
            "tagnode": "test2",
            "rule": [
                {
                    "tagnode": 1,
                    "action": "drop"
                }
            ]
        }
    ]
}
```

Execute add-group command.

```
./firewallmanager.py add-group -n vyatta-local -j group.json
```

Verify group has been added properly executing get firewall configuration command.

```
./firewallmanager.py  get-fw-cfg -n vyatta-local
```

Execute overwrite-group command.

```
./firewallmanager.py overwrite-group -n vyatta-local -g test2 -j group_overwrite.json
```

Verify group has been overwritten properly executing get firewall configuration command.

```
./firewallmanager.py  get-fw-cfg -n vyatta-local
```


# Step 5: add, overwrite and delete firewall rules

Fill **add_rule**, **overwrite_rule** and **delete_rule** methods similarly as done with groups but adapt it to rule requirements.

Use following example as payload for **add_rule**

```
{
    "vyatta-security-firewall:rule": [
        {
            "tagnode": 3,
            "action": "accept"
        }
    ]
}
```

Use following example as payload for **overwrite_rule**

```
{
    "vyatta-security-firewall:rule": [
        {
            "tagnode": 3,
            "action": "drop"
        }
    ]
}
```
