# Lab: Setting Up Opendaylight Development Environment

## Prerequisites

Students requires running an Ubuntu Trusty machine. It can be a virtual machine over any virtual environment like VMware, VirtualBox, etc.

## Requirements

* Eclipse Luna
* Java JDK 1.7
* Maven
* Git
* Ubuntu Trusty 64 bits

## Overview

In this lab we will install the basic tools to be able to develop applications for Opendaylight in a Ubuntu machine. Opendaylight Development Environment can be installed in different OS like Mac OS or other linux distribution. This document describes the steps required for Ubuntu but they can be easily adapted to other OS.

Opendaylight is a platform running on OSGi, so it mainly requires a Java development environment with maven.

---


## JDK 1.7

OpenDaylight requires Java 7 JDK for both Helium (previous release) and Lithium (current release).

### Install JDK 1.7

Following commands shows how to install Open JDK 1.7 on Ubuntu server.

```
sudo apt-get install openjdk-7-jdk
```

Some Java applications reads JAVA_HOME value from the environment. It is recommended that we set JAVA_HOME variable in the user bashrc file. Edit .bashrc file

```
vi ~/.bashrc
```

and append the following line to the end of the bashrc file.

```
export JAVA_HOME=/usr/lib/jvm/java-7-openjdk-amd64/jre
```

---

## Maven

OpenDaylight applications are organised and built using [Apache Maven](https://maven.apache.org/). Maven is a tool for building and managing any Java-based project, it describes how the project is built, and its dependencies.

The pom.xml file (project object model) describes the software project being built, its dependencies on other external modules and components, the build order, directories, and required plug-ins.

### Install Maven

OpenDaylight requires Maven 3.1.1 or newer installed. For this lab we will be using version 3.3.3 (the latest as of 07/2015).

Navigate to the logged in users home directory.

```
cd ~
```
Create the directory where we will install maven.

```
mkdir /usr/share/apache-maven
```

Fetch the maven 3.3.3 gzipped tarball.

```
wget http://apache.mirror.uber.com.au/maven/maven-3/3.3.3/binaries/apache-maven-3.3.3-bin.tar.gz
```
Extract the gzipped tarball to the newly created apache-maven directory. We will also remove the tarball as it is no longer needed.

```
sudo tar -zxf apache-maven-3.3.3-bin.tar.gz -C /usr/share/apache-maven
rm -f apache-maven-3.3.3-bin.tar.gz
```

As we have not installed Maven using a package manager it is required that we set some global variables in our bashrc file.

```
vim ~/.bashrc
```

Append the following lines to the end of the bashrc file.

```
# Set the Maven Home directory
export M2_HOME=/usr/share/apache-maven/apache-maven-3.3.3
# Set the Maven Bin directory (where the Maven executable lives)
export M2=$M2_HOME/bin
# Increase Maven's memory usage, so it doesn't fail wen building our ODL project
export MAVEN_OPTS='-Xmx1048m -XX:MaxPermSize=512m'
# Add the Maven bin directory path to the global PATH directory.
export PATH=$M2:$PATH
```
Let's reload the .bashrc file in our current shell so that these variables are immediately available.

```
source ~/.bashrc
```
OpenDaylight maintains its own repositories outside of Maven Central, which means maven cannot resolve OpenDaylight artifacts by default. Since OpenDaylight is organized as multiple inter-dependent projects, building a particular project usually means pulling in some artifacts. In order to make this work, your maven installation needs to know the location of OpenDaylight repositories and has to taught to use them.

This is achieved by making sure ~/.m2/settings.xml looks something like this:

```
mkdir ~/.m2
cp -n ~/.m2/settings.xml{,.orig} ; \wget -q -O - https://raw.githubusercontent.com/opendaylight/odlparent/master/settings.xml > ~/.m2/settings.xml
```
---

## Eclipse

Eclipse is an IDE (Integrated Development Environment) which provides a single pane of glass for developing and debugging source code.

### Installing Eclipse

The recommended way to install Eclipse on Ubuntu is using the distribution's package manager. But unfortunately the Ubuntu distribution contains Eclipse version is 3.8.1-5.1 which does not support Maven integration because the required package org.eclipse.m2e.core requires Eclipse Luna. The Luna is not packaged for Ubuntu (yet) so the manual installation is the only option.

Remove previous eclipse installation if it is installed.

```
sudo apt-get remove eclipse eclipse-platform
```

Download Eclipse Luna for ""Java EE Developers" or for "Java Developers".

```
wget http://archive.eclipse.org/technology/epp/downloads/release/luna/SR1/eclipse-java-luna-SR1-linux-gtk-x86_64.tar.gz
```

Extract the Eclipse installation tarball into your home directory

```
tar -xvzf eclipse-java-luna-SR1-linux-gtk-x86_64.tar.gz
```

Increase eclipse memory and heapsize

```
sed -i 's/Xmx.*/Xmx2048m/g' eclipse/eclipse.ini
sed -i 's/XX:MaxPermSize=.*/XX:MaxPermSize=512m/g' eclipse/eclipse.ini
```


Start eclipse

```
~/eclipse/eclipse
```

Install following eclipse plugins through Help => Install New Software using detailed repository

* **m2e** and **m2e - slf4j**,  **repository** http://download.eclipse.org/technology/m2e/releases
* **Tycho**,  **repository** http://repo1.maven.org/maven2/.m2e/connectors/m2eclipse-tycho/0.7.0/N/LATEST
* **Xtend**,  **repository** http://download.eclipse.org/modeling/tmf/xtext/updates/composite/releases
* **Groovy-Eclipse** and **m2e Confgurator for Groovy-Eclipse**,  **repository** http://dist.springsource.org/release/GRECLIPSE/e4.4

Install **build-helper-maven** in Windows => Preferences => Discovery => Open Catalog => Type 'build' and click Find => choose **buildhelper**


It is also recommended to automatically remove trailing whitespace. To enable this feature on eclipse, go to Window => Preferences, expand Java => Editor and click "Save Actions". Ensure "Remove trailing whitespace" is configured, if not add it.

Finally, restart eclipse to ensure all plugins are loaded.
