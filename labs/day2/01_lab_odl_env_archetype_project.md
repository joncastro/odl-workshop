# Lab: MD-SAL Archetype Project

## Prerequisites

Students should have already completed following lab 

* Setting Up OpenDaylight Development Environment


## Overview

In this lab we will be creating the structure for a MD-SAL application called **BandwidthScheduler**. This will be achieved by using Maven, OpenDaylight Archetype extensions based on Maven Archetype.

---

## Maven Archetype

[Maven Archetype](https://maven.apache.org/guides/introduction/introduction-to-archetypes.html) is a project templating toolkit plugin for Maven. A archetype defines the folder structure, files, pom.xml file etc as a template for a project so that developers can quickly start developing a project following the projects best practices and standards.

In the case of OpenDaylight there are several maven archetypes that define the folders, Java files, YANG files, pom.xml files, and feature.xml files that are needed in order to build a fully-functional, installable MD-SAL application.

The maven archetype we will be using is provided as part of the OpenDaylight Archetype which is a framework to enabled developers to easily develop applications on top of the OpenDaylight controller.


Following command will invoke the OpenDaylight Archetype MD-SAL Application template. After executing we need to answer some questions.

```
mvn archetype:generate -DarchetypeGroupId=org.opendaylight.controller -DarchetypeArtifactId=opendaylight-startup-archetype -DarchetypeRepository=http://nexus.opendaylight.org/content/repositories/opendaylight.snapshot/ -DarchetypeCatalog=http://nexus.opendaylight.org/content/repositories/opendaylight.snapshot/archetype-catalog.xml
```

**NOTE**: if executing above command produces an error then rename ~/.m2 folder to ~/.m2.back and try again. Once the project has been generated copy settings.xml to ~/.m2 executing cp ~/.m2.back/settings.xml ~/.m2

* **groupId**: Typically this is a high level grouping, starting with the company or organization, such as 'org.opendaylight.controller' we will use `com.brocade.odl.workshop`
* **artifactId**: This will be the prefix for the folder names and all sub-maven project. Typically this describes the functionality or group project, such as "maclearner" etc we will use `bandwidthscheduler`
* **version**: The version of this artifact. You can hit enter to accept the default of "1.0=SNAPSHOT" or enter your own version. We will use the default.
* **package**: Defines the prefix for all packages. This defaults to a package created from the group id. We will use the default.
* **classPrefix**: Defines the class prefix for automated created java classes. By default it is artifact which the first letter in upper case and the rest in lower case. We will use `BandwidthScheduler` , notice we are using the letter S uppercase so it is not the default value.
* **copyright**: Adds given copyright to all generated files. For example, `Brocade Communications Systems, Inc`

Example

```
Define value for property 'groupId': : com.brocade.odl.workshop
Define value for property 'artifactId': : bandwidthscheduler
Define value for property 'version':  1.0-SNAPSHOT: :
Define value for property 'package':  com.brocade.odl.workshop: :
Sep 09, 2015 10:53:43 AM org.apache.velocity.runtime.log.JdkLogChute log
INFO: FileResourceLoader : adding path '.'
Define value for property 'classPrefix':  Bandwidthscheduler: : BandwidthScheduler
Define value for property 'copyright': : Brocade Communications Systems, Inc
Confirm properties configuration:
groupId: com.brocade.odl.workshop
artifactId: bandwidthscheduler
version: 1.0-SNAPSHOT
package: com.brocade.odl.workshop
classPrefix: BandwidthScheduler
copyright: Brocade Communications Systems, Inc
 Y: : Y
 ```

### Folder Structure


This will create the following folder structure.

```
bandwidthscheduler
├── pom.xml # Maven POM file.
├── features # karaf features file
├── api # YANG definitions, models the services provided by this plugin
├── impl # Implementation of modeled services
├── karaf # local karaf instance to run opendaylight locally
├── artifacts # contains artifacts dependencies
```

* **features**: In this folder is stored the features.xml file, which will be used by Maven when your application is built. It defines your application (aka "feature"), as well as its dependencies, configuration file location, etc.
* **api**: In this folder is stored the "model" for your application, which is stored in a YANG file. In addition to your model file, the Archetype will build this directory such that it runs the "YANG tools", which will create Java files from the model definitions in the YANG file.
* **impl**: In this folder is stored your application's actual implementation code. You will find a number of auto-generated Java files, but also a special Java package and file where you will modify code and add capabilities to your application.
* **artifacts**: contains project artifacts. We will not use it in this lab.
* **karaf**: contains a local opendaylight instance to run the application locally


Run a complete maven build from `bandwidthscheduler` folder.

```
mvn clean install
```

You should see that the parent project (`bandwidthscheduler`), and five child projects (`api`, `features`, `impl`, `artifacts`, and `karaf`) all compile successfully.


Above command also install an opendaylight karaf installation ready to be run locally. Execute **./karaf/target/assembly/bin/karaf** command to start karaf locally.

```
./karaf/target/assembly/bin/karaf

   ________                       ________                .__  .__       .__     __
   \_____  \ ______   ____   ____ \______ \ _____  ___.__.|  | |__| ____ |  |___/  |_
    /   |   \\____ \_/ __ \ /    \ |    |  \\__  \<   |  ||  | |  |/ ___\|  |  \   __\
   /    |    \  |_> >  ___/|   |  \|    `   \/ __ \\___  ||  |_|  / /_/  >   Y  \  |
   \_______  /   __/ \___  >___|  /_______  (____  / ____||____/__\___  /|___|  /__|
           \/|__|        \/     \/        \/     \/\/            /_____/      \/


Hit '<tab>' for a list of available commands
and '[cmd] --help' for help on a specific command.
Hit '<ctrl-d>' or type 'system:shutdown' or 'logout' to shutdown OpenDaylight.

opendaylight-user@root>
```

Validate application has been installed successfully running feature:list command on karaf console.

```
opendaylight-user@root>feature:list | grep bandwidthscheduler
odl-bandwidthscheduler-api                  | 1.0-SNAPSHOT     | x         | odl-bandwidthscheduler-1.0-SNAPSHOT             | OpenDaylight :: bandwidthscheduler :: api
odl-bandwidthscheduler                      | 1.0-SNAPSHOT     | x         | odl-bandwidthscheduler-1.0-SNAPSHOT             | OpenDaylight :: bandwidthscheduler
odl-bandwidthscheduler-rest                 | 1.0-SNAPSHOT     | x         | odl-bandwidthscheduler-1.0-SNAPSHOT             | OpenDaylight :: bandwidthscheduler :: REST
odl-bandwidthscheduler-ui                   | 1.0-SNAPSHOT     | x         | odl-bandwidthscheduler-1.0-SNAPSHOT             | OpenDaylight :: bandwidthscheduler :: UI
opendaylight-user@root>
```

To exit and stop karaf application execute `logout` command

```
opendaylight-user@root>
```

**Tip**, if you want to speed up the mvn installation process you could execute with following options.

* checkstyle.skip=true : skips style checking
* skipTests : skips tests
* nsu: skip updating snapshot from maven repository

```
mvn clean install -Dcheckstyle.skip=true -DskipTests -nsu
```

We now have a fully functional MD-SAL application.
