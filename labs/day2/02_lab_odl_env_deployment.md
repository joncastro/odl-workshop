# Lab: MD-SAL ODL Deployment

## Prerequisites

Students should have already completed the following lab:

* MD-SAL Archetype Project


## Overview

In this lab we will be deploying the MD-SAL application called **BandwidthScheduler** that was created in the previous lab to a OpenDaylight instance. This will be achieved by using Maven and Apache Karaf. We will use a Brocade SDN Controller instance as our Opendaylight distribution for this and the rest of labs.


---

## OSGi

OpenDaylight is a [OSGi](https://en.wikipedia.org/wiki/OSGi) (Open Services Gateway initiative) based project

OSGi is a specification like MVC but more rigid to organise and enforce modular design in Java applications. OSGi standard says that application components should be placed into “bundles”.

A bundle is a group of Java classes and additional resources equipped with a detailed manifest MANIFEST.MF file on all its contents.

A Life Cycle layer adds bundles that can be dynamically installed, started, stopped, updated and uninstalled. This makes OSGi an ideal candidate framework for a critical application such as a SDN controller.

## Karaf

Being an OSGi project, a OSGi runtime is required. [Apache Karaf](http://karaf.apache.org/) is a OSGi based runtime which provides a lightweight container onto which various components and applications can be deployed.

Karaf's key features:

* Hot deployment
* Dynamic configuration
* Logging System
* Provisioning
* Native OS integration
* Extensible Shell console
* Remote access
* Security framework based on JAAS
* Managing instances

OSGi bundles are called features, features can depend on other features, can be versioned etc. This allows features (plugins/applications) to be installed separately, and easily mix and match features to make a custom release of OpenDaylight.

## Deploy MD-SAL Application

Navigate to the projects directory.

```
cd ~/bandwidthscheduler
```

Edit the features projects' pom.xml file. To create a Karaf archive file.

```
vi features/pom.xml
```

and append following lines after dependencies.

```
<build>
  <plugins>
    <plugin>
      <groupId>org.apache.karaf.tooling</groupId>
      <artifactId>features-maven-plugin</artifactId>
      <version>2.2.8</version>
      <executions>
        <execution>
          <id>create-kar</id>
          <goals>
            <goal>create-kar</goal>
          </goals>
          <configuration>
            <featuresFile>target/classes/features.xml</featuresFile>
          </configuration>
        </execution>
      </executions>
    </plugin>
  </plugins>
</build>
```

Connect to your Opendaylight instance and validate which version of yangtools and mdsal is running. Karaf is listening SSH on 8101 and by default both username and password is karaf.

For example, following command can be used to connect to the karaf console. Just replace odl for your Opendaylight server ip.

```
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p 8101 karaf@odl
```

Then, execute `feature:list | grep odl-mdsal-all` to obtain md-sal version.

```
vyatta-user@root> feature:list | grep odl-mdsal-all
odl-mdsal-all                              | 1.2.0-Lithium    |           | odl-mdsal-1.2.0-Lithium                | OpenDaylight :: MDSAL :: All
vyatta-user@root> feature:list | grep odl-yangtools-all
odl-yangtools-all                          | 0.7.0-Lithium    |           | odl-yangtools-0.7.0-Lithium            | OpenDaylight Yangtools All
vyatta-user@root>
```

Update **features/pom.xml** file setting the corresponding dependencies.

```
<mdsal.version>1.2.0-Lithium</mdsal.version>
<yangtools.version>0.7.0-Lithium</yangtools.version>
```

In order to speed up the mvn build process we need to comment following lines in the root **pom.xml** file. This will avoid Maven build process to create the local Karaf instance which will not be used anymore.

**NOTE**: To add a comment in a xml file it is required  to add `<!--` at the begining and `-->` at the end.

```
 <!-- <module>karaf</module> -->
```

Edit **features/src/main/features/features.xml** and comment or remove `odl-bandwidthscheduler-rest` and `odl-bandwidthscheduler-ui` features which will not be used in these labs.

```
 <!--
<feature name='odl-bandwidthscheduler-rest' version='${project.version}' description='OpenDaylight :: bandwidthscheduler :: REST'>
  <feature version="${project.version}">odl-bandwidthscheduler</feature>
  <feature version="${mdsal.version}">odl-restconf</feature>
</feature>
<feature name='odl-bandwidthscheduler-ui' version='${project.version}' description='OpenDaylight :: bandwidthscheduler :: UI'>
  <feature version="${project.version}">odl-bandwidthscheduler-rest</feature>
  <feature version="${mdsal.version}">odl-mdsal-apidocs</feature>
  <feature version="${mdsal.version}">odl-mdsal-xsql</feature>
</feature>
 -->
```

Ensure maven build is successful.

```
mvn clean install -Dcheckstyle.skip=true -DskipTests -nsu
```

Validate `features-<artifactId>-<version>.kar` file has been created under features/targert directory. This file contains all the necessary information to deploy our application in Opendaylight.

Ensure `org.ops4j.pax.url.mvn.defaultRepositories` property in **org.ops4j.pax.url.mvn.cfg** file of your Opendaylight instance contains  `file:${karaf.data}/kar@id=kar.repository@multi@snapshots`. Kar files are deployed under data/kar and this property ensure karaf will use this directory as repository too.

```
$ grep org.ops4j.pax.url.mvn.defaultRepositories /opt/bvc/controller/etc/org.ops4j.pax.url.mvn.cfg
org.ops4j.pax.url.mvn.defaultRepositories=file:${karaf.home}/${karaf.default.repository}@id=system.repository,file:${karaf.data}/kar@id=kar.repository@multi@snapshots
```

If you had to update above property restart the controller using one of the following options.

```
sudo service bsc restart
```

or

```
/opt/bvc/bin/stop
/opt/bvc/bin/start
```

Now, we do have two options to deploy the kar file.

### Option 1

Copy the .kar file to the remote OpenDaylight controller deploy directory located in `/opt/bvc/controller/deploy`. This folder is automatically monitored by Karaf and it will install any new kar file detected automatically.

```
scp -r features/target/features-bandwidthscheduler-1.0-SNAPSHOT.kar vagrant@odl:/opt/bvc/controller/deploy
```


### Option 2


Copy the .kar file to the remote OpenDaylight controller outside of karaf structure, for example, /tmp folder.

```
scp -r features/target/features-bandwidthscheduler-1.0-SNAPSHOT.kar vagrant@odl:/tmp
```


SSH into the remote controller server. Remember Karaf is listening SSH on 8101 and by default both username and password is karaf.

```
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p 8101 karaf@odl
```

Install kar file using **kar:install** command on karaf shell.

```
> kar:install file:///tmp/bandwidthscheduler-features-1.0-SNAPSHOT.kar
```

## Validate deployment

The KAR Deployer will deploy all the kar content starting from the features descriptor. The KAR Deployer creates a repository dedicated to your kar (in the $/local-repo) and register the features descriptor. You can now see your feature available and installed.

```
vyatta-user@root>feature:list | grep ban
odl-bandwidthscheduler-api                 | 1.0-SNAPSHOT     | x         | odl-bandwidthscheduler-1.0-SNAPSHOT    | OpenDaylight :: bandwidthscheduler :: api
odl-bandwidthscheduler                     | 1.0-SNAPSHOT     | x         | odl-bandwidthscheduler-1.0-SNAPSHOT    | OpenDaylight :: bandwidthscheduler
vyatta-user@root>
```

Our solution is now installed on the SDN controller.
