# Lab: Creating the model with YANG

## Prerequisites

Students should have already completed the following labs:

* MD-SAL Archetype Project
* MD-SAL ODL Deployment

## Overview

In this lab we will be updating the YANG file to define our application model and API. The YANG file contains the configuration data, the operational data, RPC operations and notifications. In this lab we will just define configuration data. RPC calls and notifications will be covered in subsequences labs.

---

From now on, we will not use the local karaf created with the project and we will use only Brocade SDN Controller to test and deploy _BandwidthScheduler_ application. The provided Brocade SDN Controller comes with a `clean.sh` script located in the home folder that will help us to automatically clean the installation and go back to an original state. We will perform clean script before deploying the solution to ensure there is no previous version of developed application.

```
$ sh clean.sh
```

## Model

We will create an application to define bandwidth profiles to be applied between two hosts for a duration. The application will request two hosts (source and destination) and it will calculate the shortest path between them and program all the necessary Openflow devices in the path to enable reachability and bandwidth limitation.

Open **bandwidth-scheduler.yang** file and create following items:

* import ietf-inet-types type revision-date 2010-09-24
* define a typedef called bandwitch-profile which contains three type of bandwidth
  * gold 100000
  * silver 10000
  * bronze 1000
* create a container called scheduled-bandwidth which contains:
  * a list named limit which the key is composed by source and destination
  * the list contain 4 attributes
    * source (mandatory): use type ipv4-address used from ietf-inet-types
    * destination (mandatory): use type ipv4-address used from ietf-inet-types
    * bandwidth (mandatory): type bandwitch-profile
    * duration-sec (optional): type int32


Notice our YANG model imports an external model called `ietf-inet-types`, so it is also required to add following maven dependency to **api/pom.xml** file to be able to compile the yang model.

```
<dependencies>
   <dependency>
     <groupId>org.opendaylight.yangtools.model</groupId>
     <artifactId>ietf-inet-types</artifactId>
    </dependency>
</dependencies>
```


The **import** statement makes definitions from one module available inside another module or submodule. The mandatory **prefix** substatement assigns a prefix for the imported module that is scoped to the importing module or submodule. In this way we can refer to types or other statements using prefix name and a colon. Example, _type inet:ip-address;_

Add following import to the YANG model.

```
	import ietf-inet-types {
		prefix "inet";
		revision-date 2010-09-24;
	}
```



The **typedef** statement defines a new type that may be used locally in the module, in modules or submodules which include it, and by other modules that import from it. All derived types can be traced back to a YANG built-in type and MUST NOT have the name of one of the YANG built-in types.

In this case, typedef will contain an enumeration to define a predefined list of values to define different type of bandwitdh limitation. Add following typedef to the YANG model.

```
    typedef bandwidth-profile {
	    type enumeration {
	      enum gold {
	        value 800;
	      }
	      enum silver {
	        value 500;
	      }
	      enum bronze {
	        value 100;
	      }
	    }
	    default "bronze";
	}
```

A **container** node does not have a value, it holds related child nodes in the data tree.  The child nodes are defined in the container's substatements.

A **list** node is similar to a container in that it holds related child nodes, however can exist in multiple instances, with each instance being known as a list entry. The "list" statement takes one argument, which is an identifier, followed by a block of substatements that holds detailed list information. A list entry is uniquely identified by the "key" sub statement. The "key" statement, which MUST be present if the list represents configuration, takes as string argument that specifies a space-separated list of leaf identifiers of this list. All key leafs MUST be given values when a list entry is created, can be of any type (derived or built-in) EXCEPT the built-in type "empty".

The **leaf** statement is used to define a single leaf node in the schema tree. It takes one argument, which is an identifier, followed by a block of substatements that holds detailed leaf information. A leaf node exists in zero or one instances in the data tree.


Add following container to the YANG file.

```
    container scheduled-bandwidth {
	  	list limit {   
	      key "source destination";
	  		leaf source {
	  			type inet:ip-address;
	  			mandatory true;
	  			description "source ip address";
	  		}

	  		leaf destination {
	  			type inet:ip-address;
	  			mandatory true;
	  			description "destination ip address";
	  		}

	  		leaf bandwidth {
	  			type bandwidth-profile;
	  			 mandatory true;
	  			description "bandwidth profile";
	  		}

	  		leaf duration-sec {
	  			type int32;
	  			default 0;
	  			description "if bigger than 0, this policy will expire after given seconds";
		 }
  	   }
  	}
```

---

Execute ` mvn clean install -Dcheckstyle.skip=true -DskipTests -nsu` after completing the YANG model.

Call **clean.sh** in the Opendaylight server to remove any previous application installation


```
$ sh clean.sh
```

and then deploy the new version of the application on the SDN Controller.

```
scp -r features/target/features-bandwidthscheduler-1.0-SNAPSHOT.kar vagrant@odl:/opt/bvc/controller/deploy
```

---

After the application has been successfully deployed, connect to `http://{odl-ip}:8181/apidoc/explorer/index.html` and validate the **bandwidthscheduler** model is available. Take a look to the API documentation to understand the available operations.

Now, we are going to add a couple of bandwidth limits in the network executing HTTP PUT operations using following url `http://<odl-ip>:8181/restconf/config/bandwidthscheduler:scheduled-bandwidth`

Header

```
Authorization : Basic YWRtaW46YWRtaW4=
Accept : application/json
Content-Type : application/json
```

and following payload

```
{
    "scheduled-bandwidth": {
        "limit": [
            {
                "source": "10.0.0.1",
                "destination": "10.0.0.2",
                "bandwidth": "bronze",
                "duration-sec": 100
            },
          	{
                "source": "10.0.0.2",
                "destination": "10.0.0.1",
                "bandwidth": "bronze",
                "duration-sec": 100
            }
        ]
    }
}
```

Above operation should return 200 HTTP code stating a successful answer.

---

Execute a HTTP GET on `http://<odl-ip>:8181/restconf/config/bandwidthscheduler:scheduled-bandwidth` and validate the data written in the previous step is returned successfully. You have to use the same headers defined for PUT operation.

It is also possible to request a particular limit. Our model contains a list of limit which contains a key composed by source and destination. In order to obtain a single limit we need to append to the url `limit/{source-value}/{destination-value}`, for example
`http://{odl-ip}:8181/restconf/config/bandwidthscheduler:scheduled-bandwidth/limit/10.0.0.1/10.0.0.2`

Excute a HTTP GET operation to return a limit which source is 10.0.0.2 and destination is 10.0.0.1.

---

Opendaylight RESTConf API validates the url (path), payload format and content as defined in the YANG model. For example, **source** address has been defined as an _inet:ip-address_ which accepts any ipv4 or ipv6 address.

Now, try to execute a PUT operation with a payload which **source** and/or **destination**  not containing a valid ipv4 or ipv6 address.

```
{
    "scheduled-bandwidth": {
        "limit": [
            {
                "source": "10.0.0.11234",
                "destination": "10.0.0.2",
                "bandwidth": "bronze",
                "duration-sec": 100
            }
        ]
    }
}
```

Validate Opendaylight is returning an error.

Try to send other invalid request like bandwidth value different from gold, silver or broze, etc.
