# Lab: Registering a change listeners

## Prerequisites

Students should have already completed the following lab:

* Creating the model with YANG

## Overview

In this lab we will register a change listener to be notified when values from a model change. In this case, we will listen to changes on the Topology model to be notified when an Openflow network is updated. In this way, every time that an openflow device or host is added, removed or updated Opendaylight will notify to all registered listeners.

---

## Dependencies

First, append following dependencies to **impl/pom.xml** to be able to use Topology models, l2 host tracker and dijkstra graphs.

```
<dependency>
  <groupId>org.opendaylight.openflowplugin.model</groupId>
  <artifactId>model-flow-service</artifactId>
  <version>0.1.0-Lithium</version>
</dependency>        
<dependency>
  <groupId>org.opendaylight.controller.thirdparty</groupId>
  <artifactId>net.sf.jung2</artifactId>
  <version>2.0.1</version>
</dependency>
<dependency>
  <groupId>org.opendaylight.controller.model</groupId>
  <artifactId>model-topology</artifactId>
</dependency>
<dependency>
  <groupId>org.opendaylight.l2switch.hosttracker</groupId>
  <artifactId>hosttracker-model</artifactId>
  <version>0.2.0-Lithium</version>
</dependency>
```

## Implementing data change listener

When a listener is registered to receive changes in a particular model it is required to provide an implementation of **org.opendaylight.controller.md.sal.binding.api.DataChangeListener**. The class which implements this method will implement **onDataChanged** method which receives the changes applied on the model. Our listener implementation will listen to Openflow Topology and Bandwidth Scheduler models.

Edit **BandwidthSchedulerProvider.java** class and apply following changes:

Add following imports

```
import org.opendaylight.controller.md.sal.binding.api.DataChangeListener;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.NetworkTopology;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.TopologyId;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.Topology;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.TopologyKey;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.topology.Link;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.opendaylight.controller.md.sal.common.api.data.LogicalDatastoreType;
import org.opendaylight.controller.md.sal.common.api.data.AsyncDataBroker.DataChangeScope;
import org.opendaylight.controller.md.sal.binding.api.DataBroker;
import org.opendaylight.controller.md.sal.binding.api.DataChangeListener;
import org.opendaylight.yangtools.concepts.ListenerRegistration;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.ScheduledBandwidth;
```

Edit **BandwidthSchedulerProvider.java** and create following private class which implements `DataChangeListener` interface. Following class will receive any change notification after  registering it. Notice, following class implement `onDataChanged` method and this method receives a single parameter containing new, updated and deleted data. Analyze **change** variable to understand how to detect which changes has been made.

```
private class TopologyDataChangeListener implements DataChangeListener{
  @Override
  public void onDataChanged(AsyncDataChangeEvent<InstanceIdentifier<?>, DataObject> change) {
      LOG.info("topology model has been changed");
  }
}
```


## Openflow change listener

Edit **BandwidthSchedulerProvider.java** class and create a new method called **setDataBroker** with following content. **setDataBroker** method will be called by **BandwidthSchedulerModule** class as described in following steps. This method receives the DataBroker, saves the reference and then register the listeners.

When a listener is registered we need to provide:

* Logical Datastore, CONFIGURATION or OPERATIONAL
* Identifier: an instance of **InstanceIdentifier** which identifies the model and the node
* Change scope: SUBTREE (any change in given node and any children or nested children), ONE (any change in the node or inmediate children) or BASE (only changes in the given node)

We will register a listener on Openflow topology under topology/flow:1 as operational data.

```
    public void setDataBroker(DataBroker dataBroker){
    	this.dataBroker = dataBroker;

    	// this listener will list to any change on the following url
    	// http://<odl-ip>:8181/restconf/operational/network-topology:network-topology/topology/flow:1
       dataChangeTopologyListenerRegistration =
        		this.dataBroker.registerDataChangeListener(LogicalDatastoreType.OPERATIONAL,InstanceIdentifier.builder(NetworkTopology.class)
        		        .child(Topology.class, new TopologyKey(new TopologyId("flow:1"))).build()
        		        ,new TopologyDataChangeListener(),DataChangeScope.SUBTREE);

    }
```

Add following two variables definition to the class

```
private DataBroker dataBroker = null;
private ListenerRegistration<DataChangeListener> dataChangeTopologyListenerRegistration;
```


It is best practices to close the listeners to avoid having unnecessary open resources. Update **close** method to include closing listener call.

```
@Override
public void close() throws Exception {
    LOG.info("BandwidthSchedulerProvider Closed");
    if (dataChangeTopologyListenerRegistration!=null)dataChangeTopologyListenerRegistration.close();  
}
```




### Add data broker dependency


It is required to add data-broker module to the project to be able to use data broker service and data change listeners. We need to apply following two steps.

* Edit **impl/src/main/config/default-config.xml** file and include following new module.

```
<data-broker>
    <type xmlns:binding="urn:opendaylight:params:xml:ns:yang:controller:md:sal:binding">binding:binding-async-data-broker</type>
    <name>binding-data-broker</name>
</data-broker>
```

* Edit **impl/src/main/yang/bandwidthscheduler-impl.yang** and include following container data broker.

```
container data-broker {
    uses config:service-ref {
        refine type {
            mandatory false;
            config:required-identity md-sal-binding:binding-async-data-broker;
        }
    }
}
```

At this moment, it is preferable to execute `mvn clean install -Dcheckstyle.skip=true -DskipTests -nsu` because it will force some auto-generated classes include above data broker dependency and we will be able to use it in our classes without compiling errors.

After compiling, data broker is available from **org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.impl.rev141210.BandwidthSchedulerModule** class. We need to update it to include the set call to our provider.

Edit **BandwidthSchedulerModule.java** class and set the dataBroker on the provider on `createInstance` method.

```
DataBroker dataBroker = getDataBrokerDependency();
provider.setDataBroker(dataBroker);
```

Above lines requires following import

```
import org.opendaylight.controller.md.sal.binding.api.DataBroker;
```

### Testing

Now, compile `mvn clean install -Dcheckstyle.skip=true -DskipTests -nsu` and deploy the new solution on the SDN Controller. Remember to call **clean.sh** method before copying the kar file to the deploy folder.

Start a mininet network, for example, executing following mininet command.

```
sudo mn --topo tree,3 --mac --switch user --controller=remote,ip=10.0.0.6,port=6633
```

As soon as the mininet network is created you should see one or more logs like following on **/opt/bvc/log/controller_logs/karaf.log** file.

```
2015-09-15 04:24:54,919 | INFO  | lt-dispatcher-16 | BandwidthSchedulerProvider       | 353 - com.brocade.odl.workshop.bandwidthscheduler-impl - 1.0.0.SNAPSHOT | topology model has been changed
```

After executing `pingall` command on mininet you should also see above logs because the topology has been updated with new hosts.

Executing `exit` on minet will produce listener to be called too because the topology has removed the devices.



## Registering Bandwidth Scheduler listener

Now, we will add a new listener to receive any change on bandwidth scheduler model. This change only requires modifying **BandwidthSchedulerProvider.java** because data broker steps was made in previous modification.

* Add private class which implemente DataChangeListenrs

```
private class BandwidthSchedulerDataChangeListener implements DataChangeListener{
  @Override
  public void onDataChanged(AsyncDataChangeEvent<InstanceIdentifier<?>, DataObject> change) {
      LOG.info("bandwidth scheduler model has been changed");
  }
}
```

* Add listener variable to the class

```
private ListenerRegistration<DataChangeListener> dataChangeScheduledBandwidthListenerRegistration;
```

* Edit **setDataBroker** method adding following lines

```
       // this listener will list to any change on the following url
   	   // http://<odl-ip>:8181/restconf/config/bandwidthscheduler:scheduled-bandwidth
       dataChangeScheduledBandwidthListenerRegistration =
        		this.dataBroker.registerDataChangeListener(LogicalDatastoreType.CONFIGURATION,InstanceIdentifier.builder(ScheduledBandwidth.class).build()
        				,new BandwidthSchedulerDataChangeListener(),DataChangeScope.SUBTREE);
```

* Edit **close** method and include close listener line

```
    if (dataChangeScheduledBandwidthListenerRegistration!=null)dataChangeScheduledBandwidthListenerRegistration.close();
}
```


Compile executing `mvn clean install -Dcheckstyle.skip=true -DskipTests -nsu` and deploy the new solution. Remember to execute clean.sh before deploying it.

Send a HTTP PUT operation using following url `http://<odl-ip>:8181/restconf/config/bandwidthscheduler:scheduled-bandwidth`

Header

```
Authorization : Basic YWRtaW46YWRtaW4=
Accept : application/json
Content-Type : application/json
```

and following payload

```
{
    "scheduled-bandwidth": {
        "limit": [
            {
                "source": "10.0.0.1",
                "destination": "10.0.0.2",
                "bandwidth": "bronze",
                "duration-sec": 100
            },
          	{
                "source": "10.0.0.2",
                "destination": "10.0.0.1",
                "bandwidth": "bronze",
                "duration-sec": 100
            }
        ]
    }
}
```

You should see a log containing `bandwidth scheduler model has been changed` in **/opt/bvc/log/controller_logs/karaf.log** file.
