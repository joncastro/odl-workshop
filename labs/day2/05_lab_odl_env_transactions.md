# Lab: Creating transactions

## Prerequisites

Students should have already completed the following lab:

* Registering a change listeners

## Overview

In this lab we will create transactions to read and write information to/from models. We will create flows, meters, and read topology in a graph.

---

## Topology Helper

Create TopologyHelper class with following content. TopologyHelper class will read the openflow topology and create a dijkstra shortest graph. Dijkstra graph will be used to calculate the shortest path between two hosts.

```
package com.brocade.odl.workshop.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.opendaylight.controller.md.sal.binding.api.DataBroker;
import org.opendaylight.controller.md.sal.binding.api.ReadOnlyTransaction;
import org.opendaylight.controller.md.sal.common.api.data.LogicalDatastoreType;
import org.opendaylight.controller.sal.binding.api.NotificationProviderService;
import org.opendaylight.yang.gen.v1.urn.opendaylight.address.tracker.rev140617.address.node.connector.Addresses;
import org.opendaylight.yang.gen.v1.urn.opendaylight.host.tracker.rev140624.HostNode;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.NetworkTopology;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.NodeId;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.TopologyId;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.Topology;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.TopologyKey;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.topology.Link;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.topology.Node;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;

import edu.uci.ics.jung.algorithms.shortestpath.DijkstraShortestPath;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseMultigraph;
import edu.uci.ics.jung.graph.util.EdgeType;

/**
 * Topology Helper class keeps an updated view of the Openflow network
 */
public class TopologyHelper {

	private static final Logger LOG = LoggerFactory.getLogger(TopologyHelper.class);
	private Graph<NodeId, Link> networkGraph =  null;
	private DijkstraShortestPath<NodeId, Link> dijkstraShortestPath =  null;
	private Map<String,NodeId> ipNodeIdsMap = new HashMap<String,NodeId>();
	private Map<NodeId,Node> nodeIdsMap = new HashMap<NodeId,Node>();

  /**
	 * Update topology information.
	 *
	 * @param dataBroker the data broker
	 * @param topologyId the topology id
	 */
	public synchronized void update(DataBroker dataBroker, String topologyId) {
		update(dataBroker,null,topologyId);
	}

	/**
	 * Update topology information.
	 *
	 * @param dataBroker the data broker
	 * @param notificationProvider the notification service
	 * @param topologyId the topology id
	 */
	public synchronized void update(DataBroker dataBroker, NotificationProviderService notificationProvider, String topologyId) {

		LOG.info("network graph model has changed, checking new values");

		try {

			Topology topology = null;

			//TODO read openflow topology. Following lines have to create
			// a read transaction and read from topology network
			// once Topology object is obtained, the rest of the code
			// in this class will take care of updating the graph


			List<Link> topologyLinks = (topology != null)?topology.getLink():null;

			Graph<NodeId, Link> networkGraphTmp = new SparseMultigraph<>();

			if (topologyLinks != null){
				for (Link link : topologyLinks) {
					NodeId sourceNodeId = link.getSource().getSourceNode();
					NodeId destinationNodeId = link.getDestination().getDestNode();
					networkGraphTmp.addVertex(sourceNodeId);
					networkGraphTmp.addVertex(destinationNodeId);
					networkGraphTmp.addEdge(link, sourceNodeId, destinationNodeId, EdgeType.DIRECTED);
					//LOG.info("added link " + link);
				}
			}

			Map<String,NodeId> ipNodeIdsMapTmp = new HashMap<String, NodeId>();
			Map<NodeId,Node> nodeIdsMapTmp = new HashMap<NodeId, Node>();

			if (topology != null && topology.getNode() !=null && !topology.getNode().isEmpty()){

				//add each node to the Map
				for (Node node : topology.getNode()){
					nodeIdsMapTmp.put(node.getNodeId(), node);

					//if node is a host, also keep tracking of ip addresses
					String nodeName = node.getNodeId().getValue();
					if (nodeName != null && nodeName.startsWith("host")){

						NodeId value = node.getNodeId();

						//read host augmentation to be able to read
						//host addresses information
						HostNode hostNode = node.getAugmentation(HostNode.class);
						if (hostNode != null && hostNode.getAddresses() != null){

							for (Addresses address: hostNode.getAddresses()){
								if (address.getIp() != null && address.getIp().getIpv4Address() != null){
									LOG.info("adding ip " + address.getIp().getIpv4Address().getValue());
									ipNodeIdsMapTmp.put(address.getIp().getIpv4Address().getValue(),value);
								}else if (address.getIp() != null && address.getIp().getIpv4Address() != null){
									LOG.info("adding ip " + address.getIp().getIpv6Address().getValue());
									ipNodeIdsMapTmp.put(address.getIp().getIpv6Address().getValue(),value);
								}
							}
						}
					}
				}
			}

			//find new Openflow devices and create the meters
			for (Map.Entry<NodeId, Node> entry: nodeIdsMapTmp.entrySet()){
				String nodeName = entry.getKey().getValue();
				if (nodeName!= null && !nodeIdsMap.containsKey(entry.getKey())){
					if ( nodeName.startsWith("openflow") ){
						//TODO create meter on openflow device
						LOG.info("added new openflow device '" + nodeName + "'");
					}
					//TODO send a notification when a new node is added
				}
			}

			//find removed Openflow devices and remove all their configuration
			for (Map.Entry<NodeId, Node> entry: nodeIdsMap.entrySet()){
				String nodeName = entry.getKey().getValue();
				if (nodeName!= null && !nodeIdsMapTmp.containsKey(entry.getKey())){
					if (nodeName.startsWith("openflow")){
						//TODO remove device from openflow model
						LOG.info("removed openflow device '" + nodeName + "'");
					}
					//TODO send a notification when a new node is removed
				}
			}

			ipNodeIdsMap = ipNodeIdsMapTmp;
			nodeIdsMap = nodeIdsMapTmp;
			networkGraph = networkGraphTmp;
			dijkstraShortestPath = new DijkstraShortestPath<NodeId, Link>(networkGraph);

		} catch (Exception e) {
			LOG.error("cannot update networ graph service " + e, e);
		}
	}			

	/**
	 * Gets the path between two hosts based on Dijkstra shortest path algorithm
	 *
	 * @param source the source host
	 * @param destination the destination host
	 * @return the path
	 */
	public List<Link> getPath(String source, String destination ) {
		NodeId sourceNodeId = (source != null)?ipNodeIdsMap.get(source):null;
		NodeId destinationNodeId = (destination != null)?ipNodeIdsMap.get(destination):null;

		if (sourceNodeId != null && destinationNodeId != null){
			return dijkstraShortestPath.getPath(sourceNodeId, destinationNodeId);
		}

		LOG.info("source '" + source  + "' or destination '" + destination + "' has not been found");
		return null;
	}

  /**
   * Gets the nodes.
   *
   * @return the nodes
   */
  public Map<NodeId,Node> getNodes(){
    return nodeIdsMap;
  }
}
```

## Reading from a model

Edit TopologyHelper class and include the lines required to get the topology. These lines will be placed in the first when the TODO comment `//TODO read openflow topology` is located.

Steps for reading from a transaction:

* create a read transaction from **dataBroker**
* create topology identifier exactly the same as we created it for topology change listener.
* execute read operation in the transaction using LogicalDatastoreType.OPERATIONAL datastore and the identifier.
* read method returns a CheckedMethod. Just call get method from the this object and it returns an Optional<Topology> object
* finally, if **isPresent** method in Optional<Topology> returns true call get method and save the result on topology variable



```
      ReadOnlyTransaction readOnlyTransaction = dataBroker.newReadOnlyTransaction();
			try {

				//create topology identifier
				InstanceIdentifier<Topology> topologyInstanceIdentifier = InstanceIdentifier.builder(NetworkTopology.class)
		                .child(Topology.class, new TopologyKey(new TopologyId(topologyId)))
		                .build();

				Optional<Topology> topologyOptional = readOnlyTransaction.read(
						LogicalDatastoreType.OPERATIONAL,topologyInstanceIdentifier).get();
				if (topologyOptional.isPresent()) {
					topology = topologyOptional.get();
				}
			} catch (Exception e) {
				LOG.error("Error reading topology '" + topologyId +"'",e);
				readOnlyTransaction.close();
				throw new RuntimeException("Error reading from operational store, topology : " + topologyId, e);
			}
```


## Update graph every time there is a change

Add following variable to **BandwidthSchedulerProvider.java** class that will be used to keep an updated view of the dijkstra graph.

```
private TopologyHelper topologyHelper = new TopologyHelper();
```

Update **TopologyDataChangeListener** private class to update the topology every time that the topology change. Notice we are using a background thread to update the topology. This is best practices because the thread used by md-sal should not be used to execute heavy tasks or other more md-sal operations. In this case we just start a background threads but there are more efficient ways of using background threads like ScheduledFuture.

```
    @Override
		public void onDataChanged(AsyncDataChangeEvent<InstanceIdentifier<?>, DataObject> change) {
				LOG.info("topology model has been changed");

				// we use a background thread to perform this task
				// to avoid using mdsal thread
				// This lines could be done more efficiently with a scheduled
				// background thread
				new Thread(){
					public void run(){
						topologyHelper.update(dataBroker, "flow:1");		
					}
				}.start();
		}
```


## Creating meters on openflow devices


Some openflow devices support meters which can be used for example to delimit the traffic. This is the use case that we are trying to achieve with out application to limit the bandwidth between two hosts. Everytime that a new openflow device is discovered we will install the meters based on the bandwidth profiles defined.

Create MeterHelper class

```
package com.brocade.odl.workshop.impl;

import java.util.ArrayList;
import java.util.List;

import org.opendaylight.controller.md.sal.binding.api.DataBroker;
import org.opendaylight.controller.md.sal.binding.api.ReadWriteTransaction;
import org.opendaylight.controller.md.sal.common.api.data.LogicalDatastoreType;
import org.opendaylight.controller.md.sal.common.api.data.TransactionCommitFailedException;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.FlowCapableNode;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.meters.Meter;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.meters.MeterBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.meters.MeterKey;
import org.opendaylight.yang.gen.v1.urn.opendaylight.inventory.rev130819.NodeId;
import org.opendaylight.yang.gen.v1.urn.opendaylight.inventory.rev130819.Nodes;
import org.opendaylight.yang.gen.v1.urn.opendaylight.inventory.rev130819.nodes.Node;
import org.opendaylight.yang.gen.v1.urn.opendaylight.inventory.rev130819.nodes.NodeKey;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.BandId;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.MeterBandType;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.MeterFlags;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.MeterId;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.band.type.band.type.DropBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.meter.MeterBandHeadersBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.meter.meter.band.headers.MeterBandHeader;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.meter.meter.band.headers.MeterBandHeaderBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.meter.meter.band.headers.meter.band.header.MeterBandTypesBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.BandwidthProfile;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.CheckedFuture;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;

/**
 * Creates meters on Openflow devices
 */
public class MeterHelper {

	private static final Logger LOG = LoggerFactory.getLogger(MeterHelper.class);

	/**
	 * Creates the meters defined in bandwidh
	 *
	 * @param dataBroker the data broker
	 * @param node the node
	 */
	public void createMeters(DataBroker dataBroker, final String node){
		for (BandwidthProfile profile : BandwidthProfile.values()){
			createMeter(dataBroker, node,profile.getIntValue() , profile.getIntValue());
		}
	}

	/**
	 * Creates the meter.
	 *
	 * @param dataBroker the data broker
	 * @param node the node
	 * @param meterId the meter id
	 * @param rate the rate
	 */
	public void createMeter(DataBroker dataBroker, final String node, final long meterId, long rate){

		MeterId id = new MeterId(meterId);
	    MeterKey key = new MeterKey(id);
	    MeterBuilder meter = new MeterBuilder();
	    meter.setKey(key);
	    meter.setMeterId(id);
	    meter.setFlags(new MeterFlags(false, true, false, false));

	    MeterBandHeadersBuilder bandHeaders = new MeterBandHeadersBuilder();

	    List<MeterBandHeader> bandHdr = new ArrayList<MeterBandHeader>();

	    MeterBandHeaderBuilder bandHeader = new MeterBandHeaderBuilder();
	    bandHeader.setBandRate(rate);
	    bandHeader.setBandBurstSize(0L);	        
	    bandHeader.setBandId(new BandId(0L));
	    bandHeader.setBandType(new DropBuilder().setDropBurstSize(0L).setDropRate(rate).build());
	    bandHeader.setMeterBandTypes(new MeterBandTypesBuilder().setFlags(new MeterBandType(true, false, false)).build());
	    bandHdr.add(bandHeader.build());

	    bandHeaders.setMeterBandHeader(bandHdr);

	    meter.setMeterBandHeaders(bandHeaders.build());


	    // TODO create a write transaction which will
	    // write the meter in the given openflow device
	}
}
```

## Write in a model

Edit MeterHelper class and include the lines required to write a meter in the openflow device. These lines will be placed after the comment `// TODO create a write transaction`, at the end of the method **createMeter**

Steps for writing a meter using transaction:

* create a write transaction from dataBroker
* create meter identifier
* execute write operation in the transaction using LogicalDatastoreType.CONFIGURATION datastore, the identifier and the meter object


```
ReadWriteTransaction modification = dataBroker.newReadWriteTransaction();
InstanceIdentifier<Meter> meterIdentifier = InstanceIdentifier.create(Nodes.class).child(Node.class, new NodeKey(new NodeId(node)))
          .augmentation(FlowCapableNode.class).child(Meter.class, new MeterKey(new MeterId(meterId)));
modification.put(LogicalDatastoreType.CONFIGURATION, meterIdentifier, meter.build(), true);
CheckedFuture<Void, TransactionCommitFailedException> commitFuture = modification.submit();
  Futures.addCallback(commitFuture, new FutureCallback<Void>() {
      @Override
      public void onSuccess(Void aVoid) {
        LOG.info("meter '" + meterId + "' has been successfully installed on '" + node + "'");
      }

      @Override
      public void onFailure(Throwable throwable) {
        LOG.info("error adding meter '" + meterId + "' to '" + node + "'",throwable);
      }
  });

```

## Create meters everytime a new device is detected

Edit **TopologyHelper** class and replace `//TODO create meter on openflow device`

```
meterHelper.createMeters(dataBroker, nodeName);
```

Also, add following variable to TopologyHelper

```
private MeterHelper meterHelper = new MeterHelper();
```


## Testing

Openvswitch does not currently support Meters. Mininet also provides the ability to use user space based openflow switch implemented by CPqD. This switch performance is not as good as Open vSwitch but it provides meter implementation.

Install the new version of the application in the controller and create a mininet network using CPqD switch. Option `--switch user` is the one that tells minet to use CPqD switch.

```
sudo mn --topo tree,3 --mac --switch user --controller=remote,ip=10.0.0.6,port=6633
```

As soon as you start the mininet network, TopologyHelper receives an update and detects new openflow devices. Then, it will install the 3 meters (gold, silver and bronze). You can verify it checking for logs like following one in  **/opt/bvc/log/controller_logs/karaf.log** file.

```
2015-09-15 09:17:20,358 | INFO  | CommitFutures-4  | MeterHelper                      | 353 - com.brocade.odl.workshop.bandwidthscheduler-impl - 1.0.0.SNAPSHOT | meter '800' has been successfully installed on 'openflow:5'
```


We can also validate the meters has been successfully installed in the switch. For example, to validate meter 100 has been installed in switch s1 then `sudo dpctl  unix:/tmp/s1 stats-meter 100` command must be executed. Use a separated unix shell on the mininet server to run the command.

```
$ sudo dpctl  unix:/tmp/s1 stats-meter 100

SENDING (xid=0xF0FF00F0):
stat_req{type="mstats", flags="0x0"{meter_id= 64"}


RECEIVED (xid=0xF0FF00F0):
stat_repl{type="mstats", flags="0x0", stats=[{meter= 64"", flow_cnt="0", pkt_in_cnt="0", byte_in_cnt="0", duration_sec="15", duration_nsec="643000000", bands=[{pkt_band_cnt="0", byte_band_cnt="0"}]}]}
```
