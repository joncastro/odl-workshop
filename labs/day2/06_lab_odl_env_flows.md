# Lab: Adding flows

## Prerequisites

Students should have already completed the following lab:

* Creating transactions

## Overview

In this lab we will create flows in openflow devices to delimit the bandwidth between two hots. Also, it will configure all the Openflow device to ensure the traffic goes through the devices detected by the shortest path algorithm.

---

## Openflow Helper class

Create OpenflowHelper class with following content

```
package com.brocade.odl.workshop.impl;

import java.util.ArrayList;
import java.util.List;

import org.opendaylight.controller.md.sal.binding.api.DataBroker;
import org.opendaylight.controller.md.sal.binding.api.ReadWriteTransaction;
import org.opendaylight.controller.md.sal.common.api.data.LogicalDatastoreType;
import org.opendaylight.controller.md.sal.common.api.data.TransactionCommitFailedException;
import org.opendaylight.yang.gen.v1.urn.ietf.params.xml.ns.yang.ietf.inet.types.rev100924.Ipv4Prefix;
import org.opendaylight.yang.gen.v1.urn.ietf.params.xml.ns.yang.ietf.inet.types.rev100924.Uri;
import org.opendaylight.yang.gen.v1.urn.opendaylight.action.types.rev131112.action.action.OutputActionCaseBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.action.types.rev131112.action.action.output.action._case.OutputActionBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.action.types.rev131112.action.list.Action;
import org.opendaylight.yang.gen.v1.urn.opendaylight.action.types.rev131112.action.list.ActionBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.FlowCapableNode;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.FlowId;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.tables.Table;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.tables.TableKey;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.tables.table.Flow;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.tables.table.FlowBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.tables.table.FlowKey;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.flow.InstructionsBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.flow.Match;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.flow.MatchBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.instruction.instruction.ApplyActionsCaseBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.instruction.instruction.MeterCaseBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.instruction.instruction.apply.actions._case.ApplyActionsBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.instruction.instruction.meter._case.MeterBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.instruction.list.Instruction;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.instruction.list.InstructionBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.instruction.list.InstructionKey;
import org.opendaylight.yang.gen.v1.urn.opendaylight.inventory.rev130819.NodeId;
import org.opendaylight.yang.gen.v1.urn.opendaylight.inventory.rev130819.Nodes;
import org.opendaylight.yang.gen.v1.urn.opendaylight.inventory.rev130819.nodes.Node;
import org.opendaylight.yang.gen.v1.urn.opendaylight.inventory.rev130819.nodes.NodeKey;
import org.opendaylight.yang.gen.v1.urn.opendaylight.l2.types.rev130827.EtherType;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.MeterId;
import org.opendaylight.yang.gen.v1.urn.opendaylight.model.match.types.rev131026.ethernet.match.fields.EthernetTypeBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.model.match.types.rev131026.match.EthernetMatchBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.model.match.types.rev131026.match.layer._3.match.Ipv4MatchBuilder;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.google.common.util.concurrent.CheckedFuture;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;

/**
 * A helper to create flows and meters
 */
public class OpenflowHelper {

	private static final Logger LOG = LoggerFactory.getLogger(OpenflowHelper.class);

	private final short DEFAULT_TABLE_ID = (short)0;
	private final int DEFAULT_PRIORITY = 32768;
	private final int DEFAULT_ID_TIMEOUT = 0;

	/**
	 * Creates the flow for the given meter, source and destination.
	 *
	 * @param dataBroker the data broker
	 * @param node the node
	 * @param source the source
	 * @param destination the destination
	 * @param outputPort the output port
	 * @param meterId the meter id
	 * @param seconds duration in seconds
	 */
	public void createFlow(DataBroker dataBroker, final String node, String source, String destination, String outputPort, long meterId, int seconds){

		LOG.info(String.format("creating flow '%s' source '%s' destination '%s' outputPort '%s' meterId '%s' seconds '%s'",node,source,destination,outputPort,meterId,seconds) );

		if (!source.contains("/")){
			source = source + "/32";
		}

		if (!destination.contains("/")){
			destination = destination + "/32";
		}

		//final String flowId = source+"-"+destination+"-"+meterId + "-"+ seconds + ((seconds > 0)? "-" + System.currentTimeMillis():"");
		final String flowId = "bandwidth-" + source+"-"+destination+"-"+meterId + "-"+ seconds;


		LOG.info(String.format("flow id '%s' on  node '%s' ",flowId,node));

		//create all meter properties
        List<Instruction> instructions = new ArrayList<Instruction>();

        List<Action> actionList = new ArrayList<Action>();
        actionList.add(new ActionBuilder()
        		.setOrder(0)
        		.setAction(new OutputActionCaseBuilder()
        				.setOutputAction(new OutputActionBuilder()
        				.setOutputNodeConnector(new Uri(outputPort)).build())
        				.build())
        		.build());


        instructions.add(new InstructionBuilder()
        		.setOrder(0)
        		.setInstruction(new MeterCaseBuilder()
        			.setMeter(new MeterBuilder().setMeterId(new MeterId(meterId)).build()).build())
        			.setKey(new InstructionKey(0)).build());

        instructions.add(new InstructionBuilder()
        		.setOrder(1)
        		.setInstruction(new ApplyActionsCaseBuilder().setApplyActions(new ApplyActionsBuilder().setAction(actionList).build()).build())
        		.build());


        Match match = new MatchBuilder()
        	.setLayer3Match(new Ipv4MatchBuilder().setIpv4Source(new Ipv4Prefix(source)).setIpv4Destination(new Ipv4Prefix(destination)).build())
        	.setEthernetMatch(new EthernetMatchBuilder().setEthernetType(new EthernetTypeBuilder().setType(new EtherType(0x0800L)).build()).build())
        	.build();

        Flow flow = new FlowBuilder()
        	.setId(new FlowId(flowId))
	        .setTableId(DEFAULT_TABLE_ID)
	        .setHardTimeout(seconds)
	        .setIdleTimeout(DEFAULT_ID_TIMEOUT)
	        .setInstructions(new InstructionsBuilder().setInstruction(instructions).build())	        
	        .setPriority(DEFAULT_PRIORITY)
	        .setMatch(match)
	        .build();

        // write the meter configuration in the configuration data store
	    ReadWriteTransaction modification = dataBroker.newReadWriteTransaction();

	    InstanceIdentifier<Flow> identifier = InstanceIdentifier.create(Nodes.class).child(Node.class, new NodeKey(new NodeId(node)))
	                	.augmentation(FlowCapableNode.class)
	                	.child(Table.class, new TableKey(new Short(DEFAULT_TABLE_ID)))
	                	.child(Flow.class, new FlowKey(new FlowId(flowId)));

	    modification.merge(LogicalDatastoreType.CONFIGURATION, identifier, flow, true);

	    CheckedFuture<Void, TransactionCommitFailedException> commitFuture = modification.submit();

        Futures.addCallback(commitFuture, new FutureCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            	LOG.info("flow '" + flowId + "' has been sucessfully installed on '" + node + "'");
            }

            @Override
            public void onFailure(Throwable throwable) {
            	LOG.info("error adding flow '" + flowId + "' to '" + node + "'",throwable);
            }
        });
	}

	/**
	 * Creates the flow for the given meter, source and destination.
	 *
	 * @param dataBroker the data broker
	 * @param node the node
	 * @param source the source
	 * @param destination the destination
	 * @param outputPort the output port
	 * @param meterId the meter id
	 * @param seconds duration in seconds
	 */
	public void deleteFlow(DataBroker dataBroker, final String node, String source, String destination, long meterId, int seconds){

		LOG.info(String.format("deleting flow '%s' source '%s' destination '%s' meterId '%s' seconds '%s'",node,source,destination,meterId,seconds) );

		if (!source.contains("/")){
			source = source + "/32";
		}

		if (!destination.contains("/")){
			destination = destination + "/32";
		}

		final String flowId = "bandwidth-" + source+"-"+destination+"-"+meterId + "-"+ seconds;


        // write the meter configuration in the configuration data store
	    ReadWriteTransaction modification = dataBroker.newReadWriteTransaction();

	    InstanceIdentifier<Flow> identifier = InstanceIdentifier.create(Nodes.class).child(Node.class, new NodeKey(new NodeId(node)))
	                	.augmentation(FlowCapableNode.class)
	                	.child(Table.class, new TableKey(new Short(DEFAULT_TABLE_ID)))
	                	.child(Flow.class, new FlowKey(new FlowId(flowId)));

	    Optional<Flow> flow;
		try {
			 flow = modification.read(LogicalDatastoreType.CONFIGURATION, identifier).get();
			 if (flow.isPresent()){
			    	modification.delete(LogicalDatastoreType.CONFIGURATION, identifier);
			 }
		} catch (Exception e) {
			LOG.error("cannot read or delete flow",e);
		}


	    CheckedFuture<Void, TransactionCommitFailedException> commitFuture = modification.submit();

        Futures.addCallback(commitFuture, new FutureCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            	LOG.info("flow '" + flowId + "' deleted been sucessfully installed on '" + node + "'");
            }

            @Override
            public void onFailure(Throwable throwable) {
            	LOG.info("error deleting flow '" + flowId + "' to '" + node + "'",throwable);
            }
        });
	}

	/**
	 * Delete openflow node configuration from data store
	 *
	 * @param dataBroker the data broker
	 * @param node the node
	 */
	public void deleteNode(DataBroker dataBroker, final String node){

		ReadWriteTransaction modification = dataBroker.newReadWriteTransaction();
		InstanceIdentifier<Node> identifier = InstanceIdentifier.create(Nodes.class).child(Node.class, new NodeKey(new NodeId(node)));
		modification.delete(LogicalDatastoreType.CONFIGURATION, identifier);
	    CheckedFuture<Void, TransactionCommitFailedException> commitFuture = modification.submit();

	    Futures.addCallback(commitFuture, new FutureCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            	LOG.info("node '" + node + "' configuration has been removed");
            }

            @Override
            public void onFailure(Throwable throwable) {
            	LOG.info("error removing node '" + node + "'",throwable);
            }
        });

	}
}
```


## Add flows to the network

Every time bandwidthscheduler model changes we will require to update the Openflow network to add or remove the Openflow network to reflect the changes.

We are going to improve the onDataChanged method on **BandwidthSchedulerDataChangeListener** private class located in **BandwidthSchedulerProvider.java** file.

First, add OpenflowHelper variable to **BandwidthSchedulerProvider.java** class.

```
private OpenflowHelper openflowHelper = new OpenflowHelper();
```

Add following two methods to BandwidthSchedulerDataChangeListener class which removes and add limits to the network.

Adding a new bandwidth limitation is done in two steps:

* find the shortest path between the two hosts
* program all the devices in the path 

In the case of deleting, we just remove the flow from all openflow devices. We know some devices could not have it but we do not know which devices were programmed. To do this more efficiently, we should improve the YANG model and add operational data to contain the path being used when the limit was added. This way, we could just add from the operational data the devices being using in the path between the two hosts and just remove the flows from those devices.

```
  private void addLimit(Limit limit){

		String source = (limit.getSource().getIpv4Address()!=null)?limit.getSource().getIpv4Address().getValue():limit.getSource().getIpv6Address().getValue();
		String destination = (limit.getDestination().getIpv4Address()!=null)?limit.getDestination().getIpv4Address().getValue():limit.getDestination().getIpv6Address().getValue();

		List<Link> shortLinks =  topologyHelper.getPath(source, destination);
		if (shortLinks != null && !shortLinks.isEmpty()){
			for(Link link: shortLinks){

				String sourceNode = link.getSource().getSourceNode().getValue();
				String sourceTP = link.getSource().getSourceTp().getValue();

				// when source in an openflow device we need to program a flow
				// on the switch. This cover the case which
				// source = openflow destination = openflow
				// source = openflow destination = host
				if (sourceNode.startsWith("openflow")){
					String port = sourceTP.substring(sourceTP.lastIndexOf(":")+1,sourceTP.length());
					openflowHelper.createFlow(dataBroker, sourceNode, source, destination, port, (long)limit.getBandwidth().getIntValue(), limit.getDurationSec());
				}
			}
		}else {
			LOG.error("path not found between '" + source + "' and '" + destination + "'" );
		}
	}

  private void removeLimit(Limit limit){
		// at the moment, we are not saving the path in
		// bandwidth scheduler model so we just try
		// to remove the limit from all nodes
		// we know in advance that some nodes it not have
		// that limit

		String source = (limit.getSource().getIpv4Address()!=null)?limit.getSource().getIpv4Address().getValue():limit.getSource().getIpv6Address().getValue();
		String destination = (limit.getDestination().getIpv4Address()!=null)?limit.getDestination().getIpv4Address().getValue():limit.getDestination().getIpv6Address().getValue();
		for (NodeId node: topologyHelper.getNodes().keySet()){
			LOG.info("removing node in '" + node.getValue());
			if (node.getValue().startsWith("openflow")){
				LOG.info("calling removing flow in node in '" + node.getValue() + " limit " + limit);
				openflowHelper.deleteFlow(dataBroker, node.getValue(), source, destination, limit.getBandwidth().getIntValue(), limit.getDurationSec());
			}
		}
	}
```


Then, update **onDataChanged** method on **BandwidthSchedulerDataChangeListener** private class. **Change** object given as an input parameters contains the data that has been created, updated or deleted. Following code covers all the 3 cases.

```
public void onDataChanged(AsyncDataChangeEvent<InstanceIdentifier<?>, DataObject> change) {
				LOG.info("bandwidth scheduler model has been changed");

				final AsyncDataChangeEvent<InstanceIdentifier<?>, DataObject> dataChange = change;

				// we use a background thread to perform this task
				// to avoid using mdsal thread
				// This lines could be done more efficiently with a scheduled
				// background thread
				new Thread(){
					public void run(){
						if (dataChange.getCreatedData() != null){
							for (Map.Entry<InstanceIdentifier<?>, DataObject> entry: dataChange.getCreatedData().entrySet()){
								if (entry.getKey().getTargetType().isAssignableFrom(Limit.class)){
									LOG.info("add entry is limit: " + entry.getKey());
									Limit limit = (Limit) entry.getValue();
									addLimit(limit);
								}
							}
						}

						if (dataChange.getUpdatedData() != null){
							for (Map.Entry<InstanceIdentifier<?>, DataObject> entry: dataChange.getUpdatedData().entrySet()){
								if (entry.getKey().getTargetType().isAssignableFrom(Limit.class)){
									LOG.info("update entry is limit: " + entry.getKey());
									Limit limit = (Limit) dataChange.getOriginalData().get(entry.getKey());
									removeLimit(limit);
									limit = (Limit) entry.getValue();
									addLimit(limit);
								}
							}
						}

						if (dataChange.getRemovedPaths() != null){
							for (InstanceIdentifier<?> path: dataChange.getRemovedPaths()){
								LOG.info("removed path: " + path);
								if (path.getTargetType().isAssignableFrom(Limit.class)){
									LOG.info("removed entry is limit: " + path);
									Limit limit = (Limit) dataChange.getOriginalData().get(path);
									removeLimit(limit);
								}

							}
						}
					}
				}.start();
		}
	}
```


### Testing 


After compiling `mvn clean install -Dcheckstyle.skip=true -DskipTests -nsu` and deploying the kar file in the deploy folder (remember to call clean.sh before copying the kar file) our new application will install bandwidth limitation between two hosts.

First, create a mininet network using user space switch CPqD as explained in previous labs.

```
$ sudo mn --topo tree,3 --mac --switch user --controller=remote,ip=10.0.0.6,port=6633
```

Then, execute `pingall` so the controller can discover all the hosts.

```
mininet> pingall
*** Ping: testing ping reachability
h1 -> h2 h3 h4 h5 h6 h7 h8
h2 -> h1 h3 h4 h5 h6 h7 h8
h3 -> h1 h2 h4 h5 h6 h7 h8
h4 -> h1 h2 h3 h5 h6 h7 h8
h5 -> h1 h2 h3 h4 h6 h7 h8
h6 -> h1 h2 h3 h4 h5 h7 h8
h7 -> h1 h2 h3 h4 h5 h6 h8
h8 -> h1 h2 h3 h4 h5 h6 h7
*** Results: 0% dropped (56/56 received)
mininet>
```

Now, test the bandwidth between two hosts. For example, h1 and h8. We can use iperf command available in mininet for this purpose.

```
mininet> iperf h1 h8
*** Iperf: testing TCP bandwidth between h1 and h8
*** Results: ['877 Kbits/sec', '954 Kbits/sec']
mininet>
```

Now, we are going to add a bronze bandwidth limit between h1 and h8 in both directions in the network. Executing HTTP PUT operations using following url `http://<odl-ip>:8181/restconf/config/bandwidthscheduler:scheduled-bandwidth`

Header 

```
Authorization : Basic YWRtaW46YWRtaW4=
Accept : application/json
Content-Type : application/json
```

and following payload

```
{
    "scheduled-bandwidth": {
        "limit": [
            {
                "source": "10.0.0.1",
                "destination": "10.0.0.8",
                "bandwidth": "bronze",
                "duration-sec": 100
            },
          	{
                "source": "10.0.0.8",
                "destination": "10.0.0.1",
                "bandwidth": "bronze",
                "duration-sec": 100
            }
        ]
    }
}
```

If you execute iperf command again you will notice a significant reduce of the bandwidth.

```
mininet> iperf h1 h8
*** Iperf: testing TCP bandwidth between h1 and h8
*** Results: ['32.2 Kbits/sec', '75.7 Kbits/sec']
mininet>
```

Remember that we have set **duration-sec** to 100 seconds in the payload. If you wait a couple of minutes and execute again the iperf you will see the performance is similar as the one done initially. We can set **hard-time** attribute in the flow when add it to the device which defines the maximum time that the flow will be live in the device. **duration-sec** is used for this purpose.

```
mininet> iperf h1 h8
*** Iperf: testing TCP bandwidth between h1 and h8
*** Results: ['877 Kbits/sec', '957 Kbits/sec']
mininet>
```
