# Lab: Creating RPC Calls

## Prerequisites

Students should have already completed the following lab:

* Adding flows

## Overview

In this lab we will add a RPC call to discover the path between two hosts using the shortest path.

---


## Update YANG model

Add network-topology dependency to **api/pom.xml** file because we will reuse **link-attributes** from that model.

```
  <dependency>
    <groupId>org.opendaylight.controller.model</groupId>
    <artifactId>model-topology</artifactId>
    <version>1.2.0-Lithium</version>
  </dependency>
```

Edit **bandwidthscheduler.yang** file and import Topology. This allow us to use type definition and others from this model. In this case, we will reuse **link-attributes**.

```
import network-topology {
  prefix "topology";
  revision-date 2013-10-21;
}
```

and add following rpc call named get-path, which will return the path between two hosts.

```
rpc get-path {
  description
    "return the links between two paths";
  input {
    leaf source {
      type inet:ip-address;
      mandatory true;
      description "source ip address";
    }

    leaf destination {
      type inet:ip-address;
      mandatory true;
      description "destination ip address";
    }
  }
  output {
    list link{
      key link-id;
      uses topology:link-attributes;
    }      
  }
}
```

Execute **mvn clean install -Dcheckstyle.skip=true -DskipTests -nsu** command to update the auto-generated model classes. This will create a **BandwidthschedulerService** which contains the `getPath` method to be implemented.

## Implement RPC service

Edit **BandwidthSchedulerProvider.java** file and add **BandwidthschedulerService** interface in the implementation header of the class.


```
public class BandwidthSchedulerProvider implements BindingAwareProvider, AutoCloseable, BandwidthschedulerService {
```

Add the implementation of **getPath** method.

```
/* (non-Javadoc)
 * @see org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.BandwidthschedulerService#getPath(org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.GetPathInput)
 */
@Override
public Future<RpcResult<GetPathOutput>> getPath(GetPathInput input) {
  List<org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.get.path.output.Link> links = new ArrayList<org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.get.path.output.Link>();

  String source = null;
  if (input.getSource() != null && input.getSource().getIpv4Address() != null){
    source = input.getSource().getIpv4Address().getValue();
  }else if (input.getSource() != null && input.getSource().getIpv6Address() != null){
    source = input.getSource().getIpv6Address().getValue();
  }

  String destination = null;
  if (input.getDestination() != null && input.getDestination().getIpv4Address() != null){
    destination = input.getDestination().getIpv4Address().getValue();
  }else if (input.getDestination() != null && input.getDestination().getIpv6Address() != null){
    destination = input.getDestination().getIpv6Address().getValue();
  }

  List<Link> shortLinks = topologyHelper.getPath(source, destination);
  if (shortLinks != null && !shortLinks.isEmpty()){
    for(Link link: shortLinks){
      LOG.info("getpath: adding link result '" + link.getLinkId() + "'");
      links.add(new org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.get.path.output.LinkBuilder()
      .setLinkId(link.getLinkId())
      .setDestination(link.getDestination())
      .setSource(link.getSource())
      .setSupportingLink(link.getSupportingLink())
      .build());
    }
  }else{
    LOG.info("path not found between source '" + source  + "' or destination '" + destination + "'");
  }

  return Futures.immediateFuture(RpcResultBuilder.<GetPathOutput>success(new GetPathOutputBuilder().setLink(links)).build());
}
```

Add following variables

```
private RpcProviderRegistry rpcRegistryDependency = null;
private BindingAwareBroker.RpcRegistration<BandwidthschedulerService> rpcRegistration = null;
```

and include following setter which will receive the rpc registration dependency and register the provider. This registration tells to the controller that if anyone wants to execute **get-path** RPC call our provider instance should be called.

```
public void setRpcProviderRegistry(RpcProviderRegistry rpcRegistryDependency){
  this.rpcRegistryDependency = rpcRegistryDependency;

  rpcRegistration = this.rpcRegistryDependency.addRpcImplementation(BandwidthschedulerService.class,this);

}
```

Add following line to **close** method to ensure the registration is closed when the provider goes down. It is best practices to close the resources.

```
if (rpcRegistration!=null)rpcRegistration.close();
```

Above code requires following imports

```
import java.util.concurrent.Future;
import org.opendaylight.yangtools.yang.common.RpcResult;
import org.opendaylight.yangtools.yang.common.RpcResultBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.GetPathInput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.GetPathOutput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.GetPathOutputBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.BandwidthschedulerService;
import org.opendaylight.controller.sal.binding.api.RpcProviderRegistry;
import org.opendaylight.controller.sal.binding.api.BindingAwareBroker;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.BandwidthschedulerService;
```

## Add rpc dependency and register our service


To be able to use **RPC service** it is required to add **rcp-registry** module to the project. Similar as the data broker service this requires following two steps.

Edit **impl/src/main/config/default-config.xml** file and include following new module.

```
<rpc-registry>
    <type xmlns:binding="urn:opendaylight:params:xml:ns:yang:controller:md:sal:binding">binding:binding-rpc-registry</type>
    <name>binding-rpc-broker</name>
</rpc-registry>
```

Then, edit **impl/src/main/yang/bandwidthscheduler-impl.yang** and include following container data broker.

```
container rpc-registry {
    uses config:service-ref {
        refine type {
            mandatory true;
            config:required-identity md-sal-binding:binding-rpc-registry;
        }
    }
}
```

At this moment, it is preferible to execute `mvn clean install -Dcheckstyle.skip=true -DskipTests -nsu` because it will force some auto-generated classes include above rpc dependency and we will be able to use it in our classes without compiling errors.

Once rpc is available we could obtain the data broker from **org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.impl.rev141210.BandwidthSchedulerModule** class and register our provider to listen to any changes.

Edit **BandwidthSchedulerModule.java** class and set the **RPC provider** on the provider on `createInstance` method.

```
RpcProviderRegistry rpcRegistryDependency = getRpcRegistryDependency();
provider.setRpcProviderRegistry(rpcRegistryDependency);
```

Above lines requires following import

```
import org.opendaylight.controller.sal.binding.api.RpcProviderRegistry;
```


### Testing 

After compiling `mvn clean install -Dcheckstyle.skip=true -DskipTests -nsu` and deploying the kar file in the deploy folder (remember to call clean.sh before copying the kar file) our new application will be able to provide the path between two hosts through the **get-path** method.

First, create a mininet network

```
$ sudo mn --topo tree,3 --mac --switch user --controller=remote,ip=10.0.0.6,port=6633
```

and then, execute `pingall` so the controller can discover all the hosts.

```
mininet> pingall
*** Ping: testing ping reachability
h1 -> h2 h3 h4 h5 h6 h7 h8
h2 -> h1 h3 h4 h5 h6 h7 h8
h3 -> h1 h2 h4 h5 h6 h7 h8
h4 -> h1 h2 h3 h5 h6 h7 h8
h5 -> h1 h2 h3 h4 h6 h7 h8
h6 -> h1 h2 h3 h4 h5 h7 h8
h7 -> h1 h2 h3 h4 h5 h6 h8
h8 -> h1 h2 h3 h4 h5 h6 h7
*** Results: 0% dropped (56/56 received)
mininet>
```


Finally, send a HTTP POST operations using following url `http://<odl-ip>:8181/restconf/operations/bandwidthscheduler:get-path`

Header 

```
Authorization : Basic YWRtaW46YWRtaW4=
Accept : application/json
Content-Type : application/json
```

and following payload

```
{
  input : {
   source: "10.0.0.1",
   destination: "10.0.0.8"
  }
}
```

It will return the links defining the path between the two hosts if found.

