# Lab: Adding notifications

## Prerequisites

Students should have already completed the following lab:

* Creating RPC Calls

## Overview

In this lab we will create a couple of notifications for everytime that bandwidthscheduler discover a new device or a device is removed.

---


## Update YANG model

Add following notifications. **node-add** will produce a notification if an openflow device is added and **node-removed** if an openflow device is removed.

```
notification node-added {
  description "a new node has been introduced in the topology";
  leaf name {
    type string;
    description "node name";
  }
}

notification node-removed {
  description "an existing node has been removed from the topology";
  leaf name {
    type string;
    description "node name";
  }
}
```

Execute **mvn clean install -Dcheckstyle.skip=true -DskipTests -nsu** command to update the auto-generated model classes. New model will produce **NodeAddedBuilder** and **NodeRemovedBuilder** classes.

## Implement Notification service

Edit **BandwidthSchedulerProvider.java** file and add following variable. We will keep the reference in the provider class to the notification service.

```
private NotificationProviderService notificationProvider;
```

and include following setter which will receive the notification service registration dependency.

```
	/**
	 * Sets the notification provider.
	 *
	 * @param notificationProvider the new notification provider
	 */
	public void setNotificationProvider(
			NotificationProviderService notificationProvider) {
		this.notificationProvider = notificationProvider;
	}
```

Change following line located **TopologyDataChangeListener** private class

from

```
topologyHelper.update(dataBroker, "flow:1");
```

to

```
topologyHelper.update(dataBroker, notificationProvider, "flow:1");
```

We will pass the notification service to the topology helper because this class is the one that detects changes in the openflow network.

## Update topologyHelper

Update topology helper class which will create a notification every time that a node is added or removed. 

Sending a notification is as simple of calling the **publish** method on the notification service given the instance of the notification class.

Replace `//TODO send a notification when a new node is added` with following lines

```
if (notificationProvider != null){
  notificationProvider.publish(new NodeAddedBuilder().setName(nodeName).build());
  LOG.info("notification sent for added openflow device '" + nodeName + "'");
}

```

and `//TODO send a notification when a new node is removed` for

```
if (notificationProvider != null){
  notificationProvider.publish(new NodeRemovedBuilder().setName(nodeName).build());
  LOG.info("notification sent for removed openflow device '" + nodeName + "'");
}
```

above lines requires following imports

```
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.NodeAddedBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.NodeRemovedBuilder;
```

## Add rpc dependency and register our service


To be able to use notification service it is required to add **notification-service** module to the project. Similar to data broker and RPC this requires following two steps:

* Edit **impl/src/main/config/default-config.xml** file and include following new module.

```
<notification-service>
    <type xmlns:binding="urn:opendaylight:params:xml:ns:yang:controller:md:sal:binding">binding:binding-notification-service</type>
    <name>binding-notification-broker</name>
</notification-service>  
```

* Edit **impl/src/main/yang/bandwidthscheduler-impl.yang** and include following container data broker.

```
container notification-service {
    uses config:service-ref {
        refine type {
            mandatory true;
            config:required-identity md-sal-binding:binding-notification-service;
        }
    }
}
```

At this moment, it is preferible to execute **mvn clean install -Dcheckstyle.skip=true -DskipTests -nsu** because it will force some auto-generated classes include above notifcation dependency and we will be able to use it in our classes without compiling errors.

Once notifcation service is available we could obtain the notification service from **org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.impl.rev141210.BandwidthSchedulerModule** class.

Edit **BandwidthSchedulerModule** class and set the notification service on the provider updating `createInstance` method.

```
NotificationProviderService notificationProvider = getNotificationServiceDependency();
provider.setNotificationProvider(notificationProvider);
```

Above lines requires following import

```
import org.opendaylight.controller.sal.binding.api.NotificationProviderService;
```


### Testing


After compiling `mvn clean install -Dcheckstyle.skip=true -DskipTests -nsu` and deploying the kar file in the deploy folder (remember to call clean.sh before copying the kar file) our new application will install bandwidth limitation between two hosts.

As soon as we create a mininet network  using following command

```
$ sudo mn --topo tree,3 --mac --switch user --controller=remote,ip=10.0.0.6,port=6633
```

the application will print a log like following in **/opt/bvc/log/controller_logs/karaf.log** when a new device is detected.

```
2015-09-17 06:12:48,201 | INFO  | Thread-69        | TopologyHelper                   | 353 - com.brocade.odl.workshop.bandwidthscheduler-impl - 1.0.0.SNAPSHOT | notification sent for added openflow device 'openflow:4'
```

Or following one if we exit from mininet network because the device will be removed from the controller.

```
2015-09-17 06:15:41,258 | INFO  | Thread-77        | TopologyHelper                   | 353 - com.brocade.odl.workshop.bandwidthscheduler-impl - 1.0.0.SNAPSHOT | notification sent for removed openflow device 'openflow:7'
```

After executing exit on mininet the remove notification logs should be printed too.
