/*
 * Brocade Communications Systems, Inc and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package com.brocade.odl.workshop.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import org.opendaylight.controller.md.sal.binding.api.DataBroker;
import org.opendaylight.controller.md.sal.binding.api.DataChangeListener;
import org.opendaylight.controller.md.sal.common.api.data.AsyncDataBroker.DataChangeScope;
import org.opendaylight.controller.md.sal.common.api.data.AsyncDataChangeEvent;
import org.opendaylight.controller.md.sal.common.api.data.LogicalDatastoreType;
import org.opendaylight.controller.sal.binding.api.BindingAwareBroker.ProviderContext;
import org.opendaylight.controller.sal.binding.api.BindingAwareBroker;
import org.opendaylight.controller.sal.binding.api.BindingAwareProvider;
import org.opendaylight.controller.sal.binding.api.RpcProviderRegistry;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.BandwidthschedulerService;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.GetPathInput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.GetPathOutput;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.GetPathOutputBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.ScheduledBandwidth;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.scheduled.bandwidth.Limit;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.NetworkTopology;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.NodeId;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.TopologyId;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.Topology;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.TopologyKey;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.topology.Link;
import org.opendaylight.yangtools.concepts.ListenerRegistration;
import org.opendaylight.yangtools.yang.binding.DataObject;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.opendaylight.yangtools.yang.common.RpcResult;
import org.opendaylight.yangtools.yang.common.RpcResultBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.Futures;

public class BandwidthSchedulerProvider implements BindingAwareProvider, AutoCloseable, BandwidthschedulerService {

    private static final Logger LOG = LoggerFactory.getLogger(BandwidthSchedulerProvider.class);

    private RpcProviderRegistry rpcRegistryDependency = null;
    private BindingAwareBroker.RpcRegistration<BandwidthschedulerService> rpcRegistration = null;
    
    private DataBroker dataBroker = null;
    private ListenerRegistration<DataChangeListener> dataChangeTopologyListenerRegistration;
    private ListenerRegistration<DataChangeListener> dataChangeScheduledBandwidthListenerRegistration;
    private TopologyHelper topologyHelper = new TopologyHelper();
    private OpenflowHelper openflowHelper = new OpenflowHelper();
    
    @Override
    public void onSessionInitiated(ProviderContext session) {
        LOG.info("BandwidthSchedulerProvider Session Initiated");
    }

    @Override
    public void close() throws Exception {
        LOG.info("BandwidthSchedulerProvider Closed");
        if (dataChangeTopologyListenerRegistration!=null)dataChangeTopologyListenerRegistration.close();
        if (dataChangeScheduledBandwidthListenerRegistration!=null)dataChangeScheduledBandwidthListenerRegistration.close();
        if (rpcRegistration!=null)rpcRegistration.close();
    }

    public void setDataBroker(DataBroker dataBroker){
    	this.dataBroker = dataBroker;
    	
    	// this listener will list to any change on the following url
    	// http://<odl-ip>:8181/restconf/operational/network-topology:network-topology/topology/flow:1
       dataChangeTopologyListenerRegistration =
        		this.dataBroker.registerDataChangeListener(LogicalDatastoreType.OPERATIONAL,InstanceIdentifier.builder(NetworkTopology.class)
        		        .child(Topology.class, new TopologyKey(new TopologyId("flow:1"))).build()
        		        ,new TopologyDataChangeListener(),DataChangeScope.SUBTREE);

       // this listener will list to any change on the following url
   	   // http://<odl-ip>:8181/restconf/config/bandwidthscheduler:scheduled-bandwidth
       dataChangeScheduledBandwidthListenerRegistration =
        		this.dataBroker.registerDataChangeListener(LogicalDatastoreType.CONFIGURATION,InstanceIdentifier.builder(ScheduledBandwidth.class).build()
        				,new BandwidthSchedulerDataChangeListener(),DataChangeScope.SUBTREE);

    }
    
    public void setRpcProviderRegistry(RpcProviderRegistry rpcRegistryDependency){
    	this.rpcRegistryDependency = rpcRegistryDependency;
    	
    	rpcRegistration = this.rpcRegistryDependency.addRpcImplementation(BandwidthschedulerService.class,this);
    	
    }
    
	/* (non-Javadoc)
	 * @see org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.BandwidthschedulerService#getPath(org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.GetPathInput)
	 */
	@Override
	public Future<RpcResult<GetPathOutput>> getPath(GetPathInput input) {
		List<org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.get.path.output.Link> links = new ArrayList<org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.get.path.output.Link>();
		
		String source = null;
		if (input.getSource() != null && input.getSource().getIpv4Address() != null){
			source = input.getSource().getIpv4Address().getValue();
		}else if (input.getSource() != null && input.getSource().getIpv6Address() != null){
			source = input.getSource().getIpv6Address().getValue();
		}
	
		String destination = null;
		if (input.getDestination() != null && input.getDestination().getIpv4Address() != null){
			destination = input.getDestination().getIpv4Address().getValue();
		}else if (input.getDestination() != null && input.getDestination().getIpv6Address() != null){
			destination = input.getDestination().getIpv6Address().getValue();
		}
		
		List<Link> shortLinks = topologyHelper.getPath(source, destination);
		if (shortLinks != null && !shortLinks.isEmpty()){
			for(Link link: shortLinks){
				LOG.info("getpath: adding link result '" + link.getLinkId() + "'");
				links.add(new org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.get.path.output.LinkBuilder()
				.setLinkId(link.getLinkId())
				.setDestination(link.getDestination())
				.setSource(link.getSource())
				.setSupportingLink(link.getSupportingLink())
				.build());
			}
		}else{
			LOG.info("path not found between source '" + source  + "' or destination '" + destination + "'");
		}
		
		return Futures.immediateFuture(RpcResultBuilder.<GetPathOutput>success(new GetPathOutputBuilder().setLink(links)).build());
	}
	
    private void addLimit(Limit limit){
		
		String source = (limit.getSource().getIpv4Address()!=null)?limit.getSource().getIpv4Address().getValue():limit.getSource().getIpv6Address().getValue();
		String destination = (limit.getDestination().getIpv4Address()!=null)?limit.getDestination().getIpv4Address().getValue():limit.getDestination().getIpv6Address().getValue();
		
		List<Link> shortLinks =  topologyHelper.getPath(source, destination);
		if (shortLinks != null && !shortLinks.isEmpty()){
			for(Link link: shortLinks){
				
				String sourceNode = link.getSource().getSourceNode().getValue();
				String sourceTP = link.getSource().getSourceTp().getValue();
				
				// when source in an openflow device we need to program a flow
				// on the switch. This cover the case which
				// source = openflow destination = openflow
				// source = openflow destination = host
				if (sourceNode.startsWith("openflow")){ 
					String port = sourceTP.substring(sourceTP.lastIndexOf(":")+1,sourceTP.length());
					openflowHelper.createFlow(dataBroker, sourceNode, source, destination, port, (long)limit.getBandwidth().getIntValue(), limit.getDurationSec());
				}
			}
		}else {
			LOG.error("path not found between '" + source + "' and '" + destination + "'" );
		}
	}
	
	private void removeLimit(Limit limit){
		// at the moment, we are not saving the path in
		// bandwidth scheduler model so we just try
		// to remove the limit from all nodes
		// we know in advance that some nodes it not have
		// that limit
		
		String source = (limit.getSource().getIpv4Address()!=null)?limit.getSource().getIpv4Address().getValue():limit.getSource().getIpv6Address().getValue();
		String destination = (limit.getDestination().getIpv4Address()!=null)?limit.getDestination().getIpv4Address().getValue():limit.getDestination().getIpv6Address().getValue();
		for (NodeId node: topologyHelper.getNodes().keySet()){
			LOG.info("removing node in '" + node.getValue());
			if (node.getValue().startsWith("openflow")){
				LOG.info("calling removing flow in node in '" + node.getValue() + " limit " + limit);
				openflowHelper.deleteFlow(dataBroker, node.getValue(), source, destination, limit.getBandwidth().getIntValue(), limit.getDurationSec());
			}
		}
	}
	
	private class TopologyDataChangeListener implements DataChangeListener{
		
		@Override
		public void onDataChanged(AsyncDataChangeEvent<InstanceIdentifier<?>, DataObject> change) {
				LOG.info("topology model has been changed");
				
				// we use a background thread to perform this task
				// to avoid using mdsal thread
				// This lines could be done more efficienly with a scheduled 
				// background thread
				new Thread(){
					public void run(){
						topologyHelper.update(dataBroker, "flow:1");		
					}
				}.start();
		}
	}
	
	
	private class BandwidthSchedulerDataChangeListener implements DataChangeListener{
		@Override
		public void onDataChanged(AsyncDataChangeEvent<InstanceIdentifier<?>, DataObject> change) {
				LOG.info("bandwidth scheduler model has been changed");
				
				final AsyncDataChangeEvent<InstanceIdentifier<?>, DataObject> dataChange = change;
				
				// we use a background thread to perform this task
				// to avoid using mdsal thread
				// This lines could be done more efficiently with a scheduled 
				// background thread
				new Thread(){
					public void run(){
						if (dataChange.getCreatedData() != null){
							for (Map.Entry<InstanceIdentifier<?>, DataObject> entry: dataChange.getCreatedData().entrySet()){
								if (entry.getKey().getTargetType().isAssignableFrom(Limit.class)){
									LOG.info("add entry is limit: " + entry.getKey());
									Limit limit = (Limit) entry.getValue();
									addLimit(limit);
								}
							}
						}
						
						if (dataChange.getUpdatedData() != null){
							for (Map.Entry<InstanceIdentifier<?>, DataObject> entry: dataChange.getUpdatedData().entrySet()){
								if (entry.getKey().getTargetType().isAssignableFrom(Limit.class)){
									LOG.info("update entry is limit: " + entry.getKey());
									Limit limit = (Limit) dataChange.getOriginalData().get(entry.getKey());
									removeLimit(limit);
									limit = (Limit) entry.getValue();
									addLimit(limit);
								}
							}
						}
						
						if (dataChange.getRemovedPaths() != null){
							for (InstanceIdentifier<?> path: dataChange.getRemovedPaths()){
								LOG.info("removed path: " + path);
								if (path.getTargetType().isAssignableFrom(Limit.class)){
									LOG.info("removed entry is limit: " + path);
									Limit limit = (Limit) dataChange.getOriginalData().get(path);
									removeLimit(limit);
								}
		
							}
						}
					}
				}.start();
		}
	}
}

