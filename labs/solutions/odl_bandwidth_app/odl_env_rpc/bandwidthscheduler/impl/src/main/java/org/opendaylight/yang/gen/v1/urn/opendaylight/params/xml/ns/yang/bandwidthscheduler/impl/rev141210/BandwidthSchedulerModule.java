/*
 * Brocade Communications Systems, Inc and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.impl.rev141210;

import org.opendaylight.controller.md.sal.binding.api.DataBroker;
import org.opendaylight.controller.sal.binding.api.RpcProviderRegistry;

import com.brocade.odl.workshop.impl.BandwidthSchedulerProvider;

public class BandwidthSchedulerModule extends org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.impl.rev141210.AbstractBandwidthSchedulerModule {
    public BandwidthSchedulerModule(org.opendaylight.controller.config.api.ModuleIdentifier identifier, org.opendaylight.controller.config.api.DependencyResolver dependencyResolver) {
        super(identifier, dependencyResolver);
    }

    public BandwidthSchedulerModule(org.opendaylight.controller.config.api.ModuleIdentifier identifier, org.opendaylight.controller.config.api.DependencyResolver dependencyResolver, org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.impl.rev141210.BandwidthSchedulerModule oldModule, java.lang.AutoCloseable oldInstance) {
        super(identifier, dependencyResolver, oldModule, oldInstance);
    }

    @Override
    public void customValidation() {
        // add custom validation form module attributes here.
    }

    @Override
    public java.lang.AutoCloseable createInstance() {
        final BandwidthSchedulerProvider provider = new BandwidthSchedulerProvider();
        getBrokerDependency().registerProvider(provider);
        
        DataBroker dataBroker = getDataBrokerDependency();
        provider.setDataBroker(dataBroker);
        
        RpcProviderRegistry rpcRegistryDependency = getRpcRegistryDependency();
        provider.setRpcProviderRegistry(rpcRegistryDependency);

        return provider;
    }

}
