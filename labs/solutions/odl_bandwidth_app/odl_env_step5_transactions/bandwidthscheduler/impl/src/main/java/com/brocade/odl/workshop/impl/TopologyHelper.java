package com.brocade.odl.workshop.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.opendaylight.controller.md.sal.binding.api.DataBroker;
import org.opendaylight.controller.md.sal.binding.api.ReadOnlyTransaction;
import org.opendaylight.controller.md.sal.common.api.data.LogicalDatastoreType;
import org.opendaylight.controller.sal.binding.api.NotificationProviderService;
import org.opendaylight.yang.gen.v1.urn.opendaylight.address.tracker.rev140617.address.node.connector.Addresses;
import org.opendaylight.yang.gen.v1.urn.opendaylight.host.tracker.rev140624.HostNode;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.NetworkTopology;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.NodeId;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.TopologyId;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.Topology;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.TopologyKey;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.topology.Link;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.topology.Node;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;

import edu.uci.ics.jung.algorithms.shortestpath.DijkstraShortestPath;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.SparseMultigraph;
import edu.uci.ics.jung.graph.util.EdgeType;

/**
 * Topology Helper class keeps an updated view of the Openflow network
 */
public class TopologyHelper {

	private static final Logger LOG = LoggerFactory.getLogger(TopologyHelper.class);
	private Graph<NodeId, Link> networkGraph =  null;
	private DijkstraShortestPath<NodeId, Link> dijkstraShortestPath =  null;
	private Map<String,NodeId> ipNodeIdsMap = new HashMap<String,NodeId>();
	private Map<NodeId,Node> nodeIdsMap = new HashMap<NodeId,Node>();

	private MeterHelper meterHelper = new MeterHelper();

	/**
	 * Update topology information.
	 *
	 * @param dataBroker the data broker
	 * @param topologyId the topology id
	 */
	public synchronized void update(DataBroker dataBroker, String topologyId) {
		update(dataBroker,null,topologyId);
	}

	/**
	 * Update topology information.
	 *
	 * @param dataBroker the data broker
	 * @param notificationProvider the notification service
	 * @param topologyId the topology id
	 */
	public synchronized void update(DataBroker dataBroker, NotificationProviderService notificationProvider, String topologyId) {

		LOG.info("network graph model has changed, checking new values");

		try {

			Topology topology = null;

			// Following lines have to create a read transaction and read
			// from topology network once Topology object is obtained,
			// the rest of the code in this class will take care of updating
			// the graph read the topology detail for given topology id.
			ReadOnlyTransaction readOnlyTransaction = dataBroker.newReadOnlyTransaction();
			try {

				//create topology identifier
				InstanceIdentifier<Topology> topologyInstanceIdentifier = InstanceIdentifier.builder(NetworkTopology.class)
		                .child(Topology.class, new TopologyKey(new TopologyId(topologyId)))
		                .build();

				Optional<Topology> topologyOptional = readOnlyTransaction.read(
						LogicalDatastoreType.OPERATIONAL,topologyInstanceIdentifier).get();
				if (topologyOptional.isPresent()) {
					topology = topologyOptional.get();
				}
			} catch (Exception e) {
				LOG.error("Error reading topology '" + topologyId +"'",e);
				readOnlyTransaction.close();
				throw new RuntimeException("Error reading from operational store, topology : " + topologyId, e);
			}



			List<Link> topologyLinks = (topology != null)?topology.getLink():null;

			Graph<NodeId, Link> networkGraphTmp = new SparseMultigraph<>();

			if (topologyLinks != null){
				for (Link link : topologyLinks) {
					NodeId sourceNodeId = link.getSource().getSourceNode();
					NodeId destinationNodeId = link.getDestination().getDestNode();
					networkGraphTmp.addVertex(sourceNodeId);
					networkGraphTmp.addVertex(destinationNodeId);
					networkGraphTmp.addEdge(link, sourceNodeId, destinationNodeId, EdgeType.DIRECTED);
					//LOG.info("added link " + link);
				}
			}

			Map<String,NodeId> ipNodeIdsMapTmp = new HashMap<String, NodeId>();
			Map<NodeId,Node> nodeIdsMapTmp = new HashMap<NodeId, Node>();

			if (topology != null && topology.getNode() !=null && !topology.getNode().isEmpty()){

				//add each node to the Map
				for (Node node : topology.getNode()){
					nodeIdsMapTmp.put(node.getNodeId(), node);

					//if node is a host, also keep tracking of ip addresses
					String nodeName = node.getNodeId().getValue();
					if (nodeName != null && nodeName.startsWith("host")){

						NodeId value = node.getNodeId();

						//read host augmentation to be able to read
						//host addresses information
						HostNode hostNode = node.getAugmentation(HostNode.class);
						if (hostNode != null && hostNode.getAddresses() != null){

							for (Addresses address: hostNode.getAddresses()){
								if (address.getIp() != null && address.getIp().getIpv4Address() != null){
									LOG.info("adding ip " + address.getIp().getIpv4Address().getValue());
									ipNodeIdsMapTmp.put(address.getIp().getIpv4Address().getValue(),value);
								}else if (address.getIp() != null && address.getIp().getIpv4Address() != null){
									LOG.info("adding ip " + address.getIp().getIpv6Address().getValue());
									ipNodeIdsMapTmp.put(address.getIp().getIpv6Address().getValue(),value);
								}
							}
						}
					}
				}
			}

			//find new Openflow devices and create the meters
			for (Map.Entry<NodeId, Node> entry: nodeIdsMapTmp.entrySet()){
				String nodeName = entry.getKey().getValue();
				if (nodeName!= null && !nodeIdsMap.containsKey(entry.getKey())){
					if ( nodeName.startsWith("openflow") ){
						meterHelper.createMeters(dataBroker, nodeName);
						LOG.info("added new openflow device '" + nodeName + "'");
					}
					//TODO send a notification when a new node is added
				}
			}

			//find removed Openflow devices and remove all their configuration
			for (Map.Entry<NodeId, Node> entry: nodeIdsMap.entrySet()){
				String nodeName = entry.getKey().getValue();
				if (nodeName!= null && !nodeIdsMapTmp.containsKey(entry.getKey())){
					if (nodeName.startsWith("openflow")){
						//TODO remove device from openflow model
						LOG.info("removed openflow device '" + nodeName + "'");
					}
					//TODO send a notification when a new node is removed
				}
			}

			ipNodeIdsMap = ipNodeIdsMapTmp;
			nodeIdsMap = nodeIdsMapTmp;
			networkGraph = networkGraphTmp;
			dijkstraShortestPath = new DijkstraShortestPath<NodeId, Link>(networkGraph);

		} catch (Exception e) {
			LOG.error("cannot update networ graph service " + e, e);
		}
	}

	/**
	 * Gets the path between two hosts based on Dijkstra shortest path algorithm
	 *
	 * @param source the source host
	 * @param destination the destination host
	 * @return the path
	 */
	public List<Link> getPath(String source, String destination ) {
		NodeId sourceNodeId = (source != null)?ipNodeIdsMap.get(source):null;
		NodeId destinationNodeId = (destination != null)?ipNodeIdsMap.get(destination):null;

		if (sourceNodeId != null && destinationNodeId != null){
			return dijkstraShortestPath.getPath(sourceNodeId, destinationNodeId);
		}

		LOG.info("source '" + source  + "' or destination '" + destination + "' has not been found");
		return null;
	}

	/**
	 * Gets the nodes.
	 *
	 * @return the nodes
	 */
	public Map<NodeId,Node> getNodes(){
		return nodeIdsMap;
	}

}
