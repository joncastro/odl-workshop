package com.brocade.odl.workshop.impl;

import java.util.ArrayList;
import java.util.List;

import org.opendaylight.controller.md.sal.binding.api.DataBroker;
import org.opendaylight.controller.md.sal.binding.api.ReadWriteTransaction;
import org.opendaylight.controller.md.sal.common.api.data.LogicalDatastoreType;
import org.opendaylight.controller.md.sal.common.api.data.TransactionCommitFailedException;
import org.opendaylight.yang.gen.v1.urn.ietf.params.xml.ns.yang.ietf.inet.types.rev100924.Ipv4Prefix;
import org.opendaylight.yang.gen.v1.urn.ietf.params.xml.ns.yang.ietf.inet.types.rev100924.Uri;
import org.opendaylight.yang.gen.v1.urn.opendaylight.action.types.rev131112.action.action.OutputActionCaseBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.action.types.rev131112.action.action.output.action._case.OutputActionBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.action.types.rev131112.action.list.Action;
import org.opendaylight.yang.gen.v1.urn.opendaylight.action.types.rev131112.action.list.ActionBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.FlowCapableNode;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.FlowId;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.tables.Table;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.tables.TableKey;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.tables.table.Flow;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.tables.table.FlowBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.tables.table.FlowKey;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.flow.InstructionsBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.flow.Match;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.flow.MatchBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.instruction.instruction.ApplyActionsCaseBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.instruction.instruction.MeterCaseBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.instruction.instruction.apply.actions._case.ApplyActionsBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.instruction.instruction.meter._case.MeterBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.instruction.list.Instruction;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.instruction.list.InstructionBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.types.rev131026.instruction.list.InstructionKey;
import org.opendaylight.yang.gen.v1.urn.opendaylight.inventory.rev130819.NodeId;
import org.opendaylight.yang.gen.v1.urn.opendaylight.inventory.rev130819.Nodes;
import org.opendaylight.yang.gen.v1.urn.opendaylight.inventory.rev130819.nodes.Node;
import org.opendaylight.yang.gen.v1.urn.opendaylight.inventory.rev130819.nodes.NodeKey;
import org.opendaylight.yang.gen.v1.urn.opendaylight.l2.types.rev130827.EtherType;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.MeterId;
import org.opendaylight.yang.gen.v1.urn.opendaylight.model.match.types.rev131026.ethernet.match.fields.EthernetTypeBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.model.match.types.rev131026.match.EthernetMatchBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.model.match.types.rev131026.match.layer._3.match.Ipv4MatchBuilder;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.google.common.util.concurrent.CheckedFuture;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;

/**
 * A helper to create flows and meters
 */
public class OpenflowHelper {

	private static final Logger LOG = LoggerFactory.getLogger(OpenflowHelper.class);

	private final short DEFAULT_TABLE_ID = (short)0;
	private final int DEFAULT_PRIORITY = 32768;
	private final int DEFAULT_ID_TIMEOUT = 0;

	/**
	 * Creates the flow for the given meter, source and destination.
	 *
	 * @param dataBroker the data broker
	 * @param node the node
	 * @param source the source
	 * @param destination the destination
	 * @param outputPort the output port
	 * @param meterId the meter id
	 * @param seconds duration in seconds
	 */
	public void createFlow(DataBroker dataBroker, final String node, String source, String destination, String outputPort, long meterId, int seconds){

		LOG.info(String.format("creating flow '%s' source '%s' destination '%s' outputPort '%s' meterId '%s' seconds '%s'",node,source,destination,outputPort,meterId,seconds) );

		if (!source.contains("/")){
			source = source + "/32";
		}

		if (!destination.contains("/")){
			destination = destination + "/32";
		}

		//final String flowId = source+"-"+destination+"-"+meterId + "-"+ seconds + ((seconds > 0)? "-" + System.currentTimeMillis():"");
		final String flowId = "bandwidth-" + source+"-"+destination+"-"+meterId + "-"+ seconds;


		LOG.info(String.format("flow id '%s' on  node '%s' ",flowId,node));

		//create all meter properties
        List<Instruction> instructions = new ArrayList<Instruction>();

        List<Action> actionList = new ArrayList<Action>();
        actionList.add(new ActionBuilder()
        		.setOrder(0)
        		.setAction(new OutputActionCaseBuilder()
        				.setOutputAction(new OutputActionBuilder()
        				.setOutputNodeConnector(new Uri(outputPort)).build())
        				.build())
        		.build());


        instructions.add(new InstructionBuilder()
        		.setOrder(0)
        		.setInstruction(new MeterCaseBuilder()
        			.setMeter(new MeterBuilder().setMeterId(new MeterId(meterId)).build()).build())
        			.setKey(new InstructionKey(0)).build());

        instructions.add(new InstructionBuilder()
        		.setOrder(1)
        		.setInstruction(new ApplyActionsCaseBuilder().setApplyActions(new ApplyActionsBuilder().setAction(actionList).build()).build())
        		.build());


        Match match = new MatchBuilder()
        	.setLayer3Match(new Ipv4MatchBuilder().setIpv4Source(new Ipv4Prefix(source)).setIpv4Destination(new Ipv4Prefix(destination)).build())
        	.setEthernetMatch(new EthernetMatchBuilder().setEthernetType(new EthernetTypeBuilder().setType(new EtherType(0x0800L)).build()).build())
        	.build();

        Flow flow = new FlowBuilder()
        	.setId(new FlowId(flowId))
	        .setTableId(DEFAULT_TABLE_ID)
	        .setHardTimeout(seconds)
	        .setIdleTimeout(DEFAULT_ID_TIMEOUT)
	        .setInstructions(new InstructionsBuilder().setInstruction(instructions).build())
	        .setPriority(DEFAULT_PRIORITY)
	        .setMatch(match)
	        .build();

        // write the meter configuration in the configuration data store
	    ReadWriteTransaction modification = dataBroker.newReadWriteTransaction();

	    InstanceIdentifier<Flow> identifier = InstanceIdentifier.create(Nodes.class).child(Node.class, new NodeKey(new NodeId(node)))
	                	.augmentation(FlowCapableNode.class)
	                	.child(Table.class, new TableKey(new Short(DEFAULT_TABLE_ID)))
	                	.child(Flow.class, new FlowKey(new FlowId(flowId)));

	    modification.merge(LogicalDatastoreType.CONFIGURATION, identifier, flow, true);

	    CheckedFuture<Void, TransactionCommitFailedException> commitFuture = modification.submit();

        Futures.addCallback(commitFuture, new FutureCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            	LOG.info("flow '" + flowId + "' has been sucessfully installed on '" + node + "'");
            }

            @Override
            public void onFailure(Throwable throwable) {
            	LOG.info("error adding flow '" + flowId + "' to '" + node + "'",throwable);
            }
        });
	}

	/**
	 * Creates the flow for the given meter, source and destination.
	 *
	 * @param dataBroker the data broker
	 * @param node the node
	 * @param source the source
	 * @param destination the destination
	 * @param outputPort the output port
	 * @param meterId the meter id
	 * @param seconds duration in seconds
	 */
	public void deleteFlow(DataBroker dataBroker, final String node, String source, String destination, long meterId, int seconds){

		LOG.info(String.format("deleting flow '%s' source '%s' destination '%s' meterId '%s' seconds '%s'",node,source,destination,meterId,seconds) );

		if (!source.contains("/")){
			source = source + "/32";
		}

		if (!destination.contains("/")){
			destination = destination + "/32";
		}

		final String flowId = "bandwidth-" + source+"-"+destination+"-"+meterId + "-"+ seconds;


        // write the meter configuration in the configuration data store
	    ReadWriteTransaction modification = dataBroker.newReadWriteTransaction();

	    InstanceIdentifier<Flow> identifier = InstanceIdentifier.create(Nodes.class).child(Node.class, new NodeKey(new NodeId(node)))
	                	.augmentation(FlowCapableNode.class)
	                	.child(Table.class, new TableKey(new Short(DEFAULT_TABLE_ID)))
	                	.child(Flow.class, new FlowKey(new FlowId(flowId)));

	    Optional<Flow> flow;
		try {
			 flow = modification.read(LogicalDatastoreType.CONFIGURATION, identifier).get();
			 if (flow.isPresent()){
			    	modification.delete(LogicalDatastoreType.CONFIGURATION, identifier);
			 }
		} catch (Exception e) {
			LOG.error("cannot read or delete flow",e);
		}


	    CheckedFuture<Void, TransactionCommitFailedException> commitFuture = modification.submit();

        Futures.addCallback(commitFuture, new FutureCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            	LOG.info("flow '" + flowId + "' deleted been sucessfully installed on '" + node + "'");
            }

            @Override
            public void onFailure(Throwable throwable) {
            	LOG.info("error deleting flow '" + flowId + "' to '" + node + "'",throwable);
            }
        });
	}

	/**
	 * Delete openflow node configuration from data store
	 *
	 * @param dataBroker the data broker
	 * @param node the node
	 */
	public void deleteNode(DataBroker dataBroker, final String node){

		ReadWriteTransaction modification = dataBroker.newReadWriteTransaction();
		InstanceIdentifier<Node> identifier = InstanceIdentifier.create(Nodes.class).child(Node.class, new NodeKey(new NodeId(node)));
		modification.delete(LogicalDatastoreType.CONFIGURATION, identifier);
	    CheckedFuture<Void, TransactionCommitFailedException> commitFuture = modification.submit();

	    Futures.addCallback(commitFuture, new FutureCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            	LOG.info("node '" + node + "' configuration has been removed");
            }

            @Override
            public void onFailure(Throwable throwable) {
            	LOG.info("error removing node '" + node + "'",throwable);
            }
        });

	}
}
