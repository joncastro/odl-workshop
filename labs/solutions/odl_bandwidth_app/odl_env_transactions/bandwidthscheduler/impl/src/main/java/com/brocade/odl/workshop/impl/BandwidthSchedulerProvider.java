/*
 * Brocade Communications Systems, Inc and others.  All rights reserved.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 */
package com.brocade.odl.workshop.impl;

import org.opendaylight.controller.sal.binding.api.BindingAwareBroker.ProviderContext;
import org.opendaylight.controller.sal.binding.api.BindingAwareProvider;
import org.opendaylight.controller.md.sal.binding.api.DataBroker;
import org.opendaylight.controller.md.sal.binding.api.DataChangeListener;
import org.opendaylight.controller.md.sal.common.api.data.AsyncDataChangeEvent;
import org.opendaylight.controller.md.sal.common.api.data.LogicalDatastoreType;
import org.opendaylight.controller.md.sal.common.api.data.AsyncDataBroker.DataChangeScope;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.ScheduledBandwidth;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.NetworkTopology;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.TopologyId;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.Topology;
import org.opendaylight.yang.gen.v1.urn.tbd.params.xml.ns.yang.network.topology.rev131021.network.topology.TopologyKey;
import org.opendaylight.yangtools.concepts.ListenerRegistration;
import org.opendaylight.yangtools.yang.binding.DataObject;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BandwidthSchedulerProvider implements BindingAwareProvider, AutoCloseable {

    private static final Logger LOG = LoggerFactory.getLogger(BandwidthSchedulerProvider.class);

    private DataBroker dataBroker = null;
    private ListenerRegistration<DataChangeListener> dataChangeTopologyListenerRegistration;
    private ListenerRegistration<DataChangeListener> dataChangeScheduledBandwidthListenerRegistration;
    private TopologyHelper topologyHelper = new TopologyHelper();

    @Override
    public void onSessionInitiated(ProviderContext session) {
        LOG.info("BandwidthSchedulerProvider Session Initiated");
    }

    @Override
    public void close() throws Exception {
        LOG.info("BandwidthSchedulerProvider Closed");
        if (dataChangeTopologyListenerRegistration!=null)dataChangeTopologyListenerRegistration.close();
        if (dataChangeScheduledBandwidthListenerRegistration!=null)dataChangeScheduledBandwidthListenerRegistration.close();
    }

    public void setDataBroker(DataBroker dataBroker){
    	this.dataBroker = dataBroker;

    	// this listener will list to any change on the following url
    	// http://<odl-ip>:8181/restconf/operational/network-topology:network-topology/topology/flow:1
       dataChangeTopologyListenerRegistration =
        		this.dataBroker.registerDataChangeListener(LogicalDatastoreType.OPERATIONAL,InstanceIdentifier.builder(NetworkTopology.class)
        		        .child(Topology.class, new TopologyKey(new TopologyId("flow:1"))).build()
        		        ,new TopologyDataChangeListener(),DataChangeScope.SUBTREE);

       // this listener will list to any change on the following url
   	   // http://<odl-ip>:8181/restconf/config/bandwidthscheduler:scheduled-bandwidth
       dataChangeScheduledBandwidthListenerRegistration =
        		this.dataBroker.registerDataChangeListener(LogicalDatastoreType.CONFIGURATION,InstanceIdentifier.builder(ScheduledBandwidth.class).build()
        				,new BandwidthSchedulerDataChangeListener(),DataChangeScope.SUBTREE);

    }

	private class TopologyDataChangeListener implements DataChangeListener{

		@Override
		public void onDataChanged(AsyncDataChangeEvent<InstanceIdentifier<?>, DataObject> change) {
				LOG.info("topology model has been changed");

				// we use a background thread to perform this task
				// to avoid using mdsal thread
				// This lines could be done more efficiently with a scheduled 
				// background thread
				new Thread(){
					public void run(){
						topologyHelper.update(dataBroker, "flow:1");
					}
				}.start();
		}
	}

	private class BandwidthSchedulerDataChangeListener implements DataChangeListener{
		@Override
		public void onDataChanged(AsyncDataChangeEvent<InstanceIdentifier<?>, DataObject> change) {
				LOG.info("bandwidth scheduler model has been changed");
		}
	}
}
