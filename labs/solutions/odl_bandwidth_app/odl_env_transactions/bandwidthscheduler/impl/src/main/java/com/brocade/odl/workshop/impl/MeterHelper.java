package com.brocade.odl.workshop.impl;

import java.util.ArrayList;
import java.util.List;

import org.opendaylight.controller.md.sal.binding.api.DataBroker;
import org.opendaylight.controller.md.sal.binding.api.ReadWriteTransaction;
import org.opendaylight.controller.md.sal.common.api.data.LogicalDatastoreType;
import org.opendaylight.controller.md.sal.common.api.data.TransactionCommitFailedException;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.FlowCapableNode;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.meters.Meter;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.meters.MeterBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.flow.inventory.rev130819.meters.MeterKey;
import org.opendaylight.yang.gen.v1.urn.opendaylight.inventory.rev130819.NodeId;
import org.opendaylight.yang.gen.v1.urn.opendaylight.inventory.rev130819.Nodes;
import org.opendaylight.yang.gen.v1.urn.opendaylight.inventory.rev130819.nodes.Node;
import org.opendaylight.yang.gen.v1.urn.opendaylight.inventory.rev130819.nodes.NodeKey;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.BandId;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.MeterBandType;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.MeterFlags;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.MeterId;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.band.type.band.type.DropBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.meter.MeterBandHeadersBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.meter.meter.band.headers.MeterBandHeader;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.meter.meter.band.headers.MeterBandHeaderBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.meter.types.rev130918.meter.meter.band.headers.meter.band.header.MeterBandTypesBuilder;
import org.opendaylight.yang.gen.v1.urn.opendaylight.params.xml.ns.yang.bandwidthscheduler.rev150105.BandwidthProfile;
import org.opendaylight.yangtools.yang.binding.InstanceIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.CheckedFuture;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;

/**
 * Creates meters on Openflow devices
 */
public class MeterHelper {

	private static final Logger LOG = LoggerFactory.getLogger(MeterHelper.class);
	
	/**
	 * Creates the meters defined in bandwidh
	 *
	 * @param dataBroker the data broker
	 * @param node the node
	 */
	public void createMeters(DataBroker dataBroker, final String node){
		for (BandwidthProfile profile : BandwidthProfile.values()){
			createMeter(dataBroker, node,profile.getIntValue() , profile.getIntValue());
		}
	}
	
	/**
	 * Creates the meter.
	 *
	 * @param dataBroker the data broker
	 * @param node the node
	 * @param meterId the meter id
	 * @param rate the rate
	 */
	public void createMeter(DataBroker dataBroker, final String node, final long meterId, long rate){
		
		MeterId id = new MeterId(meterId);
	    MeterKey key = new MeterKey(id);
	    MeterBuilder meter = new MeterBuilder();
	    meter.setKey(key);
	    meter.setMeterId(id);
	    meter.setFlags(new MeterFlags(false, true, false, false));
	    
	    MeterBandHeadersBuilder bandHeaders = new MeterBandHeadersBuilder();
	    
	    List<MeterBandHeader> bandHdr = new ArrayList<MeterBandHeader>();
	    
	    MeterBandHeaderBuilder bandHeader = new MeterBandHeaderBuilder();
	    bandHeader.setBandRate(rate);
	    bandHeader.setBandBurstSize(0L);	        
	    bandHeader.setBandId(new BandId(0L));	
	    bandHeader.setBandType(new DropBuilder().setDropBurstSize(0L).setDropRate(rate).build());
	    bandHeader.setMeterBandTypes(new MeterBandTypesBuilder().setFlags(new MeterBandType(true, false, false)).build());
	    bandHdr.add(bandHeader.build());
	    
	    bandHeaders.setMeterBandHeader(bandHdr);
	    
	    meter.setMeterBandHeaders(bandHeaders.build());

	    
	    // create a write transaction which will
	    // write the meter in the given openflow device
	    ReadWriteTransaction modification = dataBroker.newReadWriteTransaction();
	    InstanceIdentifier<Meter> meterIdentifier = InstanceIdentifier.create(Nodes.class).child(Node.class, new NodeKey(new NodeId(node)))
                .augmentation(FlowCapableNode.class).child(Meter.class, new MeterKey(new MeterId(meterId)));
	    modification.put(LogicalDatastoreType.CONFIGURATION, meterIdentifier, meter.build(), true);
	    CheckedFuture<Void, TransactionCommitFailedException> commitFuture = modification.submit();
        Futures.addCallback(commitFuture, new FutureCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            	LOG.info("meter '" + meterId + "' has been successfully installed on '" + node + "'");
            }

            @Override
            public void onFailure(Throwable throwable) {
            	LOG.info("error adding meter '" + meterId + "' to '" + node + "'",throwable);
            }
        });
	}
}
