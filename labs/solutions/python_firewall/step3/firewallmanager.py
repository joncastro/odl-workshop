#!/usr/bin/env python

import json
import argparse
import requests

from pybvc.controller.controller import Controller
from pybvc.common.utils import load_dict_from_file
from pybvc.netconfdev.vrouter.vrouter5600 import VRouter5600
from pybvc.common.status import OperStatus, STATUS
from requests.exceptions import ConnectionError, Timeout
from pybvc.common.result import Result


#-------------------------------------------------------------------------------
# Class 'Firewallmanager'
#-------------------------------------------------------------------------------
class Firewallmanager(object):
    """ Firewall manager commands executer """

    def __init__(self):
        self.prog = 'firewallmanager'
        parser = argparse.ArgumentParser(
            prog=self.prog,
            description='Firewall manager command line tool for interaction with OpenFlow Controller',
            usage="%(prog)s [-h] [-C <path>] <command> [<args>]\n"
                  "(type '%(prog)s -h' for details)\n"
                     "\nAvailable commands are:\n"
                     "\n   get-fw-cfg            Get firewall rules"
                     "\n   add-group             Add firewall group"
                     "\n   overwrite-group        Overwrite firewall group"
                     "\n   delete-group          Delete firewall group"
                     "\n   add-rule              Add firewall rule"
                     "\n   overwrite-rule        Overwrite firewall rule"
                     "\n   delete-rule           Delete firewall rule"
                     "\n"
                     "\n  '%(prog)s help <command>' provides details for a specific command")

        parser.add_argument('-C', metavar="<path>", dest='ctrl_cfg_file',
                            help="path to the controller's configuration file (default is './ctrl.yml')",
                            default="./ctrl.yml")

        parser.add_argument('command', help='command to be executed')

        # following method will save on args the first argument
        # and re
        args, remaining_args = parser.parse_known_args()

        # Get Controller's attributes from configuration file
        self.ctrl  = self.get_controller(args.ctrl_cfg_file)
        if(self.ctrl == None):
            print "\n".strip()
            print ("Cannot find controller configuration file")
            print "\n".strip()
            exit(1)

        # Invoke method that is matching the name of sub-command argument
        # cmd variable contains the first argument
        cmd = args.command.replace('-', '_')

        if hasattr(self, cmd):
            getattr(self, cmd)(remaining_args)
        else:
            print "\n".strip()
            print ("Error, unrecognized command '%s'" % cmd)
            print "\n".strip()
            parser.print_help()
            exit(1)

    #---------------------------------------------------------------------------
    # get controller object given yaml configuration file
    #---------------------------------------------------------------------------
    def get_controller(self, file):
        ctrlProperties = {}
        if(load_dict_from_file(file, ctrlProperties) == False):
            print("Config file '%s' read error: " % f)
            return None
        try:
            ctrlIp = ctrlProperties['ctrlIp']
            ctrlPort = ctrlProperties['ctrlPort']
            ctrlUser = ctrlProperties['ctrlUser']
            ctrlPassword = ctrlProperties['ctrlPassword']
        except:
            print ("Some attribute is missing in ctrl.yml file")
            return None

        # create Controller object which represent an API provided
        # by pybvc to execute the most commons actions on the
        # SDN Controller
        return Controller(ctrlIp, ctrlPort, ctrlUser, ctrlPassword, None)

    #---------------------------------------------------------------------------
    #
    #---------------------------------------------------------------------------
    def http_put_request(self, url, nodeName , data):
        """Sends HTTP PUT request to a remote server and returns the response.

        :param string url: The partial url to the schema and element

        :param string data: The data to include in the body of the request.
                            Typically set to None.
        :return: Request status of the node.
        :rtype: None or :class:`pybvc.common.status.OperStatus`

        """

        content = None
        status = OperStatus()

        try:
            fullurl = "http://{}:{}/restconf/config/" + \
                          "opendaylight-inventory:nodes/node/{}/" + \
                          "yang-ext:mount/{}/"
            fullurl =  fullurl.format(self.ctrl.ipAddr,self.ctrl.portNum,nodeName,url)

            headers = {'content-type': 'application/json', 'accept': 'application/json'}

            resp = self.ctrl.http_put_request(fullurl,data,headers)
            if(resp == None):
                status.set_status(STATUS.CONN_ERROR)
            elif(resp.content == None):
                status.set_status(STATUS.CTRL_INTERNAL_ERROR)
            elif(resp.status_code == 200 or resp.status_code == 204):
                status.set_status(STATUS.OK)
                content = resp.content
            else:
                status.set_status(STATUS.HTTP_ERROR, resp)

            return Result(status, content)

        except (ConnectionError, Timeout) as e:
            print "Error: " + repr(e)
            status.set_status(STATUS.CONN_ERROR)

        return Result(status, content)

    #---------------------------------------------------------------------------
    #
    #---------------------------------------------------------------------------
    def http_post_request(self, url, nodeName , data):
        """Sends HTTP POST request to a remote server and returns the response.

        :param string url: The partial url to the schema and element

        :param string data: The data to include in the body of the request.
                            Typically set to None.
        :return: Request status of the node.
        :rtype: None or :class:`pybvc.common.status.OperStatus`

        """

        content = None
        status = OperStatus()

        try:
            fullurl = "http://{}:{}/restconf/config/" + \
                          "opendaylight-inventory:nodes/node/{}/" + \
                          "yang-ext:mount/{}/"

            fullurl =  fullurl.format(self.ctrl.ipAddr,self.ctrl.portNum,nodeName,url)

            headers = {'content-type': 'application/json', 'accept': 'application/json'}

            resp = self.ctrl.http_post_request(fullurl,data,headers)
            if(resp == None):
                status.set_status(STATUS.CONN_ERROR)
            elif(resp.content == None):
                status.set_status(STATUS.CTRL_INTERNAL_ERROR)
            elif(resp.status_code == 200 or resp.status_code == 204):
                status.set_status(STATUS.OK)
                content = resp.content
            else:
                status.set_status(STATUS.HTTP_ERROR, resp)

            return Result(status, content)

        except (ConnectionError, Timeout) as e:
            print "Error: " + repr(e)
            status.set_status(STATUS.CONN_ERROR)

        return Result(status, content)

    #---------------------------------------------------------------------------
    #
    #---------------------------------------------------------------------------
    def http_delete_request(self, url, nodeName , data=None):
        """Sends HTTP POST request to a remote server and returns the response.

        :param string url: The partial url to the schema and element

        :param string data: The data to include in the body of the request.
                            Typically set to None.
        :return: Request status of the node.
        :rtype: None or :class:`pybvc.common.status.OperStatus`

        """

        content = None
        status = OperStatus()

        try:
            fullurl = "http://{}:{}/restconf/config/" + \
                          "opendaylight-inventory:nodes/node/{}/" + \
                          "yang-ext:mount/{}/"

            fullurl =  fullurl.format(self.ctrl.ipAddr,self.ctrl.portNum,nodeName,url)

            headers = {'content-type': 'application/json', 'accept': 'application/json'}

            resp = self.ctrl.http_delete_request(fullurl,data,headers)
            if(resp == None):
                status.set_status(STATUS.CONN_ERROR)
            elif(resp.content == None):
                status.set_status(STATUS.CTRL_INTERNAL_ERROR)
            elif(resp.status_code == 200 or resp.status_code == 204):
                status.set_status(STATUS.OK)
                content = resp.content
            else:
                status.set_status(STATUS.HTTP_ERROR, resp)

            return Result(status, content)

        except (ConnectionError, Timeout) as e:
            print "Error: " + repr(e)
            status.set_status(STATUS.CONN_ERROR)

        return Result(status, content)

    #---------------------------------------------------------------------------
    # get firewall rules
    #---------------------------------------------------------------------------
    def get_fw_cfg(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s get-fw-cfg -n=NAME|--name=NAME\n\n"
                  "Get firewall configuration from vRouter device\n\n"
                  "Options:\n"
                  "  -n, --name          device name\n")
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


        # create NETCONF Node
        node = VRouter5600(self.ctrl, args.name, None, None, None, None, None)

        # remove node from the controller
        result = node.get_firewalls_cfg()

        # check results
        status = result.get_status()
        if(status.eq(STATUS.OK)):
            print ("'%s' firewall configuration" % args.name)
            print json.dumps(json.loads(result.get_data()), indent=4)
        else:
            print ("\n")
            print status.detailed()


    #---------------------------------------------------------------------------
    # add group
    #---------------------------------------------------------------------------
    def add_group(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s add-group -n=NAME|--name=NAME -j=JSON|--json=JSON\n\n"
                  "Add firewall group \n\n"
                  "Options:\n"
                  "  -n, --name          device name\n"
                  "  -j, --json          json file\n"
                  )
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-j', '--json', metavar = "<JSON>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


    #---------------------------------------------------------------------------
    # add group
    #---------------------------------------------------------------------------
    def overwrite_group(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s overwrite-group -n=NAME|--name=NAME -g=GROUP|--group=GROUP -j=JSON|--json=JSON\n\n"
                  "Overwrite firewall group \n\n"
                  "Options:\n"
                  "  -n, --name          device name\n"
                  "  -g, --group         group name\n"
                  "  -j, --json          json file\n"
                  )
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-g', '--group', metavar = "<GROUP>", required=True)
        parser.add_argument('-j', '--json', metavar = "<JSON>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


    #---------------------------------------------------------------------------
    # delete group
    #---------------------------------------------------------------------------
    def delete_group(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s delete-group -n=NAME|--name=NAME -g=GROUP|--group=GROUP\n\n"
                  "Delete firewall group \n\n"
                  "Options:\n"
                  "  -n, --name          device name\n"
                  "  -g, --group         group name\n"
                  )
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-g', '--group', metavar = "<GROUP>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


    #---------------------------------------------------------------------------
    # add rule
    #---------------------------------------------------------------------------
    def add_rule(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s add-rule -n=NAME|--name=NAME -j=JSON|--json=JSON\n\n"
                  "Add firewall rule \n\n"
                  "Options:\n"
                  "  -n, --name          device name\n"
                  "  -j, --json          json file\n"
                  )
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-j', '--json', metavar = "<JSON>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


    #---------------------------------------------------------------------------
    # add group
    #---------------------------------------------------------------------------
    def overwrite_rule(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s overwrite-rule -n=NAME|--name=NAME -g=GROUP|--group=GROUP -r=RULE|--rule=RULE -j=JSON|--json=JSON\n\n"
                  "Overwrite firewall rule \n\n"
                  "Options:\n"
                  "  -n, --name          device name\n"
                  "  -g, --group         group name\n"
                  "  -r, --rule          rule name\n"
                  "  -j, --json          json file\n"
                  )
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-g', '--group', metavar = "<GROUP>", required=True)
        parser.add_argument('-r', '--rule', metavar = "<RULE>", required=True)
        parser.add_argument('-j', '--json', metavar = "<JSON>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


    #---------------------------------------------------------------------------
    # delete group
    #---------------------------------------------------------------------------
    def delete_rule(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s delete-rule -n=NAME|--name=NAME -g=GROUP|--group=GROUP -r=RULE|--rule=RULE\n\n"
                  "Delete firewall rule\n\n"
                  "Options:\n"
                  "  -n, --name          device name\n"
                  "  -g, --group         group name\n"
                  "  -r, --rule          rule name\n"
                  )
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-g', '--group', metavar = "<GROUP>", required=True)
        parser.add_argument('-r', '--rule', metavar = "<RULE>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)


#-------------------------------------------------------------------------------
# main
#-------------------------------------------------------------------------------
if __name__ == "__main__":
    Firewallmanager()
