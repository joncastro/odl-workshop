#!/usr/bin/env python

import requests

data = {
    "flow": [
        {
            "id": "1",
            "priority": 200,
            "instructions": {
                "instruction": [
                    {
                        "order": 0,
                        "apply-actions": {
                            "action": [
                                {
                                    "order": 0,
                                    "drop-action": {}
                                }
                            ]
                        }
                    }
                ]
            },
            "match": {
               "ipv4-destination": "10.0.0.1/32",
                "ethernet-match": {
                    "ethernet-type": {
                        "type": 2048
                    }
                }
            },
            "flow-name": "flow1",
            "installHw": 'true',
            "cookie_mask": 255,
            "table_id": 0,
            "idle-timeout": 34,
            "barrier": 'false',
            "strict": 'false',
            "cookie": 3
        }
    ]
 }

url = 'http://127.0.0.1:8181/restconf/config/opendaylight-inventory:nodes/node/openflow:3/table/0/flow/1'
headers = { 'Authorization': 'Basic YWRtaW46YWRtaW4=','Content-type': 'application/json', 'Accept': 'application/json' }
r = requests.put(url, json=data, headers=headers)

if (r.status_code != 200):
  print "ERROR: http code: %d" % r.status_code
  print "ERROR: http message: " + r.content
  exit(1)

print "Succeded!"
