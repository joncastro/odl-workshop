#!/usr/bin/env python

import requests

url = 'http://127.0.0.1:8181/restconf/config/opendaylight-inventory:nodes/node/openflow:3/table/0/flow/1'
headers = { 'Authorization': 'Basic YWRtaW46YWRtaW4=','Content-type': 'application/json', 'Accept': 'application/json' }
r = requests.delete(url, headers=headers)

if (r.status_code != 200):
  print "ERROR: http code: %d" % r.status_code
  print "ERROR: http message: " + r.content
  exit(1)

print "Succeded!"
