#!/usr/bin/env python

import json
import argparse

from pybvc.controller.controller import Controller
from pybvc.common.utils import load_dict_from_file

from pybvc.openflowdev.ofswitch import OFSwitch
from pybvc.openflowdev.ofswitch import FlowEntry
from pybvc.openflowdev.ofswitch import Instruction
from pybvc.openflowdev.ofswitch import DropAction
from pybvc.openflowdev.ofswitch import Match
from pybvc.common.status import STATUS
from pybvc.common.constants import *


#-------------------------------------------------------------------------------
# Class 'Flowmanager'
#-------------------------------------------------------------------------------
class Flowmanager(object):
    """ Flowmanager commands executer """

    def __init__(self):
        self.prog = 'flowmanager'
        parser = argparse.ArgumentParser(
            prog=self.prog,
            description='Flow manager command line tool for interaction with OpenFlow Controller',
            usage="%(prog)s [-h] [-C <path>] <command> [<args>]\n"
                  "(type '%(prog)s -h' for details)\n"
                     "\nAvailable commands are:\n"
                     "\n   show-switch       Show network switch information"
                     "\n   show-flow       Show OpenFlow flows information"
                     "\n   delete-flow     Delete OpenFlow flows"
                     "\n   add-flow        Add OpenFlow flows"
                     "\n"
                     "\n  '%(prog)s help <command>' provides details for a specific command")

        parser.add_argument('-C', metavar="<path>", dest='ctrl_cfg_file',
                            help="path to the controller's configuration file (default is './ctrl.yml')",
                            default="./ctrl.yml")

        parser.add_argument('command', help='command to be executed')

        # following method will save on args the first argument
        # and re
        args, remaining_args = parser.parse_known_args()

        # Get Controller's attributes from configuration file
        self.ctrl  = self.get_controller(args.ctrl_cfg_file)
        if(self.ctrl == None):
            print "\n".strip()
            print ("Cannot find controller configuration file")
            print "\n".strip()
            exit(1)

        # Invoke method that is matching the name of sub-command argument
        # cmd variable contains the first argument
        cmd = args.command.replace('-', '_')

        if hasattr(self, cmd):
            getattr(self, cmd)(remaining_args)
        else:
            print "\n".strip()
            print ("Error, unrecognized command '%s'" % cmd)
            print "\n".strip()
            parser.print_help()
            exit(1)

    #---------------------------------------------------------------------------
    # get controller object given yaml configuration file
    #---------------------------------------------------------------------------
    def get_controller(self, file):
        ctrlProperties = {}
        if(load_dict_from_file(file, ctrlProperties) == False):
            print("Config file '%s' read error: " % f)
            return None
        try:
            ctrlIp = ctrlProperties['ctrlIp']
            ctrlPort = ctrlProperties['ctrlPort']
            ctrlUser = ctrlProperties['ctrlUser']
            ctrlPassword = ctrlProperties['ctrlPassword']
        except:
            print ("Some attribute is missing in ctrl.yml file")
            return None

        # create Controller object which represent an API provided
        # by pybvc to execute the most commons actions on the
        # SDN Controller
        return Controller(ctrlIp, ctrlPort, ctrlUser, ctrlPassword, None)


    #---------------------------------------------------------------------------
    # show network switch information
    #---------------------------------------------------------------------------
    def show_switch(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s show-switch [-s=SWITCHID|--switch=SWITCHID] [-v|--verbose]\n\n"
                  "Show OpenFlow Switch information\n\n"
                  "Options:\n"
                  "  -s, --switch    switch identifier\n")
        parser.add_argument('-s', '--switch', metavar = "<SWITCHID>")
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


        print ("TODO print switch '%s' switch" % args.switch)

    #---------------------------------------------------------------------------
    # show flow table information
    #---------------------------------------------------------------------------
    def show_table (self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s show-table [-s=SWITCHID|--switch=SWITCHID] [-t=TABLEID|--table=TABLEID] [-v|--verbose]\n\n"
                  "Show OpenFlow switch table information\n\n"
                  "Options:\n"
                  "  -s, --switch    switch identifier\n"
                  "  -t, --tableid   table identifier\n")
        parser.add_argument('-s', '--switch', metavar = "<SWITCHID>")
        parser.add_argument('-t', '--tableid', metavar = "<TABLEID>")
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


        print "TODO"

    #---------------------------------------------------------------------------
    # add flow to a table and switch
    #---------------------------------------------------------------------------
    def add_flow(self, arguments):
        print "TODO"

    #---------------------------------------------------------------------------
    # delete flow to a table and switch
    #---------------------------------------------------------------------------
    def delete_flow(self, arguments):
        print "TODO"


#-------------------------------------------------------------------------------
# main
#-------------------------------------------------------------------------------
if __name__ == "__main__":
    Flowmanager()
