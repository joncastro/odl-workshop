#!/usr/bin/env python

import json
import argparse

from pybvc.controller.controller import Controller
from pybvc.common.utils import load_dict_from_file

from pybvc.openflowdev.ofswitch import OFSwitch
from pybvc.openflowdev.ofswitch import FlowEntry
from pybvc.openflowdev.ofswitch import Instruction
from pybvc.openflowdev.ofswitch import DropAction
from pybvc.openflowdev.ofswitch import Match
from pybvc.common.status import STATUS
from pybvc.common.constants import *


#-------------------------------------------------------------------------------
# Class 'Flowmanager'
#-------------------------------------------------------------------------------
class Flowmanager(object):
    """ Flowmanager commands executer """

    def __init__(self):
        self.prog = 'flowmanager'
        parser = argparse.ArgumentParser(
            prog=self.prog,
            description='Flow manager command line tool for interaction with OpenFlow Controller',
            usage="%(prog)s [-h] [-C <path>] <command> [<args>]\n"
                  "(type '%(prog)s -h' for details)\n"
                     "\nAvailable commands are:\n"
                     "\n   show-switch       Show network switch information"
                     "\n   show-flow       Show OpenFlow flows information"
                     "\n   delete-flow     Delete OpenFlow flows"
                     "\n   add-flow        Add OpenFlow flows"
                     "\n"
                     "\n  '%(prog)s help <command>' provides details for a specific command")

        parser.add_argument('-C', metavar="<path>", dest='ctrl_cfg_file',
                            help="path to the controller's configuration file (default is './ctrl.yml')",
                            default="./ctrl.yml")

        parser.add_argument('command', help='command to be executed')

        # following method will save on args the first argument
        # and re
        args, remaining_args = parser.parse_known_args()

        # Get Controller's attributes from configuration file
        self.ctrl  = self.get_controller(args.ctrl_cfg_file)
        if(self.ctrl == None):
            print "\n".strip()
            print ("Cannot find controller configuration file")
            print "\n".strip()
            exit(1)

        # Invoke method that is matching the name of sub-command argument
        # cmd variable contains the first argument
        cmd = args.command.replace('-', '_')

        if hasattr(self, cmd):
            getattr(self, cmd)(remaining_args)
        else:
            print "\n".strip()
            print ("Error, unrecognized command '%s'" % cmd)
            print "\n".strip()
            parser.print_help()
            exit(1)

    #---------------------------------------------------------------------------
    # get controller object given yaml configuration file
    #---------------------------------------------------------------------------
    def get_controller(self, file):
        ctrlProperties = {}
        if(load_dict_from_file(file, ctrlProperties) == False):
            print("Config file '%s' read error: " % f)
            return None
        try:
            ctrlIp = ctrlProperties['ctrlIp']
            ctrlPort = ctrlProperties['ctrlPort']
            ctrlUser = ctrlProperties['ctrlUser']
            ctrlPassword = ctrlProperties['ctrlPassword']
        except:
            print ("Some attribute is missing in ctrl.yml file")
            return None

        # create Controller object which represent an API provided
        # by pybvc to execute the most commons actions on the
        # SDN Controller
        return Controller(ctrlIp, ctrlPort, ctrlUser, ctrlPassword, None)


    #---------------------------------------------------------------------------
    # show network switch information
    #---------------------------------------------------------------------------
    def show_switch(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s show-switch [-s=SWITCHID|--switch=SWITCHID] [-v|--verbose]\n\n"
                  "Show OpenFlow Switch information\n\n"
                  "Options:\n"
                  "  -s, --switch    switch identifier\n")
        parser.add_argument('-s', '--switch', metavar = "<SWITCHID>")
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return

        if (args.switch == None):
            result = self.ctrl.get_nodes_operational_list()
            if(result.get_status().eq(STATUS.OK) == True):
                for node in result.get_data():
                    self.print_node(node)
        else:
            self.print_node(args.switch)


    #---------------------------------------------------------------------------
    # print node information
    #---------------------------------------------------------------------------
    def print_node(self, nodeName):

      # create openflow switch object
      ofswitch = OFSwitch(self.ctrl, nodeName)

      print ("\nNode information for: '%s'" % nodeName)

      # get ofswitch information
      result = ofswitch.get_switch_info()

      if(result.get_status().eq(STATUS.OK) == True):
          print ("\nSwitch information for: '%s'" % nodeName)
          print json.dumps(result.get_data(), indent=4)
      else:
         print ("\nSwitch information not found : '%s'" % nodeName)
         return

      # get ports brief information
      result = ofswitch.get_ports_brief_info()

      if(result.get_status().eq(STATUS.OK) == True):
          print ("\nBrief port info for: '%s'" % nodeName)
          print json.dumps(result.get_data(), indent=4)

      # get features information
      result = ofswitch.get_features_info()

      if(result.get_status().eq(STATUS.OK) == True):
          print ("\nFeatures for: '%s'" % nodeName)
          print json.dumps(result.get_data(), indent=4)

    #---------------------------------------------------------------------------
    # show flow table information
    #---------------------------------------------------------------------------
    def show_table (self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s show-table [-s=SWITCHID|--switch=SWITCHID] [-t=TABLEID|--table=TABLEID] [-v|--verbose]\n\n"
                  "Show OpenFlow switch table information\n\n"
                  "Options:\n"
                  "  -s, --switch    switch identifier\n"
                  "  -t, --tableid   table identifier\n")
        parser.add_argument('-s', '--switch', metavar = "<SWITCHID>")
        parser.add_argument('-t', '--tableid', metavar = "<TABLEID>")
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return

        tableid = args.tableid
        if (tableid == None):
            tableid = 0

        if (args.switch == None):
            result = self.ctrl.get_nodes_operational_list()
            if(result.get_status().eq(STATUS.OK) == True):
                for node in result.get_data():
                    self.print_table(node,tableid)
        else:
            self.print_table(args.switch,tableid)


    #---------------------------------------------------------------------------
    # print node information
    #---------------------------------------------------------------------------
    def print_table(self, nodeName, tableid):

      # create openflow switch object
      ofswitch = OFSwitch(self.ctrl, nodeName)

      print ("\nTable information for node '%s' and table '%s'" % (nodeName, tableid))

      # get ofswitch information
      result = ofswitch.get_configured_flows(tableid)
      if(result.get_status().eq(STATUS.OK) == True):
          print "\nConfigured flows:"
          print json.dumps(result.get_data(), indent=4)


    #---------------------------------------------------------------------------
    # add flow to a table and switch
    #---------------------------------------------------------------------------
    def add_flow(self, arguments):
        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s show-table -s=SWITCHID|--switch=SWITCHID -t=TABLEID|--table=TABLEID"
                    "-i=IP|--ip=IP -f=FLOWID|--flowid=FLOWID\n\n"
                  "Show OpenFlow switch table information\n\n"
                  "Options:\n"
                  "  -s, --switch    switch identifier\n"
                  "  -t, --tableid   table identifier\n"
                  "  -i, --ip   ip address\n"
                  "  -f, --flowid   flow id\n")
        parser.add_argument('-s', '--switch', metavar = "<SWITCHID>")
        parser.add_argument('-t', '--tableid', metavar = "<TABLEID>")
        parser.add_argument('-i', '--ip', metavar = "<IP>")
        parser.add_argument('-f', '--flowid', metavar = "<FLOWID>")
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return

        ofswitch = OFSwitch(self.ctrl, args.switch)

        flow_entry = FlowEntry()
        flow_entry.set_flow_table_id(args.tableid)
        flow_entry.set_flow_id(args.flowid)
        flow_entry.set_flow_priority(flow_priority = 1000)

        # --- Instruction: 'Apply-actions'
        #     Action:      'Drop'
        instruction = Instruction(instruction_order = 0)
        action = DropAction(order = 0)
        instruction.add_apply_action(action)
        flow_entry.add_instruction(instruction)

        # --- Match Fields: Ethernet Type
        #                   IPv4 Destination Address
        match = Match()
        match.set_eth_type(ETH_TYPE_IPv4)
        match.set_ipv4_dst(args.ip)
        flow_entry.add_match(match)

        print ("\nFlow to send:")
        print flow_entry.get_payload()

        result = ofswitch.add_modify_flow(flow_entry)
        status = result.get_status()
        if(status.eq(STATUS.OK) == True):
            print ("Flow successfully added to the Controller")
        else:
            print ("\n")
            print ("Flow cannot be configured, reason: %s" % status.brief().lower())



    #---------------------------------------------------------------------------
    # delete flow to a table and switch
    #---------------------------------------------------------------------------
    def delete_flow(self, arguments):
        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s show-table -s=SWITCHID|--switch=SWITCHID -t=TABLEID|--table=TABLEID"
                    "-f=FLOWID|--flowid=FLOWID\n\n"
                  "Show OpenFlow switch table information\n\n"
                  "Options:\n"
                  "  -s, --switch    switch identifier\n"
                  "  -t, --tableid   table identifier\n"
                  "  -f, --flowid   flow id\n")

        parser.add_argument('-s', '--switch', metavar = "<SWITCHID>")
        parser.add_argument('-t', '--tableid', metavar = "<TABLEID>")
        parser.add_argument('-f', '--flowid', metavar = "<FLOWID>")
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return

        ofswitch = OFSwitch(self.ctrl, args.switch)

        result = ofswitch.delete_flow(args.tableid, args.flowid)
        status = result.get_status()
        if(status.eq(STATUS.OK) == True):
            print ("Flow successfully removed from the Controller")
        else:
            print ("\n")
            print ("Flow cannot be removed from the Controller, reason: %s" % status.brief().lower())
            exit(0)



#-------------------------------------------------------------------------------
# main
#-------------------------------------------------------------------------------
if __name__ == "__main__":
    Flowmanager()
