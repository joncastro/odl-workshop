#!/usr/bin/env python

import requests
import json

url = 'http://localhost:8181/restconf/operational/network-topology:network-topology/topology/flow:1'
headers = { 'Authorization': 'Basic YWRtaW46YWRtaW4=','Content-type': 'application/json', 'Accept': 'application/json' }
r = requests.get(url, headers=headers)

if (r.status_code != 200):
  print "ERROR: http code: %d" % r.status_code
  print "ERROR: http message: " + r.content
  exit(1)

data = json.loads(r.content)
print json.dumps(data, indent=4)
