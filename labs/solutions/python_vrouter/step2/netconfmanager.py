#!/usr/bin/env python

import json
import argparse

from pybvc.controller.controller import Controller
from pybvc.common.utils import load_dict_from_file
from pybvc.netconfdev.vrouter.vrouter5600 import VRouter5600
from pybvc.controller.netconfnode import NetconfNode
from pybvc.common.status import STATUS


#-------------------------------------------------------------------------------
# Class 'Netconfmanager'
#-------------------------------------------------------------------------------
class Netconfmanager(object):
    """ Netconf manager commands executer """

    def __init__(self):
        self.prog = 'netconfmanager'
        parser = argparse.ArgumentParser(
            prog=self.prog,
            description='NETCONF manager command line tool for interaction with OpenFlow Controller',
            usage="%(prog)s [-h] [-C <path>] <command> [<args>]\n"
                  "(type '%(prog)s -h' for details)\n"
                     "\nAvailable commands are:\n"
                     "\n   add-device           Add netconf capable device"
                     "\n   add-vrouter          Add a vRouter5600 netconf device"
                     "\n   delete-device        Delete netconf device"
                     "\n   delete-vrouter       Delete vRouter5600 netconf device"
                     "\n   get-status           Get netconf device status"
                     "\n   get-schemas          Get netconf device schemas"
                     "\n   get-vrouter-config   Get vRouter5600 configuration"
                     "\n"
                     "\n  '%(prog)s help <command>' provides details for a specific command")

        parser.add_argument('-C', metavar="<path>", dest='ctrl_cfg_file',
                            help="path to the controller's configuration file (default is './ctrl.yml')",
                            default="./ctrl.yml")

        parser.add_argument('command', help='command to be executed')

        # following method will save on args the first argument
        # and re
        args, remaining_args = parser.parse_known_args()

        # Get Controller's attributes from configuration file
        self.ctrl  = self.get_controller(args.ctrl_cfg_file)
        if(self.ctrl == None):
            print "\n".strip()
            print ("Cannot find controller configuration file")
            print "\n".strip()
            exit(1)

        # Invoke method that is matching the name of sub-command argument
        # cmd variable contains the first argument
        cmd = args.command.replace('-', '_')

        if hasattr(self, cmd):
            getattr(self, cmd)(remaining_args)
        else:
            print "\n".strip()
            print ("Error, unrecognized command '%s'" % cmd)
            print "\n".strip()
            parser.print_help()
            exit(1)

    #---------------------------------------------------------------------------
    # get controller object given yaml configuration file
    #---------------------------------------------------------------------------
    def get_controller(self, file):
        ctrlProperties = {}
        if(load_dict_from_file(file, ctrlProperties) == False):
            print("Config file '%s' read error: " % f)
            return None
        try:
            ctrlIp = ctrlProperties['ctrlIp']
            ctrlPort = ctrlProperties['ctrlPort']
            ctrlUser = ctrlProperties['ctrlUser']
            ctrlPassword = ctrlProperties['ctrlPassword']
        except:
            print ("Some attribute is missing in ctrl.yml file")
            return None

        # create Controller object which represent an API provided
        # by pybvc to execute the most commons actions on the
        # SDN Controller
        return Controller(ctrlIp, ctrlPort, ctrlUser, ctrlPassword, None)


    #---------------------------------------------------------------------------
    # add netconf device
    #---------------------------------------------------------------------------
    def add_device(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s add-device -n=NAME|--name=NAME -i=IP|--ip=IP -p=PORT|--port=PORT"
                  " -u=USER|--user=USER -c=CREDENTIALS|--credentials=CREDENTIALS\n\n"
                  "Add netconf device\n\n"
                  "Options:\n"
                  "  -n, --name        device name\n"
                  "  -i, --ip          device ip address\n"
                  "  -p, --port        netconf port\n"
                  "  -u, --user        user name\n"
                  "  -c, --credentials password\n")
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-i', '--ip', metavar = "<IP>", required=True)
        parser.add_argument('-p', '--port', metavar = "<PORT>", required=True)
        parser.add_argument('-u', '--user', metavar = "<USER>", required=True)
        parser.add_argument('-c', '--credentials', metavar = "<CREDENTIALS>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


        print ("TODO add netconf device '%s' " % args.name)

    #---------------------------------------------------------------------------
    # add vrouter netconf device
    #---------------------------------------------------------------------------
    def add_vrouter(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s add-vrouter -n=NAME|--name=NAME -i=IP|--ip=IP -p=PORT|--port=PORT"
                  " -u=USER|--user=USER -c=CREDENTIALS|--credentials=CREDENTIALS\n\n"
                  "Add vrouter netconf device\n\n"
                  "Options:\n"
                  "  -n, --name          device name\n"
                  "  -i, --ip          device ip address\n"
                  "  -p, --port        netconf port\n"
                  "  -u, --user        user name\n"
                  "  -c, --credentials password\n")
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-i', '--ip', metavar = "<IP>", required=True)
        parser.add_argument('-p', '--port', metavar = "<PORT>", required=True)
        parser.add_argument('-u', '--user', metavar = "<USER>", required=True)
        parser.add_argument('-c', '--credentials', metavar = "<CREDENTIALS>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


        print ("TODO add vrouter netconf device '%s' " % args.name)

    #---------------------------------------------------------------------------
    # delete netconf device
    #---------------------------------------------------------------------------
    def delete_device(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s delete-device -n=NAME|--name=NAME\n\n"
                  "Delete netconf device\n\n"
                  "Options:\n"
                  "  -n, --name          device name\n")
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


        print ("TODO delete netconf device '%s' " % args.name)

    #---------------------------------------------------------------------------
    # delete vrouter netconf device
    #---------------------------------------------------------------------------
    def delete_vrouter(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s delete-vrouter -n=NAME|--name=NAME\n\n"
                  "Delete vrouter netconf device\n\n"
                  "Options:\n"
                  "  -n, --name          device name\n")
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


        print ("TODO delete vrouter netconf device '%s' " % args.name)

    #---------------------------------------------------------------------------
    # get netconf device status
    #---------------------------------------------------------------------------
    def get_status(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s get-status -n=NAME|--name=NAME\n\n"
                  "Get netconf device status\n\n"
                  "Options:\n"
                  "  -n, --name          device name\n")
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return

        print ("TODO get status from a netconf device '%s' " % args.name)

    #---------------------------------------------------------------------------
    # get netconf device schemas
    #---------------------------------------------------------------------------
    def get_schemas(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s get-schemas -n=NAME|--name=NAME\n\n"
                  "Get netconf device schemas\n\n"
                  "Options:\n"
                  "  -n, --name          device name\n")
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


        print ("TODO get schemas from a netconf device '%s' " % args.name)

    #---------------------------------------------------------------------------
    # get vrouter configuration
    #---------------------------------------------------------------------------
    def get_vrouter_config(self, arguments):

        # parse agurments
        parser = argparse.ArgumentParser(
            prog=self.prog,
            usage="%(prog)s get-vrouter-config -n=NAME|--name=NAME\n\n"
                  "Get netconf device schemas\n\n"
                  "Options:\n"
                  "  -n, --name          device name\n")
        parser.add_argument('-n', '--name', metavar = "<NAME>", required=True)
        parser.add_argument('-U', action="store_true", dest="usage", help=argparse.SUPPRESS)

        args = parser.parse_args(arguments)
        if(args.usage):
            parser.print_usage()
            print "\n".strip()
            return


        print ("TODO get vrouter configuration from device '%s' " % args.name)


#-------------------------------------------------------------------------------
# main
#-------------------------------------------------------------------------------
if __name__ == "__main__":
    Netconfmanager()
